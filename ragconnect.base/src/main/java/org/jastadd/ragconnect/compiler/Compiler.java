package org.jastadd.ragconnect.compiler;

import beaver.Parser;
import org.jastadd.option.BooleanOption;
import org.jastadd.option.ValueOption;
import org.jastadd.ragconnect.ast.*;
import org.jastadd.ragconnect.parser.RagConnectParser;
import org.jastadd.ragconnect.scanner.RagConnectScanner;
import org.jastadd.relast.compiler.AbstractCompiler;
import org.jastadd.relast.compiler.CompilerException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Compiler extends AbstractCompiler {

  private ValueOption optionRootNode;
  private ValueOption optionProtocols;
  private BooleanOption optionPrintYaml;
  private BooleanOption optionVerbose;
  private BooleanOption optionLogReads;
  private BooleanOption optionLogWrites;
  private BooleanOption optionLogIncremental;
  private ValueOption optionLogTarget;
  private BooleanOption optionExperimentalJastAdd329;
  private BooleanOption optionEvaluationCounter;

  private static final String OPTION_LOGGING_TARGET_CONSOLE = "console";
  private static final String OPTION_LOGGING_TARGET_SLF4J = "slf4j";

  private static final String OPTION_PROTOCOL_JAVA = "java";
  private static final String OPTION_PROTOCOL_MQTT = "mqtt";
  private static final String OPTION_PROTOCOL_REST = "rest";
  private static final String OPTION_PROTOCOL_REST_CLIENT = "restClient";

  public Compiler() {
    super("ragconnect", true);
  }

  @Override
  protected int compile() throws CompilerException {
    compile0();
    return 0;
  }

  /**
   * Compiles with given options. Either successful, or throws exception upon failure.
   * @throws CompilerException if something went wrong
   */
  private void compile0() throws CompilerException {
    if (getConfiguration().shouldPrintVersion()) {
      System.out.println(readVersion());
      return;
    }
    if (getConfiguration().shouldPrintHelp()) {
      getConfiguration().printHelp(System.out);
      return;
    }

    System.out.println("Running RagConnect " + readVersion());

    if (!getConfiguration().outputDir().exists()) {
      try {
        Files.createDirectories(getConfiguration().outputDir().toPath());
      } catch (IOException e) {
        throw new CompilerException("Error creating output dir " + getConfiguration().outputDir(), e);
      }
    }

    if (!optionRootNode.isMatched()) {
      throw new CompilerException("Root node not specified");
    }

    RagConnect ragConnect = parseProgram(getConfiguration().getFiles());
    try {
      setConfiguration(ragConnect);
    } catch (RuntimeException re) {
      throw new CompilerException("Failed to parse all files", re);
    }

    if (!ragConnect.warnings().isEmpty()) {
      StringBuilder sb = new StringBuilder("Warnings:\n");
      for (CompilerMessage message : ragConnect.warnings()) {
        sb.append(message).append("\n");
      }
      System.err.println(sb);
    }

    if (!ragConnect.errors().isEmpty()) {
      StringBuilder sb = new StringBuilder("Errors:\n");
      for (CompilerMessage message : ragConnect.errors()) {
        sb.append(message).append("\n");
      }
      System.err.println(sb);
      System.exit(1);
    }

    if (optionPrintYaml.value()) {
      String yamlContent = ragConnect.toYAML().prettyPrint();
      if (isVerbose()) {
        System.out.println(yamlContent);
      }
      writeToFile(getConfiguration().outputDir().toPath().resolve("RagConnect.yml"), yamlContent);
      return;
    }

    if (isVerbose()) {
      System.out.println("Writing output files");
    }
    for (GrammarFile grammarFile : ragConnect.getProgram().getGrammarFileList()) {
      Path outputFile = getConfiguration().outputDir().toPath().resolve(grammarFile.getFileName());
      writeToFile(outputFile, grammarFile.generateAbstractGrammar());
    }
    String aspectCode;
    try {
      aspectCode = generateAspect(ragConnect);
    } catch (RuntimeException re) {
      throw new CompilerException("Could not generate RagConnect aspect", re);
    }
    writeToFile(getConfiguration().outputDir().toPath().resolve("RagConnect.jadd"), aspectCode);
  }

  public static void main(String[] args) {
    System.setProperty("mustache.debug", "true");
    Compiler compiler = new Compiler();
    try {
      compiler.run(args);
    } catch (CompilerException e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
      System.exit(1);
    }
  }

  private boolean isVerbose() {
    return optionVerbose != null && optionVerbose.value();
  }

  /**
   * Reads the version string.
   * <p>
   * The version string is read from the property file
   * src/main/resources/Version.properties. This
   * file should be generated during the build process. If it is missing
   * then there is some problem in the build script.
   *
   * @return the read version string, or <code>version ?</code>
   * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
   */
  private String readVersion() {
    try {
      ResourceBundle resources = ResourceBundle.getBundle("ragconnectVersion");
      return resources.getString("version");
    } catch (MissingResourceException e) {
      return "version ?";
    }
  }

  private void writeToFile(Path path, String str) throws CompilerException {
    try (BufferedWriter writer = Files.newBufferedWriter(path)) {
      writer.append(str);
    } catch (Exception e) {
      throw new CompilerException("Could not write to file " + path.toAbsolutePath(), e);
    }
  }

  protected void initOptions() {
    super.initOptions();
    optionRootNode = addOption(
        new ValueOption("rootNode", "root node in the base grammar.")
            .acceptAnyValue()
            .needsValue(true));
    optionProtocols = addOption(
        new ValueOption("protocols", "Protocols to enable")
            .acceptMultipleValues(true)
            .addDefaultValue(OPTION_PROTOCOL_MQTT, "Enable MQTT")
            .addAcceptedValue(OPTION_PROTOCOL_JAVA, "Enable Java (experimental)")
            .addAcceptedValue(OPTION_PROTOCOL_REST, "Enable REST")
            .addAcceptedValue(OPTION_PROTOCOL_REST_CLIENT, "Enable REST client (experimental)")
    );
    optionPrintYaml = addOption(
        new BooleanOption("printYaml", "Print out YAML instead of generating files and exit.")
            .defaultValue(false));
    optionVerbose = addOption(
        new BooleanOption("verbose", "Print more messages while compiling.")
            .defaultValue(false));
    optionLogReads = addOption(
        new BooleanOption("logReads", "Enable logging for every read.")
            .defaultValue(false));
    optionLogWrites = addOption(
        new BooleanOption("logWrites", "Enable logging for every write.")
            .defaultValue(false));
    optionLogIncremental = addOption(
        new BooleanOption("logIncremental", "Enable logging for observer in incremental dependency tracking.")
            .defaultValue(false));
    optionLogTarget = addOption(
            new ValueOption("logTarget", "Logging target to use")
                    .addDefaultValue(OPTION_LOGGING_TARGET_CONSOLE, "Use std out and std err")
                    .addAcceptedValue(OPTION_LOGGING_TARGET_SLF4J, "Use SLF4J API")
    );
    optionExperimentalJastAdd329 = addOption(
        new BooleanOption("experimental-jastadd-329", "Use trace events INC_FLUSH_START and INC_FLUSH_END (JastAdd issue #329).")
            .defaultValue(false));
    optionEvaluationCounter = addOption(
            new BooleanOption("evaluationCounter", "Enable counters for evaluation.")
                    .defaultValue(false));
  }

  private RagConnect parseProgram(Collection<String> files) throws CompilerException {
    Program program = new Program();
    boolean atLeastOneGrammar = false;
    boolean atLeastOneRagConnect = false;

    RagConnect ragConnect = new RagConnect();
    ragConnect.setProgram(program);

    GrammarFile ragConnectGrammarPart = new GrammarFile();
    ragConnectGrammarPart.setFileName("RagConnect.relast");
    program.addGrammarFile(ragConnectGrammarPart);

    for (String filename : files) {
      String extension = filename.substring(filename.lastIndexOf('.') + 1);
      switch (extension) {
        case "ast":
        case "relast":
          // process grammar
          program.addGrammarFile(parseGrammar(filename));
          atLeastOneGrammar = true;
          break;
        case "connect":
        case "ragconnect":
          // process RagConnect specification
          ragConnect.addConnectSpecificationFile(parseConnectSpec(filename));
          atLeastOneRagConnect = true;
          break;
        default:
          throw new CompilerException("Unknown file extension " + extension + " in " + filename);
      }
    }

    if (!atLeastOneRagConnect) {
      System.err.println("No RagConnect specification file (*.connect, *.ragconnect) specified!");
    }
    if (!atLeastOneGrammar) {
      // without a grammar, RagConnect can not operate
      System.err.println("No grammar file (*.ast, *.relast) specified! Exiting!");
      System.exit(1);
    }

    // here, the program subtree is also flushed and resolved
    ragConnect.flushTreeCache();
    ragConnect.treeResolveAll();

    // add new parts to production rule, and new relations
    ragConnect.additionalRelations().forEach(ragConnectGrammarPart::addDeclaration);
    ragConnect.additionalTokens().forEach(TypeDecl::addComponent);

    return ragConnect;
  }

  private GrammarFile parseGrammar(String filename) throws CompilerException {
    try (BufferedReader reader = Files.newBufferedReader(Paths.get(filename))) {
      RagConnectScanner scanner = new RagConnectScanner(reader);
      RagConnectParser parser = new RagConnectParser();
      GrammarFile grammarFile = (GrammarFile) parser.parse(scanner);
      if (isVerbose()) {
        System.out.println(grammarFile.dumpTree());
      }
      grammarFile.setFileName(toBaseName(filename));
      return grammarFile;
    } catch (IOException | Parser.Exception e) {
      throw new CompilerException("Could not parse grammar file " + filename, e);
    }
  }

  private ConnectSpecificationFile parseConnectSpec(String filename) throws CompilerException {
    try (BufferedReader reader = Files.newBufferedReader(Paths.get(filename))) {
      RagConnectScanner scanner = new RagConnectScanner(reader);
      RagConnectParser parser = new RagConnectParser();
      ConnectSpecificationFile specificationFile = (ConnectSpecificationFile) parser.parse(scanner, RagConnectParser.AltGoals.connect_specification_file);
      specificationFile.setFileName(toBaseName(filename));
      return specificationFile;
    } catch (IOException | Parser.Exception e) {
      throw new CompilerException("Could not parse connect file " + filename, e);
    }
  }

  /**
   * Extracts the basename of the given file, with file extension
   *
   * @param filename the given filename
   * @return the basename
   */
  private String toBaseName(String filename) {
    return new File(filename).getName();
  }

  /**
   * Set all configuration values.
   * @param ragConnect the RagConnect instance to set configuration values
   */
  private void setConfiguration(RagConnect ragConnect) throws CompilerException {
    ragConnect.setConfiguration(new Configuration());
    ragConnect.getConfiguration().setLoggingEnabledForReads(optionLogReads.value());
    ragConnect.getConfiguration().setLoggingEnabledForWrites(optionLogWrites.value());
    ragConnect.getConfiguration().setLoggingEnabledForIncremental(optionLogIncremental.value());
    ragConnect.getConfiguration().setLoggingTarget(optionLogTarget.value());
    ragConnect.getConfiguration().setExperimentalJastAdd329(optionExperimentalJastAdd329.value());
    ragConnect.getConfiguration().setEvaluationCounter(optionEvaluationCounter.value());

    // reuse "--incremental" and "--tracing=flush" options of JastAdd
    boolean incrementalOptionActive = this.getConfiguration().incremental() && this.getConfiguration().traceFlush();
    ragConnect.getConfiguration().setIncrementalOptionActive(incrementalOptionActive);
    if (isVerbose()) {
      System.out.println("ragConnect.getConfiguration().IncrementalOptionActive = " + incrementalOptionActive);
    }

    // reuse "--cache=all" option of JastAdd
    ragConnect.getConfiguration().setCacheAllOptionActive(this.getConfiguration().cacheAll());

    // reuse "--List" and "--Opt" options of JastAdd
    ragConnect.getConfiguration().setJastAddList(this.getConfiguration().listType());
    ragConnect.getConfiguration().setJastAddOpt(this.getConfiguration().optType());

    final TypeDecl rootNode;
    try {
      rootNode = ragConnect.getProgram().resolveTypeDecl(optionRootNode.value());
    } catch (RuntimeException re) {
      // root node was not found
      throw new CompilerException("Could not resolve root node '" + optionRootNode.value() + "'!", re);
    }
    ragConnect.getConfiguration().setRootNode(rootNode);

    // Handler ::= <ClassName> <UniqueName> <InUse:boolean>;
    ragConnect.addHandler(new Handler("JavaHandler", "java", optionProtocols.hasValue(OPTION_PROTOCOL_JAVA)));
    ragConnect.addHandler(new Handler("MqttServerHandler", "mqtt", optionProtocols.hasValue(OPTION_PROTOCOL_MQTT)));
    ragConnect.addHandler(new Handler("RestServerHandler", "rest", optionProtocols.hasValue(OPTION_PROTOCOL_REST)));
    ragConnect.addHandler(new Handler("RestClientHandler", "restClient",
            optionProtocols.hasValue(OPTION_PROTOCOL_REST_CLIENT)));
  }

  public String generateAspect(RagConnect ragConnect) {
    StringBuilder sb = new StringBuilder();
    // add handler to get error message when template expansion did not find some part
    com.github.mustachejava.reflect.ReflectionObjectHandler roh = new com.github.mustachejava.reflect.ReflectionObjectHandler() {
      @Override
      public com.github.mustachejava.Binding createBinding(String name, final com.github.mustachejava.TemplateContext tc, com.github.mustachejava.Code code) {
        return new com.github.mustachejava.reflect.GuardedBinding(this, name, tc, code) {
          @Override
          protected synchronized com.github.mustachejava.util.Wrapper getWrapper(String name, java.util.List<Object> scopes) {
            com.github.mustachejava.util.Wrapper wrapper = super.getWrapper(name, scopes);
            if (wrapper instanceof com.github.mustachejava.reflect.MissingWrapper) {
              throw new com.github.mustachejava.MustacheException(name + " not found in " + tc);
            }
            return wrapper;
          }
        };
      }
    };
    com.github.mustachejava.DefaultMustacheFactory mf = new com.github.mustachejava.DefaultMustacheFactory();
    mf.setObjectHandler(roh);
    com.github.mustachejava.Mustache m = mf.compile("ragconnect.mustache");
//    String yaml = ragConnect.toYAML().prettyPrint();
//    Object context = new org.yaml.snakeyaml.Yaml().load(new StringReader(yaml));
    m.execute(new java.io.PrintWriter(new org.jastadd.ragconnect.compiler.AppendableWriter(sb)), ragConnect);
    return sb.toString();
  }

  @Override
  protected int error(String message) {
    System.err.println(message);
    return 1;
  }

}
