package org.jastadd.ragconnect.compiler;

import java.io.IOException;
import java.io.Writer;

/**
 * Writer appending to a StringBuilder.
 *
 * @author rschoene - Initial contribution
 */
public class AppendableWriter extends Writer {
  private final StringBuilder sb;

  public AppendableWriter(StringBuilder sb) {
    this.sb = sb;
  }

  @Override
  public void write(char[] chars, int off, int len) throws IOException {
    sb.append(chars, off, len);
  }

  @Override
  public void write(String str) throws IOException {
    sb.append(str);
  }

  @Override
  public void flush() {

  }

  @Override
  public void close() {

  }
}
