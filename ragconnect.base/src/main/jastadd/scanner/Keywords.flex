"receive"    { yybegin(DECLARATION); return sym(Terminals.RECEIVE); }
"send"       { yybegin(DECLARATION); return sym(Terminals.SEND); }
"using"      { return sym(Terminals.USING); }
"canDependOn" { return sym(Terminals.CAN_DEPEND_ON); }
"maps"       { return sym(Terminals.MAPS); }
"to"         { return sym(Terminals.TO); }
"as"         { return sym(Terminals.AS); }
"with"       { return sym(Terminals.WITH); }
"indexed"    { return sym(Terminals.INDEXED); }
"add"        { return sym(Terminals.ADD); }
"("          { return sym(Terminals.BRACKET_LEFT); }
")"          { return sym(Terminals.BRACKET_RIGHT); }
