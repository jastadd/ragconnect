package org.jastadd.ragconnect.scanner;

import org.jastadd.ragconnect.parser.RagConnectParser.Terminals;
%%

%public
%final
%class RagConnectScanner
%extends beaver.Scanner

%type beaver.Symbol
%function nextToken
%yylexthrow beaver.Scanner.Exception
%scanerror RagConnectScanner.ScannerError

%x COMMENT
%s DECLARATION

%line
%column
