package org.jastadd.ragconnect.tests;

import javaInc.ast.*;
import org.assertj.core.groups.Tuple;
import org.jastadd.ragconnect.tests.utils.TestChecker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.tuple;
import static org.jastadd.ragconnect.tests.utils.TestUtils.javaUri;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Testing the Java handler.
 *
 * @author rschoene - Initial contribution
 */
public class JavaTest extends RagConnectTest {

  private static final String TOPIC_RECEIVE_TOKEN = "receiveToken";
  private static final String TOPIC_RECEIVE_NODE_PLAIN = "receiveNode/plain";
  private static final String TOPIC_RECEIVE_NODE_MAPPED = "receiveNode/mapped";
  private static final String TOPIC_RECEIVE_MANY = "receiveMany";
  private static final String TOPIC_RECEIVE_NTA = "receiveNTA";

  private static final String TOPIC_SEND_TOKEN = "sendToken";
  private static final String TOPIC_SEND_NODE = "sendNode";
  private static final String TOPIC_SEND_MANY = "sendMany";
  private static final String TOPIC_SEND_NTA = "sendNTA";

  private Root model;
  private SenderRoot senderRoot;
  private ReceiverRoot receiverRoot;

  private String lastValueToken;
  private A lastValueNode;
  private JastAddList<A> lastValueMany;
  private A lastValueNTA;
  private TestChecker checker;

  void createModel() {
    model = new Root();
    senderRoot = new SenderRoot().setInput("1").setSendNode(createA("1"));
    receiverRoot = new ReceiverRoot();
    model.addSenderRoot(senderRoot);
    model.setReceiverRoot(receiverRoot);
  }

  private static A createA(String value) {
    return new A().setValue(value).setInner(new Inner("inner"));
  }

  private void setupReceiverAndConnect(boolean writeCurrentValue) throws IOException {
    // checker
    checker = new TestChecker();
    checker.setActualString(TOPIC_SEND_TOKEN, () -> lastValueToken)
            .setCheckForString(TOPIC_SEND_NODE, (name, expected) -> checkA(expected, lastValueNode, name))
            .setCheckForTuple(TOPIC_SEND_MANY, (name, expected) -> checkAList(expected, lastValueMany, name))
            .setCheckForString(TOPIC_SEND_NTA, (name, expected) -> checkA(expected, lastValueNTA, name))
            .setActualString(TOPIC_RECEIVE_TOKEN, () -> receiverRoot.getSomeToken())
            .setCheckForString(TOPIC_RECEIVE_NODE_PLAIN, (name, expected) ->
                    checkA(expected, receiverRoot.getSomeNode(), name))
            .setCheckForString(TOPIC_RECEIVE_NODE_MAPPED, (name, expected) ->
                    checkA(expected, receiverRoot.getSomeNodeWithMapping(), name))
            .setCheckForTuple(TOPIC_RECEIVE_MANY, (name, expected) ->
                    checkAList(expected, receiverRoot.getManyNodeList(), name))
            .setCheckForString(TOPIC_RECEIVE_NTA, (name, expected) ->
                    checkA(expected, receiverRoot.getNTA(), name))
            .put(TOPIC_RECEIVE_TOKEN, "")
            .put(TOPIC_RECEIVE_MANY, tuple())
            .setActualNumberOfValues(() -> 0)
            .disableManualWait();

    // callbacks
    model.ragconnectJavaRegisterConsumer(TOPIC_SEND_TOKEN, bytes -> lastValueToken = new String(bytes));
    model.ragconnectJavaRegisterConsumer(TOPIC_SEND_NODE, bytes -> lastValueNode = ExposingASTNode.INSTANCE.bytesToA(bytes));
    model.ragconnectJavaRegisterConsumer(TOPIC_SEND_MANY, bytes -> lastValueMany = ExposingASTNode.INSTANCE.bytesToList(bytes));
    model.ragconnectJavaRegisterConsumer(TOPIC_SEND_NTA, bytes -> lastValueNTA = ExposingASTNode.INSTANCE.bytesToA(bytes));

    // receive
    receiverRoot.connectSomeToken(javaUri(TOPIC_RECEIVE_TOKEN));
    receiverRoot.connectSomeNode(javaUri(TOPIC_RECEIVE_NODE_PLAIN));
    receiverRoot.connectSomeNodeWithMapping(javaUri(TOPIC_RECEIVE_NODE_MAPPED));
    receiverRoot.connectManyNodeList(javaUri(TOPIC_RECEIVE_MANY));
    receiverRoot.connectNTA(javaUri(TOPIC_RECEIVE_NTA));

    // send
    senderRoot.connectSendToken(javaUri(TOPIC_SEND_TOKEN), writeCurrentValue);
    senderRoot.connectSendNode(javaUri(TOPIC_SEND_NODE), writeCurrentValue);
    senderRoot.connectSendManyNodeList(javaUri(TOPIC_SEND_MANY), writeCurrentValue);
    senderRoot.connectSendNTA(javaUri(TOPIC_SEND_NTA), writeCurrentValue);
  }

  @Test
  public void testCommunicateSendInitialValue() throws IOException {
    createModel();
    setupReceiverAndConnect(true);
    checker.put(TOPIC_SEND_TOKEN, "")
            .put(TOPIC_SEND_NODE, "1")
            .put(TOPIC_SEND_MANY, tuple())
            .put(TOPIC_SEND_NTA, "1|1")
    ;

    communicateBoth();
  }

  @Test
  public void testCommunicateOnlyUpdatedValue() throws IOException {
    createModel();
    setupReceiverAndConnect(false);

    checker.put(TOPIC_SEND_TOKEN, (String) null)
            .put(TOPIC_SEND_NODE, (String) null)
            .put(TOPIC_SEND_MANY, (Tuple) null)
            .put(TOPIC_SEND_NTA, (String) null)
    ;

    communicateBoth();
  }

  private void communicateBoth() {
    checker.check();

    senderRoot.setInput("2");
    checker.put(TOPIC_SEND_NTA, "2|1").check();

    senderRoot.getSendNode().setValue("3");
    checker.put(TOPIC_SEND_NODE, "3").check();

    senderRoot.setSendToken("test-4");
    checker.put(TOPIC_SEND_TOKEN, "test-4").check();

    senderRoot.addSendManyNode(createA("5"));
    checker.put(TOPIC_SEND_MANY, tuple("5")).check();

    model.ragconnectJavaPush(TOPIC_RECEIVE_TOKEN, ExposingASTNode.INSTANCE.stringToBytes("7"));
    checker.put(TOPIC_RECEIVE_TOKEN, "7").check();

    model.ragconnectJavaPush(TOPIC_RECEIVE_NODE_PLAIN, ExposingASTNode.INSTANCE.aToBytes(createA("8")));
    checker.put(TOPIC_RECEIVE_NODE_PLAIN, "8").check();

    model.ragconnectJavaPush(TOPIC_RECEIVE_NODE_MAPPED, ExposingASTNode.INSTANCE.aToBytes(createA("9")));
    checker.put(TOPIC_RECEIVE_NODE_MAPPED, "9-post|inner-post").check();

    model.ragconnectJavaPush(TOPIC_RECEIVE_MANY, ExposingASTNode.INSTANCE.listToBytes(new JastAddList<>(createA("10"), createA("11"))));
    checker.put(TOPIC_RECEIVE_MANY, tuple("10", "11")).check();

    model.ragconnectJavaPush(TOPIC_RECEIVE_NTA, ExposingASTNode.INSTANCE.aToBytes(createA("12")));
    checker.put(TOPIC_RECEIVE_NTA, "12").check();
  }

  @AfterEach
  public void printEvaluationSummary() {
    System.out.println(model.ragconnectEvaluationCounterSummary());
  }

  private void checkA(String expectedValue, A actual, String alias) {
    if (expectedValue == null) {
      assertNull(actual, alias);
    } else {
      final String expectedValueInA, expectedInnerValue;
      if (expectedValue.contains("|")) {
        String[] tokens = expectedValue.split("\\|");
        expectedValueInA = tokens[0];
        expectedInnerValue = tokens[1];
      } else {
        expectedValueInA = expectedValue;
        expectedInnerValue = "inner";
      }
      assertNotNull(actual, alias);
      assertEquals(expectedValueInA, actual.getValue(), alias);
      assertNotNull(actual.getInner(), alias + ".inner");
      assertEquals(expectedInnerValue, actual.getInner().getInnerValue(), alias + ".inner");
    }
  }

  private void checkAList(Tuple expectedTuple, JastAddList<A> actual, String alias) {
    if (expectedTuple == null) {
      assertNull(actual, alias);
      return;
    }
    List<Object> expected = expectedTuple.toList();
    assertEquals(expected.size(), actual.getNumChild(), alias + ".size");
    for (int i = 0, expectedSize = expected.size(); i < expectedSize; i++) {
      String s = (String) expected.get(i);
      checkA(s, actual.getChild(i), alias + "[" + i + "]");
    }
  }

  @AfterEach
  public void alwaysCloseConnections() {
    logger.debug("Closing connections");
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  @SuppressWarnings({"rawtypes" , "unchecked"})
  static class ExposingASTNode extends ASTNode {
    static ExposingASTNode INSTANCE = new ExposingASTNode();

    public A bytesToA(byte[] bytes) {
      try {
        return _ragconnect__apply__TreeDefaultBytesToAMapping(bytes);
      } catch (Exception e) {
        return null;
      }
    }

    public JastAddList<A> bytesToList(byte[] input) {
      try {
        return _ragconnect__apply__TreeDefaultBytesToJastAddListAListMapping(input);
      } catch (Exception e) {
        return null;
      }
    }

    public byte[] aToBytes(A input) {
      try {
        return _ragconnect__apply__TreeDefaultAToBytesMapping(input);
      } catch (Exception e) {
        return null;
      }
    }

    public byte[] listToBytes(JastAddList<A> input) {
      try {
        return _ragconnect__apply__TreeDefaultJastAddListToBytesMapping(input);
      } catch (Exception e) {
        return null;
      }
    }

    public byte[] stringToBytes(String input) {
      try {
        return _ragconnect__apply__DefaultStringToBytesMapping(input);
      } catch (Exception e) {
        return null;
      }
    }
  }

}
