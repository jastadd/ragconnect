package org.jastadd.ragconnect.tests.singleListVariant;

import org.assertj.core.api.Assertions;
import org.jastadd.ragconnect.tests.AbstractMqttTest;
import org.jastadd.ragconnect.tests.utils.IntList;
import org.jastadd.ragconnect.tests.utils.TestChecker;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.BiConsumer;

import static java.lang.Math.abs;
import static org.jastadd.ragconnect.tests.utils.IntList.list;
import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.jastadd.ragconnect.tests.utils.TestUtils.testJaddContainReferenceToJackson;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Base class for test cases "singleList variant manual" and "singleList variant incremental".
 *
 * @author rschoene - Initial contribution
 */
@Tag("List")
@Tag("SingleList")
public abstract class AbstractSingleListVariantTest extends AbstractMqttTest {

  private final TestChecker checker;

  public interface TestWrapperJastAddList<T> extends Iterable<T> {
    int getNumChild();
  }
  public interface TestWrapperReceiverRoot {
    TestWrapperJastAddList<? extends TestWrapperT_Empty> getT_EmptyList();
    TestWrapperJastAddList<? extends TestWrapperT_Token> getT_TokenList();
    TestWrapperJastAddList<? extends TestWrapperT_OneChild> getT_OneChildList();
    TestWrapperJastAddList<? extends TestWrapperT_OneOpt> getT_OneOptList();
    TestWrapperJastAddList<? extends TestWrapperT_OneList> getT_OneListList();
    TestWrapperJastAddList<? extends TestWrapperT_TwoChildren> getT_TwoChildrenList();
    TestWrapperJastAddList<? extends TestWrapperT_OneOfEach> getT_OneOfEachList();
    TestWrapperJastAddList<? extends TestWrapperT_Abstract> getT_AbstractList();

    TestWrapperJastAddList<? extends TestWrapperT_Empty> getMyEmptyList();

    TestWrapperJastAddList<? extends TestWrapperT_Empty> getEmptyWithAddList();
    TestWrapperJastAddList<? extends TestWrapperT_Token> getTokenWithAddList();
    TestWrapperJastAddList<? extends TestWrapperT_OneChild> getOneChildWithAddList();
    TestWrapperJastAddList<? extends TestWrapperT_OneOpt> getOneOptWithAddList();
    TestWrapperJastAddList<? extends TestWrapperT_OneList> getOneListWithAddList();
    TestWrapperJastAddList<? extends TestWrapperT_TwoChildren> getTwoChildrenWithAddList();
    TestWrapperJastAddList<? extends TestWrapperT_OneOfEach> getOneOfEachWithAddList();
    TestWrapperJastAddList<? extends TestWrapperT_Abstract> getAbstractWithAddList();

    boolean connectT_Empty(String uriString) throws IOException;
    boolean connectT_Token(String uriString) throws IOException;
    boolean connectT_OneChild(String uriString) throws IOException;
    boolean connectT_OneOpt(String uriString) throws IOException;
    boolean connectT_OneList(String uriString) throws IOException;
    boolean connectT_TwoChildren(String uriString) throws IOException;
    boolean connectT_OneOfEach(String uriString) throws IOException;
    boolean connectT_Abstract(String uriString) throws IOException;

    boolean connectMyEmpty(String uriString) throws IOException;

    boolean connectEmptyWithAdd(String uriString) throws IOException;
    boolean connectTokenWithAdd(String uriString) throws IOException;
    boolean connectOneChildWithAdd(String uriString) throws IOException;
    boolean connectOneOptWithAdd(String uriString) throws IOException;
    boolean connectOneListWithAdd(String uriString) throws IOException;
    boolean connectTwoChildrenWithAdd(String uriString) throws IOException;
    boolean connectOneOfEachWithAdd(String uriString) throws IOException;
    boolean connectAbstractWithAdd(String uriString) throws IOException;

    boolean disconnectT_Empty(String uriString) throws IOException;
    boolean disconnectT_Token(String uriString) throws IOException;
    boolean disconnectT_OneChild(String uriString) throws IOException;
    boolean disconnectT_OneOpt(String uriString) throws IOException;
    boolean disconnectT_OneList(String uriString) throws IOException;
    boolean disconnectT_TwoChildren(String uriString) throws IOException;
    boolean disconnectT_OneOfEach(String uriString) throws IOException;
    boolean disconnectT_Abstract(String uriString) throws IOException;

    boolean disconnectMyEmpty(String uriString) throws IOException;

    boolean disconnectEmptyWithAdd(String uriString) throws IOException;
    boolean disconnectTokenWithAdd(String uriString) throws IOException;
    boolean disconnectOneChildWithAdd(String uriString) throws IOException;
    boolean disconnectOneOptWithAdd(String uriString) throws IOException;
    boolean disconnectOneListWithAdd(String uriString) throws IOException;
    boolean disconnectTwoChildrenWithAdd(String uriString) throws IOException;
    boolean disconnectOneOfEachWithAdd(String uriString) throws IOException;
    boolean disconnectAbstractWithAdd(String uriString) throws IOException;
  }
  public interface TestWrapperSenderRoot {
    boolean connectT_Empty(String uriString, boolean writeCurrentValue) throws IOException;
    boolean connectT_Token(String uriString, boolean writeCurrentValue) throws IOException;
    boolean connectT_OneChild(String uriString, boolean writeCurrentValue) throws IOException;
    boolean connectT_OneOpt(String uriString, boolean writeCurrentValue) throws IOException;
    boolean connectT_OneList(String uriString, boolean writeCurrentValue) throws IOException;
    boolean connectT_TwoChildren(String uriString, boolean writeCurrentValue) throws IOException;
    boolean connectT_OneOfEach(String uriString, boolean writeCurrentValue) throws IOException;
    boolean connectT_Abstract(String uriString, boolean writeCurrentValue) throws IOException;

    boolean disconnectT_Empty(String uriString) throws IOException;
    boolean disconnectT_Token(String uriString) throws IOException;
    boolean disconnectT_OneChild(String uriString) throws IOException;
    boolean disconnectT_OneOpt(String uriString) throws IOException;
    boolean disconnectT_OneList(String uriString) throws IOException;
    boolean disconnectT_TwoChildren(String uriString) throws IOException;
    boolean disconnectT_OneOfEach(String uriString) throws IOException;
    boolean disconnectT_Abstract(String uriString) throws IOException;

    TestWrapperSenderRoot setInput(int input);
    @SuppressWarnings("UnusedReturnValue")
    TestWrapperSenderRoot setShouldSetOptAndList(boolean shouldSetOptAndList);
    TestWrapperT_Empty getT_Empty();
    TestWrapperT_OneOpt getT_OneOpt();
  }
  public interface TestWrapperNameable {
    int getID();
  }
  public interface TestWrapperOther extends TestWrapperNameable {}
  public interface TestWrapperT_Empty extends TestWrapperNameable {}
  public interface TestWrapperT_Token extends TestWrapperNameable {
    String getValue();
  }
  public interface TestWrapperT_OneChild extends TestWrapperNameable {
    TestWrapperNameable getOther();
  }
  public interface TestWrapperT_OneOpt extends TestWrapperNameable {
    boolean hasOther();
    TestWrapperNameable getOther();
  }
  public interface TestWrapperT_OneList extends TestWrapperNameable {
    int getNumOther();
    TestWrapperNameable getOther(int index);
  }
  public interface TestWrapperT_TwoChildren extends TestWrapperNameable {
    TestWrapperNameable getLeft();
    TestWrapperNameable getRight();
  }
  public interface TestWrapperT_OneOfEach extends TestWrapperNameable {
    TestWrapperNameable getFirst();
    boolean hasSecond();
    TestWrapperNameable getSecond();
    int getNumThird();
    TestWrapperNameable getThird(int index);
    String getFourth();
  }
  public interface TestWrapperT_Abstract extends TestWrapperNameable {
    String getValueAbstract();
    String getValueSub();
  }

  AbstractSingleListVariantTest(String shortName) {
    this.shortName = shortName;
    this.checker = new TestChecker();
    this.checker.setActualNumberOfValues(() -> data.numberOfElements);
  }

  protected static final String TOPIC_T_Empty = "t/Empty";
  protected static final String TOPIC_T_Token = "t/Token";
  protected static final String TOPIC_T_OneChild = "t/OneChild";
  protected static final String TOPIC_T_OneOpt = "t/OneOpt";
  protected static final String TOPIC_T_OneList = "t/OneList";
  protected static final String TOPIC_T_TwoChildren = "t/TwoChildren";
  protected static final String TOPIC_T_OneOfEach = "t/OneOfEach";
  protected static final String TOPIC_T_Abstract = "t/Abstract";
  protected static final String TOPIC_T_all = "t/#";

  protected TestWrapperSenderRoot senderRoot;
  protected TestWrapperReceiverRoot receiverRoot;
  protected ReceiverData data;

  private final String shortName;

  @Test
  public void checkJacksonReference() {
    testJaddContainReferenceToJackson(
        Paths.get("src", "test",
            "02-after-ragconnect", shortName, "RagConnect.jadd"), true);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException, InterruptedException {
    // late model initialization
    setInput(0);
    setShouldSetOptAndList(false);

    setupReceiverAndConnectPart();

    // connect. important: first receivers, then senders. to not miss initial value.

    // receive: unnamed
    assertTrue(receiverRoot.connectT_Empty(mqttUri(TOPIC_T_Empty)));
    assertTrue(receiverRoot.connectT_Token(mqttUri(TOPIC_T_Token)));
    assertTrue(receiverRoot.connectT_OneChild(mqttUri(TOPIC_T_OneChild)));
    assertTrue(receiverRoot.connectT_OneOpt(mqttUri(TOPIC_T_OneOpt)));
    assertTrue(receiverRoot.connectT_OneList(mqttUri(TOPIC_T_OneList)));
    assertTrue(receiverRoot.connectT_TwoChildren(mqttUri(TOPIC_T_TwoChildren)));
    assertTrue(receiverRoot.connectT_OneOfEach(mqttUri(TOPIC_T_OneOfEach)));
    assertTrue(receiverRoot.connectT_Abstract(mqttUri(TOPIC_T_Abstract)));

    // receive: named
    assertTrue(receiverRoot.connectMyEmpty(mqttUri(TOPIC_T_Empty)));

    // receive: with add
    assertTrue(receiverRoot.connectEmptyWithAdd(mqttUri(TOPIC_T_Empty)));
    assertTrue(receiverRoot.connectTokenWithAdd(mqttUri(TOPIC_T_Token)));
    assertTrue(receiverRoot.connectOneChildWithAdd(mqttUri(TOPIC_T_OneChild)));
    assertTrue(receiverRoot.connectOneOptWithAdd(mqttUri(TOPIC_T_OneOpt)));
    assertTrue(receiverRoot.connectOneListWithAdd(mqttUri(TOPIC_T_OneList)));
    assertTrue(receiverRoot.connectTwoChildrenWithAdd(mqttUri(TOPIC_T_TwoChildren)));
    assertTrue(receiverRoot.connectOneOfEachWithAdd(mqttUri(TOPIC_T_OneOfEach)));
    assertTrue(receiverRoot.connectAbstractWithAdd(mqttUri(TOPIC_T_Abstract)));

    // send
    assertTrue(senderRoot.connectT_Empty(mqttUri(TOPIC_T_Empty), isWriteCurrentValue()));
    assertTrue(senderRoot.connectT_Token(mqttUri(TOPIC_T_Token), isWriteCurrentValue()));
    assertTrue(senderRoot.connectT_OneChild(mqttUri(TOPIC_T_OneChild), isWriteCurrentValue()));
    assertTrue(senderRoot.connectT_OneOpt(mqttUri(TOPIC_T_OneOpt), isWriteCurrentValue()));
    assertTrue(senderRoot.connectT_OneList(mqttUri(TOPIC_T_OneList), isWriteCurrentValue()));
    assertTrue(senderRoot.connectT_TwoChildren(mqttUri(TOPIC_T_TwoChildren), isWriteCurrentValue()));
    assertTrue(senderRoot.connectT_OneOfEach(mqttUri(TOPIC_T_OneOfEach), isWriteCurrentValue()));
    assertTrue(senderRoot.connectT_Abstract(mqttUri(TOPIC_T_Abstract), isWriteCurrentValue()));
  }

  abstract protected void setupReceiverAndConnectPart() throws IOException;

  @Override
  protected void communicateSendInitialValue() throws IOException, InterruptedException {
    // transmissions: 8 * 1 = 8
    checker.addToNumberOfValues(8);
    checkTree(list(-0), list(0), list(-0));

    setInput(1);
    // transmissions: 8 + 8 = 16
    checker.addToNumberOfValues(8);
    checkTree(list(-1), list(0, 1), list(-0, -1));

    setInput(1);
    // transmissions: 16
    checkTree(list(-1), list(0, 1), list(-0, -1));

    setShouldSetOptAndList(true);
    // transmissions: 16 + 3 = 19
    checker.addToNumberOfValues(3);
    checkTree(list(1), list(0, 1), list(-0, -1, 1));

    setShouldSetOptAndList(true);
    // transmissions: 19
    checkTree(list(1), list(0, 1), list(-0, -1, 1));

    setInput(2);
    // transmissions: 19 + 8 = 27
    checker.addToNumberOfValues(8);
    checkTree(list(2), list(0, 1, 2), list(-0, -1, 1, 2));

    setInput(5);
    // transmissions: 27 + 8 = 35
    checker.addToNumberOfValues(8);
    checkTree(list(5), list(0, 1, 2, 5), list(-0, -1, 1, 2, 5));

    disconnectReceive();
    setInput(6); // does not affect receive
    // transmissions: 35 + 8 = 43
    checker.addToNumberOfValues(8);
    checkTree(list(5), list(0, 1, 2, 5), list(-0, -1, 1, 2, 5));

    disconnectSend();
    setInput(7); // not sent
    // transmissions: 43
    checkTree(list(5), list(0, 1, 2, 5), list(-0, -1, 1, 2, 5));
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws IOException, InterruptedException {
    // transmissions: 0
    checkTree(list(), list(), list());

    setInput(1);
    // transmissions: 8 * 1 = 0
    checker.addToNumberOfValues(8);
    checkTree(list(-1), list(1), list(-1));

    setInput(1);
    // transmissions: 8
    checkTree(list(-1), list(1), list(-1));

    setShouldSetOptAndList(true);
    // transmissions: 8 + 3 = 11
    checker.addToNumberOfValues(3);
    checkTree(list(1), list(1), list(-1, 1));

    setShouldSetOptAndList(true);
    // transmissions: 11
    checkTree(list(1), list(1), list(-1, 1));

    setInput(2);
    // transmissions: 11 + 8 = 19
    checker.addToNumberOfValues(8);
    checkTree(list(2), list(1, 2), list(-1, 1, 2));

    setInput(5);
    // transmissions: 19 + 8 = 27
    checker.addToNumberOfValues(8);
    checkTree(list(5), list(1, 2, 5), list(-1, 1, 2, 5));

    disconnectReceive();
    setInput(6); // does not affect receive
    // transmissions: 27 + 8 = 35
    checker.addToNumberOfValues(8);
    checkTree(list(5), list(1, 2, 5), list(-1, 1, 2, 5));

    disconnectSend();
    setInput(7); // not sent
    // transmissions: 35
    checkTree(list(5), list(1, 2, 5), list(-1, 1, 2, 5));
  }

  private void disconnectReceive() throws IOException {
    // receive: unnamed
    assertTrue(receiverRoot.disconnectT_Empty(mqttUri(TOPIC_T_Empty)));
    assertTrue(receiverRoot.disconnectT_Token(mqttUri(TOPIC_T_Token)));
    assertTrue(receiverRoot.disconnectT_OneChild(mqttUri(TOPIC_T_OneChild)));
    assertTrue(receiverRoot.disconnectT_OneOpt(mqttUri(TOPIC_T_OneOpt)));
    assertTrue(receiverRoot.disconnectT_OneList(mqttUri(TOPIC_T_OneList)));
    assertTrue(receiverRoot.disconnectT_TwoChildren(mqttUri(TOPIC_T_TwoChildren)));
    assertTrue(receiverRoot.disconnectT_OneOfEach(mqttUri(TOPIC_T_OneOfEach)));
    assertTrue(receiverRoot.disconnectT_Abstract(mqttUri(TOPIC_T_Abstract)));

    // receive: named
    assertTrue(receiverRoot.disconnectMyEmpty(mqttUri(TOPIC_T_Empty)));

    // receive: with add
    assertTrue(receiverRoot.disconnectEmptyWithAdd(mqttUri(TOPIC_T_Empty)));
    assertTrue(receiverRoot.disconnectTokenWithAdd(mqttUri(TOPIC_T_Token)));
    assertTrue(receiverRoot.disconnectOneChildWithAdd(mqttUri(TOPIC_T_OneChild)));
    assertTrue(receiverRoot.disconnectOneOptWithAdd(mqttUri(TOPIC_T_OneOpt)));
    assertTrue(receiverRoot.disconnectOneListWithAdd(mqttUri(TOPIC_T_OneList)));
    assertTrue(receiverRoot.disconnectTwoChildrenWithAdd(mqttUri(TOPIC_T_TwoChildren)));
    assertTrue(receiverRoot.disconnectOneOfEachWithAdd(mqttUri(TOPIC_T_OneOfEach)));
    assertTrue(receiverRoot.disconnectAbstractWithAdd(mqttUri(TOPIC_T_Abstract)));
  }

  private void disconnectSend() throws IOException {
    // send
    assertTrue(senderRoot.disconnectT_Empty(mqttUri(TOPIC_T_Empty)));
    assertTrue(senderRoot.disconnectT_Token(mqttUri(TOPIC_T_Token)));
    assertTrue(senderRoot.disconnectT_OneChild(mqttUri(TOPIC_T_OneChild)));
    assertTrue(senderRoot.disconnectT_OneOpt(mqttUri(TOPIC_T_OneOpt)));
    assertTrue(senderRoot.disconnectT_OneList(mqttUri(TOPIC_T_OneList)));
    assertTrue(senderRoot.disconnectT_TwoChildren(mqttUri(TOPIC_T_TwoChildren)));
    assertTrue(senderRoot.disconnectT_OneOfEach(mqttUri(TOPIC_T_OneOfEach)));
    assertTrue(senderRoot.disconnectT_Abstract(mqttUri(TOPIC_T_Abstract)));
  }

  protected void setInput(int input) {
    senderRoot.setInput(input);
    assertEquals(input, senderRoot.getT_Empty().getID(), "ID value of empty");
  }

  protected void setShouldSetOptAndList(boolean shouldSetOptAndList) {
    senderRoot.setShouldSetOptAndList(shouldSetOptAndList);
    assertEquals(shouldSetOptAndList, senderRoot.getT_OneOpt().hasOther(), "opt is filled or not");
  }

  /**
   * Check against expected lists of IDs.
   * If an ID is negative, do not check Opts and Lists, but use absolute value for comparison.
   * The tests starts with ID 0 and does not use opts and lists at this point, so checking with > 0 is used.
   * @param expectedList                     ids for unnamed and named ports without add
   * @param expectedWithAddList              ids for ports with add, but not those with opts and lists
   *                                         (only positive numbers can/need to be passed)
   * @param expectedWithAddListForOptAndList ids for ports with add and with opts and lists
   */
  private void checkTree(IntList expectedList,
                         IntList expectedWithAddList,
                         IntList expectedWithAddListForOptAndList) {
    checker.check();

    // check unnamed
    checkList(expectedList, receiverRoot.getT_EmptyList(), (e, n) -> {});
    checkList(expectedList, receiverRoot.getT_TokenList(),
        (e, n) -> assertEquals(Integer.toString(abs(e)), n.getValue()));
    checkList(expectedList, receiverRoot.getT_OneChildList(),
        (e, n) -> assertEquals(abs(e) + 1, n.getOther().getID()));
    checkList(expectedList, receiverRoot.getT_OneOptList(),
        (e, n) -> {
          assertEquals(e > 0, n.hasOther());
          if (n.hasOther()) {
            assertEquals(abs(e) + 1, n.getOther().getID());
          }
        });
    checkList(expectedList, receiverRoot.getT_OneListList(),
        (e, n) -> {
          assertEquals(e > 0 ? 1 : 0, n.getNumOther());
          if (n.getNumOther() > 0) {
            assertEquals(abs(e) + 1, n.getOther(0).getID());
          }
        });
    checkList(expectedList, receiverRoot.getT_TwoChildrenList(),
        (e, n) -> {
          assertEquals(abs(e) + 1, n.getLeft().getID());
          assertEquals(abs(e) + 1, n.getRight().getID());
        });
    checkList(expectedList, receiverRoot.getT_OneOfEachList(),
        (e, n) -> {
          assertEquals(abs(e) + 1, n.getFirst().getID());
          assertEquals(e > 0, n.hasSecond());
          if (n.hasSecond()) {
            assertEquals(abs(e) + 1, n.getSecond().getID());
          }
          assertEquals(e > 0 ? 1 : 0, n.getNumThird());
          if (n.getNumThird() > 0) {
            assertEquals(abs(e) + 1, n.getThird(0).getID());
          }
          assertEquals(Integer.toString(abs(e)), n.getFourth());
        });
    checkList(expectedList, receiverRoot.getT_AbstractList(),
        (e, n) -> {
          assertEquals(Integer.toString(abs(e)), n.getValueAbstract());
          assertEquals(Integer.toString(abs(e)), n.getValueSub());
        });

    // check named
    checkList(expectedList, receiverRoot.getMyEmptyList(), (e, n) -> {});

    // check with add
    checkList(expectedWithAddList, receiverRoot.getEmptyWithAddList(), (e, n) -> {});
    checkList(expectedWithAddList, receiverRoot.getTokenWithAddList(),
        (e, n) -> assertEquals(Integer.toString(abs(e)), n.getValue()));
    checkList(expectedWithAddList, receiverRoot.getOneChildWithAddList(),
        (e, n) -> assertEquals(abs(e) + 1, n.getOther().getID()));
    checkList(expectedWithAddListForOptAndList, receiverRoot.getOneOptWithAddList(),
        (e, n) -> {
          if (n.hasOther()) {
            assertEquals(abs(e) + 1, n.getOther().getID());
          }
        });
    checkList(expectedWithAddListForOptAndList, receiverRoot.getOneListWithAddList(),
        (e, n) -> {
          if (n.getNumOther() > 0) {
            assertEquals(abs(e) + 1, n.getOther(0).getID());
          }
        });
    checkList(expectedWithAddList, receiverRoot.getTwoChildrenWithAddList(),
        (e, n) -> {
          assertEquals(abs(e) + 1, n.getLeft().getID());
          assertEquals(abs(e) + 1, n.getRight().getID());
        });
    checkList(expectedWithAddListForOptAndList, receiverRoot.getOneOfEachWithAddList(),
        (e, n) -> {
          assertEquals(abs(e) + 1, n.getFirst().getID());
          if (n.hasSecond()) {
            assertEquals(abs(e) + 1, n.getSecond().getID());
          }
          if (n.getNumThird() > 0) {
            assertEquals(abs(e) + 1, n.getThird(0).getID());
          }
          assertEquals(Integer.toString(abs(e)), n.getFourth());
        });
    checkList(expectedWithAddList, receiverRoot.getAbstractWithAddList(),
        (e, n) -> {
          assertEquals(Integer.toString(abs(e)), n.getValueAbstract());
          assertEquals(Integer.toString(abs(e)), n.getValueSub());
        });
  }

  private <T extends TestWrapperNameable> void checkList(IntList expectedList, TestWrapperJastAddList<T> actualList, BiConsumer<Integer, T> additionalTest) {
    Assertions.assertThat(actualList).extracting("ID").containsExactlyElementsOf(expectedList.toAbsList());
    List<Integer> normalExpectedList = expectedList.toList();
    int index = 0;
    for (T element : actualList) {
      additionalTest.accept(normalExpectedList.get(index), element);
      index++;
    }
  }

  protected static class ReceiverData {
    int numberOfElements = 0;
  }

}
