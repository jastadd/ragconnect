package org.jastadd.ragconnect.tests.utils;

import org.assertj.core.groups.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Lean checking of constraints in tests.
 *
 * @author rschoene - Initial contribution
 */
public class TestChecker {
  private static final Logger logger = LoggerFactory.getLogger(TestChecker.class);
  private final static String NUMBER_OF_VALUES = "numberOfValues";
  public final ValuesToCompare<Object> objectValues = new ValuesToCompare<>(this);
  public final ValuesToCompare<String> stringValues = new ValuesToCompare<>(this);
  public final ValuesToCompare<Tuple> tupleValues = new ValuesToCompare<>(this);
  public final IntegerValuesToCompare intValues = new IntegerValuesToCompare(this);
  private boolean needManualWait = true;
  private boolean useManualWait = true;

  public TestChecker incNumberOfValues() {
    return intValues.incNumberOfValues();
  }

  public TestChecker addToNumberOfValues(int increment) {
    return intValues.addToNumberOfValues(increment);
  }

  public TestChecker setActualNumberOfValues(Callable<Integer> actual) {
    return intValues.setActualNumberOfValues(actual);
  }

  public TestChecker alwaysWait() {
    return setActualNumberOfValues(() -> 0);
  }

  public TestChecker put(String name, Object expected) {
    return objectValues.put(name, expected);
  }

  public TestChecker setCheckForObject(String name, BiConsumer<String, Object> check) {
    return objectValues.setCheck(name, check);
  }

  public TestChecker setActualString(String name, Callable<String> actual) {
    return stringValues.setActual(name, actual);
  }

  public TestChecker setCheckForString(String name, BiConsumer<String, String> check) {
    return stringValues.setCheck(name, check);
  }

  public TestChecker put(String name, String expected) {
    return stringValues.put(name, expected);
  }

  public TestChecker setActualTuple(String name, Callable<Tuple> actual) {
    return tupleValues.setActual(name, actual);
  }

  public TestChecker setCheckForTuple(String name, BiConsumer<String, Tuple> check) {
    return tupleValues.setCheck(name, check);
  }

  public TestChecker put(String name, Tuple expected) {
    return tupleValues.put(name, expected);
  }

  public TestChecker setActualInteger(String name, Callable<Integer> actual) {
    return intValues.setActual(name, actual);
  }

  public TestChecker setCheckForInteger(String name, BiConsumer<String, Integer> check) {
    return intValues.setCheck(name, check);
  }

  public TestChecker put(String name, Integer expected) {
    return intValues.put(name, expected);
  }

  public TestChecker disableManualWait() {
    useManualWait = false;
    needManualWait = false;
    return this;
  }

  public void check() {
    if (needManualWait) {
      try {
        TestUtils.waitForMqtt();
      } catch (InterruptedException e) {
        fail(e);
      }
    }
    intValues.get(NUMBER_OF_VALUES).checkAwait(NUMBER_OF_VALUES);

    objectValues.forEach((name, aae) -> aae.check(name));
    stringValues.forEach((name, aae) -> aae.check(name));
    tupleValues.forEach((name, aae) -> aae.check(name));
    intValues.forEach((name, aae) -> {
      if (!name.equals(NUMBER_OF_VALUES)) {
        aae.check(name);
      }
    });
    needManualWait = useManualWait;
  }

  static class ActualAndExpected<T> {
    Callable<T> actual;
    T expected;
    BiConsumer<String, T> customCheck;

    ActualAndExpected(String key) {
      expected = null;
    }

    void check(String name) {
      if (customCheck != null) {
        customCheck.accept(name, expected);
        return;
      }
      if (actual == null) {
        fail("No actual getter defined for " + name);
      }
      T actualValue = null;
      try {
        actualValue = this.actual.call();
      } catch (Exception e) {
        fail(e);
      }
      assertThat(actualValue).as(name).isEqualTo(expected);
    }

    void checkAwait(String name) {
      if (customCheck != null) {
        logger.warn("Custom check set for {}. Can't await for that.", name);
        customCheck.accept(name, expected);
        return;
      }
      if (actual == null) {
        fail("No actual getter defined for " + name);
      }
      TestUtils.awaitMqtt().alias(name + " == " + expected).until(actual, Predicate.isEqual(expected));
    }
  }

  static class ValuesToCompare<T> {
    protected final Map<String, ActualAndExpected<T>> values = new HashMap<>();
    protected final TestChecker parent;

    ValuesToCompare(TestChecker parent) {
      this.parent = parent;
    }

    public TestChecker setActual(String name, Callable<T> actual) {
      _computeIfAbsent(name).actual = actual;
      return parent;
    }

    public TestChecker setCheck(String name, BiConsumer<String, T> check) {
      _computeIfAbsent(name).customCheck = check;
      return parent;
    }

    public TestChecker put(String name, T expected) {
      _computeIfAbsent(name).expected = expected;
      return parent;
    }

    public TestChecker updateExpected(String name, Function<T, T> updater) {
      ActualAndExpected<T> aae = _computeIfAbsent(name);
      aae.expected = updater.apply(aae.expected);
      return parent;
    }

    private ActualAndExpected<T> _computeIfAbsent(String name) {
      return values.computeIfAbsent(name, ActualAndExpected::new);
    }

    ActualAndExpected<T> get(String name) {
      return values.get(name);
    }

    void forEach(BiConsumer<? super String, ? super ActualAndExpected<T>> action) {
      values.forEach(action);
    }
  }

  static class IntegerValuesToCompare extends ValuesToCompare<Integer> {
    IntegerValuesToCompare(TestChecker parent) {
      super(parent);
    }

    public TestChecker incNumberOfValues() {
      return addToNumberOfValues(1);
    }

    public TestChecker addToNumberOfValues(int increment) {
      // if there is at least one call to this, we do not need to manually wait in the next check()
      parent.needManualWait = false;
      Integer currentExpected = values.computeIfAbsent(NUMBER_OF_VALUES, ActualAndExpected::new).expected;
      return put(NUMBER_OF_VALUES, currentExpected + increment);
    }

    public TestChecker setActualNumberOfValues(Callable<Integer> actual) {
      setActual(NUMBER_OF_VALUES, actual);
      values.get(NUMBER_OF_VALUES).expected = 0;
      return parent;
    }
  }
}
