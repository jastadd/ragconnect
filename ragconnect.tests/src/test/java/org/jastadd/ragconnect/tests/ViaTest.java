package org.jastadd.ragconnect.tests;

import org.jastadd.ragconnect.tests.utils.DefaultMappings;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.jastadd.ragconnect.tests.utils.TestChecker;
import org.junit.jupiter.api.Tag;
import via.ast.A;
import via.ast.MqttHandler;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.jastadd.ragconnect.tests.utils.TestUtils.restUri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "via".
 *
 * @author rschoene - Initial contribution
 */
@Tag("rest")
public class ViaTest extends AbstractMqttTest {

  private static final int REST_PORT = 9002;

  private static final String TOPIC_MQTT_2_MQTT_RECEIVE = "mqtt2mqtt/receive";
  private static final String PATH_REST_2_REST_RECEIVE = "rest2rest/receive";
  private static final String TOPIC_MQTT_2_REST_RECEIVE = "mqtt2rest/receive";
  private static final String PATH_REST_2_MQTT_RECEIVE = "rest2mqtt/receive";
  private static final String TOPIC_BOTH_MQTT_RECEIVE = "both/send";
  private static final String PATH_BOTH_REST_RECEIVE = "both/send";

  private static final String TOPIC_MQTT_2_MQTT_SEND = "mqtt2mqtt/send";
  private static final String PATH_REST_2_REST_SEND = "rest2rest/send";
  private static final String PATH_MQTT_2_REST_SEND = "mqtt2rest/send";
  private static final String TOPIC_REST_2_MQTT_SEND = "rest2mqtt/send";
  private static final String TOPIC_BOTH_2_MQTT_SEND = "both2mqtt/send";
  private static final String PATH_BOTH_2_REST_SEND = "both2rest/send";

  private static final String REST_SERVER_BASE_URL = "http://localhost:" + REST_PORT + "/";

  private static final String NOT_MAPPED = "<html><body><h2>404 Not found</h2></body></html>";

  private MqttHandler handler;
  private TestChecker checker;
  private A model;
  private ReceiverData dataMqtt2Mqtt;
  private ReceiverData dataRest2Mqtt;
  private WebTarget dataRest2Rest;
  private WebTarget dataMqtt2Rest;
  private ReceiverData dataBoth2Mqtt;
  private WebTarget dataBoth2Rest;

  private WebTarget senderRest2Rest;
  private WebTarget senderRest2Mqtt;
  private WebTarget senderBoth2Rest;

  @Override
  protected void createModel() {
    // Setting value for Input without dependencies does not trigger any updates
    model = new A();
    model.setMqtt2MqttInput("100");
    model.setRest2RestInput("200");
    model.setMqtt2RestInput("300");
    model.setRest2MqttInput("400");
    model.setBoth2BothInput("500");
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    model.addDependencyMqtt2Mqtt(model);
    model.addDependencyRest2Rest(model);
    model.addDependencyMqtt2Rest(model);
    model.addDependencyRest2Mqtt(model);
    model.addDependencyBoth2Mqtt(model);
    model.addDependencyBoth2Rest(model);

    dataMqtt2Mqtt = new ReceiverData();
    dataRest2Mqtt = new ReceiverData();
    dataBoth2Mqtt = new ReceiverData();

    handler.newConnection(TOPIC_MQTT_2_MQTT_SEND, bytes -> {
      dataMqtt2Mqtt.numberOfStringValues += 1;
      dataMqtt2Mqtt.lastStringValue = DefaultMappings.BytesToString(bytes);
    });
    handler.newConnection(TOPIC_REST_2_MQTT_SEND, bytes -> {
      dataRest2Mqtt.numberOfStringValues += 1;
      dataRest2Mqtt.lastStringValue = DefaultMappings.BytesToString(bytes);
    });
    handler.newConnection(TOPIC_BOTH_2_MQTT_SEND, bytes -> {
      dataBoth2Mqtt.numberOfStringValues += 1;
      dataBoth2Mqtt.lastStringValue = DefaultMappings.BytesToString(bytes);
    });

    checker = new TestChecker();
    checker.setActualNumberOfValues(() -> dataMqtt2Mqtt.numberOfStringValues);

    Client client = ClientBuilder.newClient();
    dataRest2Rest = client.target(REST_SERVER_BASE_URL + PATH_REST_2_REST_SEND);
    dataMqtt2Rest = client.target(REST_SERVER_BASE_URL + PATH_MQTT_2_REST_SEND);
    dataBoth2Rest = client.target(REST_SERVER_BASE_URL + PATH_BOTH_2_REST_SEND);
    senderRest2Rest = client.target(REST_SERVER_BASE_URL + PATH_REST_2_REST_RECEIVE);
    senderRest2Mqtt = client.target(REST_SERVER_BASE_URL + PATH_REST_2_MQTT_RECEIVE);
    senderBoth2Rest = client.target(REST_SERVER_BASE_URL + PATH_BOTH_REST_RECEIVE);

    assertTrue(model.connectMqtt2MqttInput(mqttUri(TOPIC_MQTT_2_MQTT_RECEIVE)));
    assertTrue(model.connectMqtt2MqttOutput(mqttUri(TOPIC_MQTT_2_MQTT_SEND), isWriteCurrentValue()));
    assertTrue(model.connectMqtt2RestInput(mqttUri(TOPIC_MQTT_2_REST_RECEIVE)));
    assertTrue(model.connectMqtt2RestOutput(restUri(PATH_MQTT_2_REST_SEND, REST_PORT), isWriteCurrentValue()));
    assertTrue(model.connectRest2MqttInput(restUri(PATH_REST_2_MQTT_RECEIVE, REST_PORT)));
    assertTrue(model.connectRest2MqttOutput(mqttUri(TOPIC_REST_2_MQTT_SEND), isWriteCurrentValue()));
    assertTrue(model.connectRest2RestInput(restUri(PATH_REST_2_REST_RECEIVE, REST_PORT)));
    assertTrue(model.connectRest2RestOutput(restUri(PATH_REST_2_REST_SEND, REST_PORT), isWriteCurrentValue()));
    assertTrue(model.connectBoth2BothInput(mqttUri(TOPIC_BOTH_MQTT_RECEIVE)));
    assertTrue(model.connectBoth2BothInput(restUri(PATH_BOTH_REST_RECEIVE, REST_PORT)));
    assertTrue(model.connectBoth2MqttOutput(mqttUri(TOPIC_BOTH_2_MQTT_SEND), isWriteCurrentValue()));
    assertTrue(model.connectBoth2RestOutput(restUri(PATH_BOTH_2_REST_SEND, REST_PORT), isWriteCurrentValue()));
  }

  @Override
  protected void communicateSendInitialValue() throws InterruptedException, IOException {
    // check initial value
    checker.addToNumberOfValues(1);
    checkData(1, "100-M2M-ToMqtt",
        "200-R2R-ToRest",
        "300-M2R-ToRest",
        1, "400-R2M-ToMqtt",
        1, "500-B2M-ToMqtt",
        "500-B2R-ToRest");

    communicateBoth(1);
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException, IOException {
    // check initial value
    checkData(0, null,
            "200-R2R-ToRest",
            "300-M2R-ToRest",
            0, null,
            0, null,
            "500-B2R-ToRest");

    communicateBoth(0);
  }

  private void communicateBoth(int initialNumberOfValues) throws IOException {
    sendData("111", "211", "311", "411");
    sendDataForBoth("511", true);
    checker.incNumberOfValues();
    checkData(initialNumberOfValues + 1, "FromMqtt-111-M2M-ToMqtt",
        "FromRest-211-R2R-ToRest",
        "FromMqtt-311-M2R-ToRest",
        initialNumberOfValues + 1, "FromRest-411-R2M-ToMqtt",
        initialNumberOfValues + 1, "511-B2M-ToMqtt",
        "511-B2R-ToRest");

    // send value only for bothInput via REST
    sendDataForBoth("512", false);
    checkData(initialNumberOfValues + 1, "FromMqtt-111-M2M-ToMqtt",
        "FromRest-211-R2R-ToRest",
        "FromMqtt-311-M2R-ToRest",
        initialNumberOfValues + 1, "FromRest-411-R2M-ToMqtt",
        initialNumberOfValues + 2, "512-B2M-ToMqtt",
        "512-B2R-ToRest");

    // send same value only for bothInput via MQTT
    sendDataForBoth("512", true);
    checkData(initialNumberOfValues + 1, "FromMqtt-111-M2M-ToMqtt",
        "FromRest-211-R2R-ToRest",
        "FromMqtt-311-M2R-ToRest",
        initialNumberOfValues + 1, "FromRest-411-R2M-ToMqtt",
        initialNumberOfValues + 2, "512-B2M-ToMqtt",
        "512-B2R-ToRest");

    // send values for other things
    sendData("112", "212", "312", "412");
    checker.incNumberOfValues();
    checkData(initialNumberOfValues + 2, "FromMqtt-112-M2M-ToMqtt",
        "FromRest-212-R2R-ToRest",
        "FromMqtt-312-M2R-ToRest",
        initialNumberOfValues + 2, "FromRest-412-R2M-ToMqtt",
        initialNumberOfValues + 2, "512-B2M-ToMqtt",
        "512-B2R-ToRest");

    // send same values again for other things
    sendData("112", "212", "312", "412");
    checkData(initialNumberOfValues + 2, "FromMqtt-112-M2M-ToMqtt",
        "FromRest-212-R2R-ToRest",
        "FromMqtt-312-M2R-ToRest",
        initialNumberOfValues + 2, "FromRest-412-R2M-ToMqtt",
        initialNumberOfValues + 2, "512-B2M-ToMqtt",
        "512-B2R-ToRest");

    // send 503 over mqtt while disconnected should not change anything
    assertTrue(model.disconnectBoth2BothInput(mqttUri(TOPIC_BOTH_MQTT_RECEIVE)));
    sendDataForBoth("513", true);
    checkData(initialNumberOfValues + 2, "FromMqtt-112-M2M-ToMqtt",
        "FromRest-212-R2R-ToRest",
        "FromMqtt-312-M2R-ToRest",
        initialNumberOfValues + 2, "FromRest-412-R2M-ToMqtt",
        initialNumberOfValues + 2, "512-B2M-ToMqtt",
        "512-B2R-ToRest");

    // send 514 over rest while still connected should update
    sendDataForBoth("514", false);
    checkData(initialNumberOfValues + 2, "FromMqtt-112-M2M-ToMqtt",
        "FromRest-212-R2R-ToRest",
        "FromMqtt-312-M2R-ToRest",
        initialNumberOfValues + 2, "FromRest-412-R2M-ToMqtt",
        initialNumberOfValues + 3, "514-B2M-ToMqtt",
        "514-B2R-ToRest");

    // send 515 over rest while also disconnected should not change anything
    assertTrue(model.disconnectBoth2BothInput(restUri(PATH_BOTH_REST_RECEIVE, REST_PORT)));
    sendDataForBoth("515", false);
    checkData(initialNumberOfValues + 2, "FromMqtt-112-M2M-ToMqtt",
        "FromRest-212-R2R-ToRest",
        "FromMqtt-312-M2R-ToRest",
        initialNumberOfValues + 2, "FromRest-412-R2M-ToMqtt",
        initialNumberOfValues + 3, "514-B2M-ToMqtt",
        "514-B2R-ToRest");

    // send new values. value over rest while sender disconnected does not provide a value anymore
    assertTrue(model.disconnectRest2RestOutput(restUri(PATH_REST_2_REST_SEND, REST_PORT)));
    assertTrue(model.disconnectMqtt2RestOutput(restUri(PATH_MQTT_2_REST_SEND, REST_PORT)));
    sendData("113", "213", "313", "413");
    checker.incNumberOfValues();
    checkData(initialNumberOfValues + 3, "FromMqtt-113-M2M-ToMqtt",
        NOT_MAPPED,
        NOT_MAPPED,
        initialNumberOfValues + 3, "FromRest-413-R2M-ToMqtt",
        initialNumberOfValues + 3, "514-B2M-ToMqtt",
        "514-B2R-ToRest");
  }

  @Override
  public void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  private void sendData(String inputMqtt2Mqtt, String inputRest2Rest, String inputMqtt2Rest, String inputRest2Mqtt) {
    publisher.publish(TOPIC_MQTT_2_MQTT_RECEIVE, inputMqtt2Mqtt.getBytes());
    senderRest2Rest.request().put(Entity.entity(inputRest2Rest, MediaType.TEXT_PLAIN_TYPE));
    publisher.publish(TOPIC_MQTT_2_REST_RECEIVE, inputMqtt2Rest.getBytes());
    senderRest2Mqtt.request().put(Entity.entity(inputRest2Mqtt, MediaType.TEXT_PLAIN_TYPE));
  }

  private void sendDataForBoth(String input, boolean useMqtt) {
    if (useMqtt) {
      publisher.publish(TOPIC_BOTH_MQTT_RECEIVE, input.getBytes());
    } else {
      senderBoth2Rest.request().put(Entity.entity(input, MediaType.TEXT_PLAIN_TYPE));
    }
  }

  private void checkData(int numberOfMqtt2MqttValues, String mqtt2MqttValue, String rest2RestValue, String mqtt2RestValue, int numberOfRest2MqttValues, String rest2MqttValue, int numberOfBoth2MqttValues, String both2MqttValue, String both2RestValue) {
    checker.check();
    dataMqtt2Mqtt.assertEqualData(numberOfMqtt2MqttValues, mqtt2MqttValue);
    dataRest2Mqtt.assertEqualData(numberOfRest2MqttValues, rest2MqttValue);
    dataBoth2Mqtt.assertEqualData(numberOfBoth2MqttValues, both2MqttValue);
    assertEquals(rest2RestValue, readRest2Rest());
    assertEquals(mqtt2RestValue, readMqtt2Rest());
    assertEquals(both2RestValue, readBoth2Rest());
  }

  private String readRest2Rest() {
    return dataRest2Rest.request().get().readEntity(String.class);
  }

  private String readMqtt2Rest() {
    return dataMqtt2Rest.request().get().readEntity(String.class);
  }

  private String readBoth2Rest() {
    return dataBoth2Rest.request().get().readEntity(String.class);
  }

  private static class ReceiverData {
    String lastStringValue;
    int numberOfStringValues = 0;

    public void assertEqualData(int expectedNumberOfValues, String expectedLastValue) {
      assertEquals(expectedNumberOfValues, this.numberOfStringValues);
      assertEquals(expectedLastValue, this.lastStringValue);
    }
  }
}
