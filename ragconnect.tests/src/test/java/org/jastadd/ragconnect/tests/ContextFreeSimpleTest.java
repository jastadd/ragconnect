package org.jastadd.ragconnect.tests;

import contextFreeSimpleInc.ast.A;
import contextFreeSimpleInc.ast.MqttHandler;
import contextFreeSimpleInc.ast.Root;
import contextFreeSimpleInc.ast.SerializationException;
import org.jastadd.ragconnect.tests.utils.DefaultMappings;
import org.jastadd.ragconnect.tests.utils.TestChecker;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Tag;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test case "context free simple".
 *
 * @author rschoene - Initial contribution
 */
@Tag("Incremental")
public class ContextFreeSimpleTest extends AbstractMqttTest {

  private static final String TOPIC_WILDCARD = "context-free/#";
  private static final String TOPIC_UNNAMED = "context-free/unnamed";
  private static final String TOPIC_SINGLE = "context-free/single";
  private static final String TOPIC_SINGLE_ALTERNATIVE = "context-free/double";
  private static final String TOPIC_OPT = "context-free/opt";
  private static final String TOPIC_LIST_1 = "context-free/list1";
  private static final String TOPIC_LIST_2 = "context-free/list2";

  /** Use initially created members as values in {@link #check(String, String, A, A)} */
  private static final String INITIAL_VALUE = null;

  private Root model;
  private A unnamedA;
  private A singleA;
  private A optA;
  private A listA1;
  private A listA2;

  private ReceiverData data;
  private MqttHandler handler;
  private TestChecker checker;

  @Override
  protected void createModel() {
    model = new Root();
    unnamedA = new A().setValue("unnamed");
    singleA = new A().setValue("single");
    optA = new A().setValue("opt");
    listA1 = new A().setValue("a1");
    listA2 = new A().setValue("a2");

    model.setA(unnamedA);
    model.setSingleA(singleA);
    model.setOptA(optA);
    model.addListA(listA1);
    model.addListA(listA2);

    data = new ReceiverData();
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException, InterruptedException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    handler.newConnection(TOPIC_WILDCARD, (topic, bytes) -> data.valuesSent += 1);

    checker = new TestChecker();
    checker.alwaysWait()
            .setCheckForString(TOPIC_UNNAMED, (name, expected) -> this.check(name, expected, model.getA(), unnamedA))
            .setCheckForString(TOPIC_SINGLE, (name, expected) -> this.check(name, expected, model.getSingleA(), singleA))
            .setCheckForString(TOPIC_OPT, (name, expected) -> this.check(name, expected, model.getOptA(), optA))
            .setCheckForString(TOPIC_LIST_1, (name, expected) -> this.check(name, expected, model.getListA(0), listA1))
            .setCheckForString(TOPIC_LIST_2, (name, expected) -> this.check(name, expected, model.getListA(1), listA2));

    assertTrue(unnamedA.connect(mqttUri(TOPIC_UNNAMED)));
    assertTrue(singleA.connect(mqttUri(TOPIC_SINGLE)));
    assertTrue(singleA.connect(mqttUri(TOPIC_SINGLE_ALTERNATIVE)));
    assertTrue(optA.connect(mqttUri(TOPIC_OPT)));
    assertTrue(listA1.connect(mqttUri(TOPIC_LIST_1)));
    assertTrue(listA2.connect(mqttUri(TOPIC_LIST_2)));
  }

  @Override
  protected void communicateSendInitialValue() throws IOException, InterruptedException {
    // empty
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws IOException, InterruptedException {
    checker.put(TOPIC_UNNAMED, INITIAL_VALUE)
            .put(TOPIC_SINGLE, INITIAL_VALUE)
            .put(TOPIC_OPT, INITIAL_VALUE)
            .put(TOPIC_LIST_1, INITIAL_VALUE)
            .put(TOPIC_LIST_2, INITIAL_VALUE);

    checker.check();

    send(TOPIC_UNNAMED, "1");
    checker.put(TOPIC_UNNAMED, "1").check();

    send(TOPIC_SINGLE, "2");
    checker.put(TOPIC_SINGLE, "pre2").check();

    send(TOPIC_SINGLE, "2.1");
    checker.put(TOPIC_SINGLE, "pre2.1").check();

    send(TOPIC_SINGLE, "2.2");
    checker.put(TOPIC_SINGLE, "pre2.2").check();

    send(TOPIC_OPT, "3");
    checker.put(TOPIC_OPT, "3post").check();

    send(TOPIC_LIST_1, "4");
    checker.put(TOPIC_LIST_1, "4").check();

    send(TOPIC_LIST_2, "5");
    checker.put(TOPIC_LIST_2, "5").check();

    send(TOPIC_SINGLE_ALTERNATIVE, "fix");
    checker.put(TOPIC_SINGLE, "prefix").check();

    assertTrue(model.getSingleA().disconnect(mqttUri(TOPIC_SINGLE)));
    send(TOPIC_SINGLE, "6");
    // no change to previous check since disconnected
    checker.check();

    send(TOPIC_SINGLE_ALTERNATIVE, "7");
    // alternative topic is still active
    checker.put(TOPIC_SINGLE, "pre7").check();

    assertTrue(model.getSingleA().disconnect(mqttUri(TOPIC_SINGLE_ALTERNATIVE)));
    send(TOPIC_SINGLE_ALTERNATIVE, "8");
    // no change to previous check since alternative topic is also disconnected now
    checker.check();
  }

  private void send(String topic, String value) throws IOException {
    A a = new A().setValue(value);
    try {
      publisher.publish(topic, DefaultMappings.TreeToBytes(a::serialize));
    } catch (SerializationException e) {
      throw new IOException(e);
    }
  }

  private void check(String name, String expected, A actual, A expectedIfInitial) {
    if (Objects.equals(INITIAL_VALUE, expected)) {
      assertEquals(expectedIfInitial, actual, name);
    } else {
      assertNotEquals(expectedIfInitial, actual, name);
      assertEquals(expected, actual.getValue(), name);
    }
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  static class ReceiverData {
    int valuesSent = 0;
  }
}
