package org.jastadd.ragconnect.tests;

import org.jastadd.ragconnect.tests.utils.TestChecker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import restClientServer.ast.*;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Testing RestClient.
 *
 * @author rschoene - Initial contribution
 */
public class RestClientServerTest extends RagConnectTest {
  private Root root;
  private TestChecker checker;

  private static final String TOPIC_NT_A_VALUE = "nt/a/value";
  private static final String TOPIC_NT_A_INNER_VALUE = "nt/a/inner/value";

  @Test
  public void testSimpleSenderRest() throws IOException {
    root.getSenderRoot().setSimpleValue(1);

    assertThat(root.getReceiverRoot().connectReceivedValue("restClient://localhost:4567/serve-simple")).isTrue();
    assertThat(root.getSenderRoot().connectSimpleValue("rest://localhost:4567/serve-simple", true)).isTrue();

    assertThat(root.getReceiverRoot().getReceivedValue()).isEqualTo(1);

    root.getSenderRoot().setSimpleValue(2);
    assertThat(root.getReceiverRoot().getReceivedValue()).isEqualTo(2);
  }

  @Test
  public void testSimpleSenderRestClient() throws IOException {
    root.getSenderRoot().setSimpleValue(1);

    assertThat(root.getReceiverRoot().connectReceivedValue("rest://localhost:4567/put-simple")).isTrue();
    assertThat(root.getSenderRoot().connectSimpleValue("restClient://localhost:4567/put-simple", true)).isTrue();

    assertThat(root.getReceiverRoot().getReceivedValue()).isEqualTo(1);

    root.getSenderRoot().setSimpleValue(2);
    assertThat(root.getReceiverRoot().getReceivedValue()).isEqualTo(2);
  }

  @Test
  public void testNonterminalSenderRest() throws IOException {
    A a = new A().setValue("a1");
    a.setInner(new Inner().setInnerValue("1"));
    root.getSenderRoot().setSingleA(a);

    checker.put(TOPIC_NT_A_VALUE, "a1")
            .put(TOPIC_NT_A_INNER_VALUE, "1");

    assertThat(root.getReceiverRoot().connectSomeA("rest://localhost:4567/put-nt")).isTrue();
    assertThat(root.getSenderRoot().connectSingleA("restClient://localhost:4567/put-nt", true)).isTrue();

    communicateNonterminal();
  }

  @Test
  @Disabled("Receiving nonterminals using restClient is not supported yet")
  public void testNonterminalSenderRestClient() throws IOException {
    A a = new A().setValue("a1");
    a.setInner(new Inner().setInnerValue("1"));
    root.getSenderRoot().setSingleA(a);

    assertThat(root.getReceiverRoot().connectSomeA("restClient://localhost:4567/serve-nt")).isTrue();
    assertThat(root.getSenderRoot().connectSingleA("rest://localhost:4567/serve-nt", true)).isTrue();

    communicateNonterminal();
  }

  private void communicateNonterminal() {
    A a = root.getSenderRoot().getSingleA();

    checker.check();

    a.setValue("a23");
    checker.put(TOPIC_NT_A_VALUE, "a23").check();

    a.getInner().setInnerValue("abc");
    checker.put(TOPIC_NT_A_INNER_VALUE, "abc").check();
  }

  @BeforeEach
  public void createModel() {
    root = new Root();
    root.setReceiverRoot(new ReceiverRoot());
    root.setSenderRoot(new SenderRoot());

    checker = new TestChecker().setActualNumberOfValues(() -> 0);
    checker.setCheckForString(TOPIC_NT_A_VALUE, (name, expected) ->
                    assertThat(someAValue()).describedAs(name).isEqualTo(expected))
            .setCheckForString(TOPIC_NT_A_INNER_VALUE, (name, expected) ->
                    assertThat(someAInnerValueOrNull()).describedAs(name).isEqualTo(expected))
    ;
  }

  private String someAValue() {
    if (root.getReceiverRoot().getSomeA() == null) {
      return null;
    }
    return root.getReceiverRoot().getSomeA().getValue();
  }

  private String someAInnerValueOrNull() {
    if (root.getReceiverRoot().getSomeA() == null || root.getReceiverRoot().getSomeA().getInner() == null) {
      return null;
    }
    return root.getReceiverRoot().getSomeA().getInner().getInnerValue();
  }

  @AfterEach
  public void close() {
    root.ragconnectCloseConnections();
  }
}
