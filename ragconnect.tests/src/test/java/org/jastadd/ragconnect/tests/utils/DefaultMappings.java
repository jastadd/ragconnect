package org.jastadd.ragconnect.tests.utils;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * TODO: Add description.
 *
 * @author rschoene - Initial contribution
 */
public class DefaultMappings {
  @SuppressWarnings("rawtypes")
  static class ReadNode extends defaultOnlyRead.ast.ASTNode {
    public boolean DefaultBytesToBooleanMapping(byte[] input) throws Exception {
      return _ragconnect__apply__DefaultBytesToBooleanMapping(input);
    }

    public int DefaultBytesToIntMapping(byte[] input) throws Exception {
      return _ragconnect__apply__DefaultBytesToIntMapping(input);
    }

    public short DefaultBytesToShortMapping(byte[] input) throws Exception {
      return _ragconnect__apply__DefaultBytesToShortMapping(input);
    }

    public long DefaultBytesToLongMapping(byte[] input) throws Exception {
      return _ragconnect__apply__DefaultBytesToLongMapping(input);
    }

    public float DefaultBytesToFloatMapping(byte[] input) throws Exception {
      return _ragconnect__apply__DefaultBytesToFloatMapping(input);
    }

    public double DefaultBytesToDoubleMapping(byte[] input) throws Exception {
      return _ragconnect__apply__DefaultBytesToDoubleMapping(input);
    }

    public char DefaultBytesToCharMapping(byte[] input) throws Exception {
      return _ragconnect__apply__DefaultBytesToCharMapping(input);
    }

    public String DefaultBytesToStringMapping(byte[] input) throws Exception {
      return _ragconnect__apply__DefaultBytesToStringMapping(input);
    }
  }

  @SuppressWarnings("rawtypes")
  static class WriteNode extends defaultOnlyWrite.ast.ASTNode {
    public byte[] DefaultBooleanToBytesMapping(boolean input) throws Exception {
      return _ragconnect__apply__DefaultBooleanToBytesMapping(input);
    }

    public byte[] DefaultIntToBytesMapping(int input) throws Exception {
      return _ragconnect__apply__DefaultIntToBytesMapping(input);
    }

    public byte[] DefaultShortToBytesMapping(short input) throws Exception {
      return _ragconnect__apply__DefaultShortToBytesMapping(input);
    }

    public byte[] DefaultLongToBytesMapping(long input) throws Exception {
      return _ragconnect__apply__DefaultLongToBytesMapping(input);
    }

    public byte[] DefaultFloatToBytesMapping(float input) throws Exception {
      return _ragconnect__apply__DefaultFloatToBytesMapping(input);
    }

    public byte[] DefaultDoubleToBytesMapping(double input) throws Exception {
      return _ragconnect__apply__DefaultDoubleToBytesMapping(input);
    }

    public byte[] DefaultCharToBytesMapping(char input) throws Exception {
      return _ragconnect__apply__DefaultCharToBytesMapping(input);
    }

    public byte[] DefaultStringToBytesMapping(String input) throws Exception {
      return _ragconnect__apply__DefaultStringToBytesMapping(input);
    }
  }

  @FunctionalInterface
  public interface SerializeFunction<E extends Throwable> {
    void accept(JsonGenerator g, String fieldName) throws E;
  }

  static ReadNode readNode = new ReadNode();
  static WriteNode writeNode = new WriteNode();

  public static boolean BytesToBool(byte[] input) {
    try {
      return readNode.DefaultBytesToBooleanMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }

  public static int BytesToInt(byte[] input) {
    try {
      return readNode.DefaultBytesToIntMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static short BytesToShort(byte[] input) {
    try {
      return readNode.DefaultBytesToShortMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static long BytesToLong(byte[] input) {
    try {
      return readNode.DefaultBytesToLongMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static float BytesToFloat(byte[] input) {
    try {
      return readNode.DefaultBytesToFloatMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static double BytesToDouble(byte[] input) {
    try {
      return readNode.DefaultBytesToDoubleMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static char BytesToChar(byte[] input) {
    try {
      return readNode.DefaultBytesToCharMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static String BytesToString(byte[] input) {
    try {
      return readNode.DefaultBytesToStringMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static byte[] BoolToBytes(boolean input) {
    try {
      return writeNode.DefaultBooleanToBytesMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static byte[] IntToBytes(int input) {
    try {
      return writeNode.DefaultIntToBytesMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static byte[] ShortToBytes(short input) {
    try {
      return writeNode.DefaultShortToBytesMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static byte[] LongToBytes(long input) {
    try {
      return writeNode.DefaultLongToBytesMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static byte[] FloatToBytes(float input) {
    try {
      return writeNode.DefaultFloatToBytesMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static byte[] DoubleToBytes(double input) {
    try {
      return writeNode.DefaultDoubleToBytesMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static byte[] CharToBytes(char input) {
    try {
      return writeNode.DefaultCharToBytesMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static byte[] StringToBytes(String input) {
    try {
      return writeNode.DefaultStringToBytesMapping(input);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static <E extends Throwable> byte[] TreeToBytes(SerializeFunction<E> serializeFunction) throws E, IOException {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    JsonFactory factory = new JsonFactory();
    JsonGenerator generator = factory.createGenerator(outputStream, JsonEncoding.UTF8);
    serializeFunction.accept(generator, null);
    generator.flush();
    return outputStream.toString().getBytes();
  }
}
