package org.jastadd.ragconnect.tests.utils;

import org.awaitility.Awaitility;
import org.awaitility.core.ConditionFactory;
import org.jastadd.ragconnect.compiler.Compiler;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Utility methods for tests.
 *
 * @author rschoene - Initial contribution
 */
public class TestUtils {

  private static final Logger logger = LoggerFactory.getLogger(TestUtils.class);
  public static final double DELTA = 0.001d;
  public static final String INPUT_DIRECTORY_PREFIX = "./src/test/01-input/";
  public static final String OUTPUT_DIRECTORY_PREFIX = "./src/test/02-after-ragconnect/";

  public static boolean isCi() {
    return System.getenv("GITLAB_CI") != null;
  }

  public static String getMqttHost() {
    if (isCi()) {
      // we are in the CI, so use "mqtt" as host
      return "mqtt";
    } {
      // else assume a locally running mqtt broker
      return "localhost";
    }
  }

  public static String mqttUri(String path) {
    return "mqtt://" + getMqttHost() + "/" + path;
  }

  public static String restUri(String path, int port) {
    return "rest://localhost:" + port + "/" + path;
  }

  public static String javaUri(String path) {
    return "java://localhost/" + path;
  }

  public static int getMqttDefaultPort() {
    return 1883;
  }

  public static Path runCompiler(String grammarFile, Iterable<String> connectFiles, String rootNode, String outputDirectory, int expectedReturnValue, String... additionalArguments) {

    assertThat(connectFiles).isNotEmpty();

    Path outPath = Paths.get(OUTPUT_DIRECTORY_PREFIX)
        .resolve(outputDirectory)
        .resolve("Compiler.out");
    ensureCreated(outPath.getParent());

    try {
      logger.debug("user.dir: {}", System.getProperty("user.dir"));
      List<String> args = new ArrayList<>() {{
        add("--o=" + OUTPUT_DIRECTORY_PREFIX + outputDirectory);
        add("--rootNode=" + rootNode);
        add("--verbose");
        add(INPUT_DIRECTORY_PREFIX + grammarFile);
      }};
      connectFiles.forEach(connectFile -> args.add(INPUT_DIRECTORY_PREFIX + connectFile));
      args.addAll(Arrays.asList(additionalArguments));

      int returnValue = exec(Compiler.class, args.toArray(new String[0]), outPath.toFile());
      Assertions.assertEquals(expectedReturnValue, returnValue, "RagConnect did not return with value " + expectedReturnValue);
    } catch (IOException | InterruptedException e) {
      fail(e);
    }
    return outPath;
  }

  public static <T> String prettyPrint(Iterable<T> aList, Function<T, String> elementPrinter) {
    StringJoiner sj = new StringJoiner(", ", "[", "]");
    aList.forEach(element -> sj.add(elementPrinter.apply(element)));
    return sj.toString();
  }

  public static void assertLinesMatch(String directory, String expectedName, String out) throws IOException {
    Path expectedPath = Paths.get(TestUtils.INPUT_DIRECTORY_PREFIX)
            .resolve(directory)
            .resolve(expectedName + ".expected");
    String expected = readFile(expectedPath, Charset.defaultCharset());
    List<String> outList = Arrays.asList(out.split("\n"));
    Collections.sort(outList);
    List<String> expectedList = Arrays.stream(expected.split("\n"))
            .sorted()
            .filter(s -> !s.isEmpty() && !s.startsWith("//"))
            .collect(Collectors.toList());

    Assertions.assertLinesMatch(expectedList, outList);
  }

  private static void ensureCreated(Path directory) {
    File directoryFile = directory.toFile();
    if (directoryFile.exists() && directoryFile.isDirectory()) {
      return;
    }
    assertTrue(directoryFile.mkdirs());
  }

  public static int exec(Class<?> klass, String[] args, File err) throws IOException,
      InterruptedException {
    String javaHome = System.getProperty("java.home");
    String javaBin = javaHome + File.separator + "bin" + File.separator + "java";
    String classpath = System.getProperty("java.class.path");
    String className = klass.getName();

    String[] newArgs = new String[args.length + 4];
    newArgs[0] = javaBin;
    newArgs[1] = "-cp";
    newArgs[2] = classpath;
    newArgs[3] = className;
    System.arraycopy(args, 0, newArgs, 4, args.length);

    ProcessBuilder builder = new ProcessBuilder(newArgs);
//    builder.redirectOutput(err);
    builder.redirectError(err);

    Process process = builder.start();
    process.waitFor();
    return process.exitValue();
  }

  public static void testJaddContainReferenceToJackson(Path path, boolean shouldContain) {
    try {
      String content = Files.readString(path);
      boolean actualContain = content.contains("com.fasterxml.jackson.databind.ObjectMapper");
      if (actualContain && !shouldContain) {
        fail(path + " should not depend on jackson library, but does");
      }
      if (!actualContain && shouldContain) {
        fail(path + " does not depend on jackson library");
      }
    } catch (IOException e) {
      fail(e);
    }
  }

  public static String readFile(Path path, Charset encoding)
      throws IOException {
    byte[] encoded = Files.readAllBytes(path);
    return new String(encoded, encoding);
  }

  public static void waitForMqtt() throws InterruptedException {
    TimeUnit.MILLISECONDS.sleep(1500);
  }

  public static ConditionFactory awaitMqtt() {
    return Awaitility.await().atMost(1500, TimeUnit.MILLISECONDS);
  }

  static <T_Event, T_ASTNode> void logEvent(T_Event event, T_ASTNode node, String attribute, Object params, Object value) {
    logger.info("event: {}, node: {}, attribute: {}, params: {}, value: {}",
            event, node, attribute, params, value);
  }

}
