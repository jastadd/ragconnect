package org.jastadd.ragconnect.tests;

import tutorial.ast.A;
import tutorial.ast.B;

import java.io.IOException;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Testcase "Tutorial".
 *
 * @author rschoene - Initial contribution
 */
public class TutorialTest extends AbstractMqttTest {

  private A a;
  private B b1;
  private B b2;

  @Override
  protected void createModel() {
    a = new A();
    // set some default value for input
    a.setInput("");
    b1 = new B();
    b2 = new B();
    a.addB(b1);
    a.addB(b2);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    // a.OutputOnA -> a.Input
    a.addDependencyA(a);
    // b1.OutputOnB -> a.Input
    b1.addDependencyB(a);
    // b2.OutputOnB -> a.Input
    b2.addDependencyB(a);

    assertTrue(a.connectInput(mqttUri("topic/for/input")));
    assertTrue(a.connectOutputOnA(mqttUri("a/out"), true));
    assertTrue(b1.connectOutputOnB(mqttUri("b1/out"), true));
    assertTrue(b2.connectOutputOnB(mqttUri("b2/out"), false));
  }

  @Override
  protected void communicateSendInitialValue() {
    // empty
  }

  @Override
  protected void communicateOnlyUpdatedValue() {
    // empty
  }

  @Override
  protected void closeConnections() {
    if (a != null) {
      a.ragconnectCloseConnections();
    }
  }
}
