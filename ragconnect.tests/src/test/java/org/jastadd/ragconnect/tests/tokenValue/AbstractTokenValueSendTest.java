package org.jastadd.ragconnect.tests.tokenValue;

import org.jastadd.ragconnect.tests.AbstractMqttTest;
import org.jastadd.ragconnect.tests.utils.DefaultMappings;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.jastadd.ragconnect.tests.utils.TestChecker;
import org.junit.jupiter.api.Test;
import tokenValueSend.ast.MqttHandler;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.jastadd.ragconnect.tests.utils.TestUtils.testJaddContainReferenceToJackson;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test case "tokenValueSend".
 *
 * @author rschoene - Initial contribution
 */
public abstract class AbstractTokenValueSendTest extends AbstractMqttTest {
  protected static final String TOPIC_SEND_ONE = "one/value/out";

  protected static final String TOPIC_RECEIVE_TWO = "two/value/in";
  protected static final String TOPIC_SEND_TWO = "two/value/out";

  protected static final String TOPIC_RECEIVE_THREE_VALUE = "three/value/in";
  protected static final String TOPIC_SEND_THREE_VALUE = "three/value/out";
  protected static final String TOPIC_SEND_THREE_OTHER = "three/other/out";

  protected static final String INITIAL_VALUE = "Start";

  protected MqttHandler handler;
  protected TestChecker checker;

  protected final String shortName;

  private ReceiverData dataOne;
  private ReceiverData dataTwo;
  private ReceiverData dataThree;
  private ReceiverData dataThreeOther;

  AbstractTokenValueSendTest(String shortName) {
    this.shortName = shortName;
    this.checker = new TestChecker();
    this.checker.setActualNumberOfValues(() -> dataOne.numberOfStringValues + dataTwo.numberOfStringValues +
            dataThree.numberOfStringValues + dataThreeOther.numberOfStringValues);
  }

  @Test
  public void checkJacksonReference() {
    testJaddContainReferenceToJackson(
            Paths.get("src", "test",
                    "02-after-ragconnect", shortName, "RagConnect.jadd"), false);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    dataOne = new ReceiverData();
    dataTwo = new ReceiverData();
    dataThree = new ReceiverData();
    dataThreeOther = new ReceiverData();

    handler.newConnection(TOPIC_SEND_ONE, bytes -> {
      dataOne.numberOfStringValues += 1;
      dataOne.lastStringValue = DefaultMappings.BytesToString(bytes);
    });
    handler.newConnection(TOPIC_SEND_TWO, bytes -> {
      dataTwo.numberOfStringValues += 1;
      dataTwo.lastStringValue = DefaultMappings.BytesToString(bytes);
    });

    handler.newConnection(TOPIC_SEND_THREE_VALUE, bytes -> {
      dataThree.numberOfStringValues += 1;
      dataThree.lastStringValue = DefaultMappings.BytesToString(bytes);
    });
    handler.newConnection(TOPIC_SEND_THREE_OTHER, bytes -> {
      dataThreeOther.numberOfStringValues += 1;
      dataThreeOther.lastStringValue = DefaultMappings.BytesToString(bytes);
    });

    connectModel();
  }

  protected abstract void connectModel() throws IOException;

  @Override
  protected void communicateSendInitialValue() throws InterruptedException, IOException {
    // check initial value
    checker.addToNumberOfValues(4);
    checkData(1, "Start-Post",
        1, "Start-Post",
        1, "Start-Post",
        1, "Start-T-Post");

    communicateBoth(1, "Start-Post");
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException, IOException {
    // check initial value
    checkData(0, null,
            0, null,
            0, null,
            0, null);

    communicateBoth(0, null);
  }

  private void communicateBoth(int initialNumberOfValues, String initialLastOneStringValue) throws IOException {
    // send new value
    sendData("210", "310");
    checker.addToNumberOfValues(3);
    checkData(initialNumberOfValues, initialLastOneStringValue,
        initialNumberOfValues + 1, "Pre-210-Post",
        initialNumberOfValues + 1, "Pre-310-Post",
        initialNumberOfValues + 1, "Pre-310-T-Post");

    // set new value
    setData("111", "211", "311");
    checker.addToNumberOfValues(4);
    checkData(initialNumberOfValues + 1, "111-Post",
        initialNumberOfValues + 2, "211-Post",
        initialNumberOfValues + 2, "311-Post",
        initialNumberOfValues + 2, "311-T-Post");

    // send the same values (will not be sent again)
    setData("111", "211", "311");
    checkData(initialNumberOfValues + 1, "111-Post",
        initialNumberOfValues + 2, "211-Post",
        initialNumberOfValues + 2, "311-Post",
        initialNumberOfValues + 2, "311-T-Post");

    // send values with prefixes imitating receiving
    checker.addToNumberOfValues(4);
    setData("112", "Pre-212", "Pre-312");
    checkData(initialNumberOfValues + 2, "112-Post",
        initialNumberOfValues + 3, "Pre-212-Post",
        initialNumberOfValues + 3, "Pre-312-Post",
        initialNumberOfValues + 3, "Pre-312-T-Post");

    // send the same values (will not be sent again, because previously prefixed)
    sendData("212", "312");
    checkData(initialNumberOfValues + 2, "112-Post",
        initialNumberOfValues + 3, "Pre-212-Post",
        initialNumberOfValues + 3, "Pre-312-Post",
        initialNumberOfValues + 3, "Pre-312-T-Post");

    // new values for two and three, but two will not send updated value
    assertTrue(disconnect("two", "SendValue", mqttUri(TOPIC_SEND_TWO)));
    sendData("213", "313");
    checker.addToNumberOfValues(2);
    checkData(initialNumberOfValues + 2, "112-Post",
        initialNumberOfValues + 3, "Pre-212-Post",
        initialNumberOfValues + 4, "Pre-313-Post",
        initialNumberOfValues + 4, "Pre-313-T-Post");
    assertEquals("Pre-213", valueOfReceiveAndSend());

    // can not disconnect again, and also not for different topic
    assertFalse(disconnect("two", "SendValue", mqttUri(TOPIC_SEND_TWO)));
    assertFalse(disconnect("two", "SendValue", mqttUri(TOPIC_RECEIVE_TWO)));

    // new values for two and three, but two will neither receive nor send updated value
    assertTrue(disconnect("two", "ReceiveValue", mqttUri(TOPIC_RECEIVE_TWO)));
    sendData("214", "314");
    checker.addToNumberOfValues(2);
    checkData(initialNumberOfValues + 2, "112-Post",
        initialNumberOfValues + 3, "Pre-212-Post",
        initialNumberOfValues + 5, "Pre-314-Post",
        initialNumberOfValues + 5, "Pre-314-T-Post");
    assertEquals("Pre-213", valueOfReceiveAndSend());

    // new values for three, but it will not receive updated value, and, thus, not send it either
    assertTrue(disconnect("three", "ReceiveValue", mqttUri(TOPIC_RECEIVE_THREE_VALUE)));
    sendData("214", "315");
    checkData(initialNumberOfValues + 2, "112-Post",
        initialNumberOfValues + 3, "Pre-212-Post",
        initialNumberOfValues + 5, "Pre-314-Post",
        initialNumberOfValues + 5, "Pre-314-T-Post");
    assertEquals("Pre-213", valueOfReceiveAndSend());

    // disconnect send is possible
    assertTrue(disconnect("three", "SendValue", mqttUri(TOPIC_SEND_THREE_VALUE)));
  }

  protected abstract boolean disconnect(String objectIdentifier, String targetIdentifier, String topic) throws IOException;

  protected abstract String valueOfReceiveAndSend();

  private void sendData(String inputTwo, String inputThree) {
    publisher.publish(TOPIC_RECEIVE_TWO, inputTwo.getBytes());
    publisher.publish(TOPIC_RECEIVE_THREE_VALUE, inputThree.getBytes());
  }

  protected abstract void setData(String inputOne, String inputTwo, String inputThree);

  private void checkData(int numberOfOneValues, String lastOneStringValue,
                         int numberOfTwoValues, String lastTwoStringValue,
                         int numberOfThreeValues, String lastThreeStringValue,
                         int numberOfOtherValues, String lastOtherStringValue
  ) {
    checker.check();
    dataOne.assertEqualData(numberOfOneValues, lastOneStringValue);
    dataTwo.assertEqualData(numberOfTwoValues, lastTwoStringValue);
    dataThree.assertEqualData(numberOfThreeValues, lastThreeStringValue);
    dataThreeOther.assertEqualData(numberOfOtherValues, lastOtherStringValue);
  }

  private static class ReceiverData {
    String lastStringValue;
    int numberOfStringValues = 0;

    public void assertEqualData(int expectedNumberOfValues, String expectedLastValue) {
      assertEquals(expectedNumberOfValues, this.numberOfStringValues);
      assertEquals(expectedLastValue, this.lastStringValue);
    }
  }

}
