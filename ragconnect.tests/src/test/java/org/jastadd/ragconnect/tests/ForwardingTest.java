package org.jastadd.ragconnect.tests;

import forwardingInc.ast.*;
import org.jastadd.ragconnect.tests.utils.TestChecker;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Tag;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.jastadd.ragconnect.tests.utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "forwarding".
 *
 * @author rschoene - Initial contribution
 */
@Tag("Incremental")
public class ForwardingTest extends AbstractMqttTest {

  private static final String TOPIC_WILDCARD = "forwarding/#";
  private static final String TOPIC_A_SINGLE = "forwarding/a-single";
  private static final String TOPIC_A_MANY = "forwarding/a-many";
  private static final String TOPIC_B_SINGLE = "forwarding/b-single";
  private static final String TOPIC_C_SINGLE = "forwarding/c-single";
  private static final String TOPIC_C_MANY = "forwarding/c-many";
  private static final String TOPIC_D_SINGLE = "forwarding/d-single";

  private static final Random rand = new Random();

  /** Use initially created members as values for {@link #communicateOnlyUpdatedValue()} method */
  private static final String INITIAL_VALUE = "initial" + rand.nextInt(100);

  private Root model;
  private SenderRoot senderRoot;
  private ReceiverRoot receiverRoot;
  private MqttHandler handler;

  private ReceiverData data;

  private TestChecker checker;

  @Override
  protected void createModel() {
    model = new Root();
//    model.trace().setReceiver(TestUtils::logEvent);
    senderRoot = new SenderRoot();
    model.setSenderRoot(senderRoot);

    senderRoot.setA(new A().setValue("as1"));
    senderRoot.setSingleA(new A().setValue("as1"));
    senderRoot.setMaybeA(new A().setValue("as1"));
    senderRoot.addMultipleA(new A().setValue("am1"));
    senderRoot.addMultipleA(new A().setValue("am2"));

    senderRoot.setB(new B().setValue("bs1"));
    senderRoot.setSingleB(new B().setValue("bs1"));
    senderRoot.setMaybeB(new B().setValue("bs1"));
    senderRoot.addMultipleB(new B().setValue("bs1"));
    senderRoot.addMultipleB(new B().setValue("bs2"));

    senderRoot.setC(new C().setValue("cs1"));
    senderRoot.setSingleC(new C().setValue("cs1"));
    senderRoot.setMaybeC(new C().setValue("cs1"));
    senderRoot.addMultipleC(new C().setValue("cm1"));
    senderRoot.addMultipleC(new C().setValue("cm2"));

    senderRoot.setD(new D().setValue("ds1"));
    senderRoot.setSingleD(new D().setValue("ds1"));
    senderRoot.setMaybeD(new D().setValue("ds1"));
    senderRoot.addMultipleD(new D().setValue("ds1"));
    senderRoot.addMultipleD(new D().setValue("ds2"));

    receiverRoot = new ReceiverRoot();
    model.setReceiverRoot(receiverRoot);

    receiverRoot.setA(new A().setValue(INITIAL_VALUE));
    receiverRoot.setB(new B().setValue(INITIAL_VALUE));
    receiverRoot.setC(new C().setValue(INITIAL_VALUE));
    receiverRoot.setD(new D().setValue(INITIAL_VALUE));

    data = new ReceiverData();
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException, InterruptedException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    handler.publish("test", Boolean.toString(isWriteCurrentValue()).getBytes(StandardCharsets.UTF_8));

    handler.newConnection(TOPIC_WILDCARD, (topic, bytes) -> {
      data.valuesSent += 1;
      data.valueSentSinceLastCheck.set(true);
    });

    checker = new TestChecker();
    checker.setActualNumberOfValues(() -> data.valuesSent)
            .setCheckForString(TOPIC_A_SINGLE,
                    (name, expected) -> assertThat(receiverRoot.getA().getValue()).as(name).isEqualTo(expected))
            .setCheckForTuple(TOPIC_A_MANY,
                    (name, expected) -> assertThat(receiverRoot.getManyAList()).extracting("Value").as(name)
                    .containsExactlyElementsOf(expected.toList()))
            .setCheckForString(TOPIC_B_SINGLE,
                    (name, expected) -> assertThat(receiverRoot.getB().getValue()).as(name).isEqualTo(expected))
            .setCheckForString(TOPIC_C_SINGLE,
                    (name, expected) -> assertThat(receiverRoot.getC().getValue()).as(name).isEqualTo(expected))
            .setCheckForTuple(TOPIC_C_MANY,
                    (name, expected) -> assertThat(receiverRoot.getManyCList()).extracting("Value").as(name)
                    .containsExactlyElementsOf(expected.toList()))
            .setCheckForString(TOPIC_D_SINGLE,
                    (name, expected) -> assertThat(receiverRoot.getD().getValue()).as(name).isEqualTo(expected))
    ;

    // connect receive
    assertTrue(receiverRoot.connectA(mqttUri(TOPIC_A_SINGLE)));
    assertTrue(receiverRoot.connectManyAList(mqttUri(TOPIC_A_MANY)));
    assertTrue(receiverRoot.connectB(mqttUri(TOPIC_B_SINGLE)));
    assertTrue(receiverRoot.connectC(mqttUri(TOPIC_C_SINGLE)));
    assertTrue(receiverRoot.connectManyCList(mqttUri(TOPIC_C_MANY)));
    assertTrue(receiverRoot.connectD(mqttUri(TOPIC_D_SINGLE)));

    // connect send
    assertTrueAndWaitForValue(senderRoot.connectA(mqttUri(TOPIC_A_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.connectSingleA(mqttUri(TOPIC_A_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.connectMaybeA(mqttUri(TOPIC_A_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.connectMultipleAList(mqttUri(TOPIC_A_MANY), isWriteCurrentValue()));

    assertTrueAndWaitForValue(senderRoot.getB().connect(mqttUri(TOPIC_B_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.getSingleB().connect(mqttUri(TOPIC_B_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.getMaybeB().connect(mqttUri(TOPIC_B_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.getMultipleB(0).connect(mqttUri(TOPIC_B_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.getMultipleB(1).connect(mqttUri(TOPIC_B_SINGLE), isWriteCurrentValue()));

    assertTrueAndWaitForValue(senderRoot.connectC(mqttUri(TOPIC_C_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.connectSingleC(mqttUri(TOPIC_C_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.connectMaybeC(mqttUri(TOPIC_C_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.connectMultipleCList(mqttUri(TOPIC_C_MANY), isWriteCurrentValue()));

    assertTrueAndWaitForValue(senderRoot.getD().connect(mqttUri(TOPIC_D_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.getSingleD().connect(mqttUri(TOPIC_D_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.getMaybeD().connect(mqttUri(TOPIC_D_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.getMultipleD(0).connect(mqttUri(TOPIC_D_SINGLE), isWriteCurrentValue()));
    assertTrueAndWaitForValue(senderRoot.getMultipleD(1).connect(mqttUri(TOPIC_D_SINGLE), isWriteCurrentValue()));
  }

  private void assertTrueAndWaitForValue(boolean actual) {
    assertTrue(actual);
    waitForValue();
  }

  private void waitForValue() {
    if (isWriteCurrentValue()) {
      awaitMqtt().until(() -> data.valueSentSinceLastCheck.getAndSet(false));
    }
  }

  @Override
  protected void communicateSendInitialValue() throws IOException, InterruptedException {
    // Sink.A     <-- Root.A, Root.SingleA, Root.MaybeA (and C)
    // Sink.ManyA <-- Root.MultipleA (and C)
    // Sink.B     <-- Root.B, Root.SingleB, Root.MaybeB, indexed Root.MultipleB (and D)
    // MultipleB += "-other", all C = "pre-" + value, almost all D += "-post", MultipleD += "-other"
    checker.addToNumberOfValues(18)
            .put(TOPIC_A_SINGLE, "as1")
            .put(TOPIC_A_MANY, tuple("am1", "am2"))
            .put(TOPIC_B_SINGLE, "bs2-other")
            .put(TOPIC_C_SINGLE, "pre-cs1")
            .put(TOPIC_C_MANY, tuple("pre-cm1", "pre-cm2"))
            .put(TOPIC_D_SINGLE, "ds2-other");

    communicateBoth();
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws IOException, InterruptedException {
    // Sink.A     <-- Root.A, Root.SingleA, Root.MaybeA (and C)
    // Sink.ManyA <-- Root.MultipleA (and C)
    // Sink.B     <-- Root.B, Root.SingleB, Root.MaybeB, indexed Root.MultipleB (and D)
    // MultipleB += "-other", all C = "pre-" + value, almost all D += "-post", MultipleD += "-other"

    checker.put(TOPIC_A_SINGLE, INITIAL_VALUE)
            .put(TOPIC_A_MANY, tuple())
            .put(TOPIC_B_SINGLE, INITIAL_VALUE)
            .put(TOPIC_C_SINGLE, INITIAL_VALUE)
            .put(TOPIC_C_MANY, tuple())
            .put(TOPIC_D_SINGLE, INITIAL_VALUE);

    communicateBoth();
  }

  private void communicateBoth() throws IOException {
    checker.check();

    // --- A ---
    senderRoot.getA().setValue("test-3");
    checker.incNumberOfValues().put(TOPIC_A_SINGLE, "test-3").check();

    senderRoot.getSingleA().setValue("test-4");
    checker.incNumberOfValues().put(TOPIC_A_SINGLE, "test-4").check();

    senderRoot.getMaybeA().setValue("test-5");
    checker.incNumberOfValues().put(TOPIC_A_SINGLE, "test-5").check();

    // whole list is updated after change of one element
    senderRoot.getMultipleA(0).setValue("test-6");
    checker.incNumberOfValues().put(TOPIC_A_MANY, tuple("test-6", "am2")).check();

    senderRoot.addMultipleA(new A().setValue("test-7"));
    checker.incNumberOfValues().put(TOPIC_A_MANY, tuple("test-6", "am2", "test-7")).check();

    // --- B ---
    senderRoot.getB().setValue("test-8");
    checker.incNumberOfValues().put(TOPIC_B_SINGLE, "test-8").check();

    senderRoot.getSingleB().setValue("test-9");
    checker.incNumberOfValues().put(TOPIC_B_SINGLE, "test-9").check();

    senderRoot.getMaybeB().setValue("test-10");
    checker.incNumberOfValues().put(TOPIC_B_SINGLE, "test-10").check();

    senderRoot.getMultipleB(0).setValue("test-11");
    checker.incNumberOfValues().put(TOPIC_B_SINGLE, "test-11-other").check();

    // not connected, so no value is sent (manually wait)
    senderRoot.addMultipleB(new B().setValue("test-12-ignored"));
    checker.check();

    // connected, but current value is not sent (manually wait)
    assertTrue(senderRoot.getMultipleB(2).connect(mqttUri(TOPIC_B_SINGLE), false));
    checker.check();

    senderRoot.getMultipleB(2).setValue("test-12");
    checker.incNumberOfValues().put(TOPIC_B_SINGLE, "test-12-other").check();

    // --- C ---
    senderRoot.getC().setValue("test-13");
    checker.incNumberOfValues().put(TOPIC_C_SINGLE, "pre-test-13").check();

    senderRoot.getSingleC().setValue("test-14");
    checker.incNumberOfValues().put(TOPIC_C_SINGLE, "pre-test-14").check();

    senderRoot.getMaybeC().setValue("test-15");
    checker.incNumberOfValues().put(TOPIC_C_SINGLE, "pre-test-15").check();

    senderRoot.getMultipleC(1).setValue("test-16");
    checker.incNumberOfValues().put(TOPIC_C_MANY, tuple("pre-cm1", "pre-test-16")).check();

    senderRoot.addMultipleC(new C().setValue("test-17"));
    checker.incNumberOfValues().put(TOPIC_C_MANY, tuple("pre-cm1", "pre-test-16", "pre-test-17")).check();

    // --- D ---
    senderRoot.getD().setValue("test-18");
    checker.incNumberOfValues().put(TOPIC_D_SINGLE, "test-18-post").check();

    senderRoot.getSingleD().setValue("test-19");
    checker.incNumberOfValues().put(TOPIC_D_SINGLE, "test-19-post").check();

    senderRoot.getMaybeD().setValue("test-20");
    checker.incNumberOfValues().put(TOPIC_D_SINGLE, "test-20-post").check();

    senderRoot.getMultipleD(0).setValue("test-21");
    checker.incNumberOfValues().put(TOPIC_D_SINGLE, "test-21-other").check();

    // not connected, so no value is sent (manually wait)
    senderRoot.addMultipleD(new D().setValue("test-22-ignored"));
    checker.check();

    // connected, but current value is not sent (manually wait)
    assertTrue(senderRoot.getMultipleD(2).connect(mqttUri(TOPIC_D_SINGLE), false));
    checker.check();

    senderRoot.getMultipleD(2).setValue("test-22");
    checker.incNumberOfValues().put(TOPIC_D_SINGLE, "test-22-other").check();


    assertTrue(senderRoot.getMultipleB(1).disconnectSend(mqttUri(TOPIC_B_SINGLE)));
    senderRoot.getMultipleB(1).setValue("test-23-ignored");

    // disconnect affects complete list
    senderRoot.getMultipleB(0).setValue("test-24-ignored");
    senderRoot.getMultipleB(2).setValue("test-25-ignored");

    assertTrue(senderRoot.getMaybeB().disconnectSend(mqttUri(TOPIC_B_SINGLE)));
    senderRoot.getMaybeB().setValue("test-26-ignored");

    assertTrue(senderRoot.getB().disconnectSend(mqttUri(TOPIC_B_SINGLE)));
    senderRoot.getB().setValue("test-27-ignored");

    checker.check();

    // same for A
    assertTrue(senderRoot.disconnectMultipleAList(mqttUri(TOPIC_A_MANY)));
    senderRoot.getMultipleA(0).setValue("test-28-ignored");
    senderRoot.getMultipleA(2).setValue("test-29-ignored");

    assertTrue(senderRoot.disconnectMaybeA(mqttUri(TOPIC_A_SINGLE)));
    senderRoot.getMaybeA().setValue("test-30-ignored");

    assertTrue(senderRoot.disconnectA(mqttUri(TOPIC_A_SINGLE)));
    senderRoot.getA().setValue("test-31-ignored");

    checker.check();
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  static class ReceiverData {
    int valuesSent = 0;
    AtomicBoolean valueSentSinceLastCheck = new AtomicBoolean(false);
  }
}
