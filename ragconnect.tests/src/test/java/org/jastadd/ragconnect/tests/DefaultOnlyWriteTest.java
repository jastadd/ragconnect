package org.jastadd.ragconnect.tests;

import defaultOnlyWrite.ast.A;
import defaultOnlyWrite.ast.BoxedTypesSyn;
import defaultOnlyWrite.ast.MqttHandler;
import defaultOnlyWrite.ast.NativeTypesSyn;
import org.jastadd.ragconnect.tests.utils.DefaultMappings;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test case "defaultOnlyRead".
 *
 * @author rschoene - Initial contribution
 */
public class DefaultOnlyWriteTest extends AbstractMqttTest {

  private static final String TOPIC_NATIVE_BOOLEAN = "native/boolean";
  private static final String TOPIC_NATIVE_INT = "native/int";
  private static final String TOPIC_NATIVE_SHORT = "native/short";
  private static final String TOPIC_NATIVE_LONG = "native/long";
  private static final String TOPIC_NATIVE_FLOAT = "native/float";
  private static final String TOPIC_NATIVE_DOUBLE = "native/double";
  private static final String TOPIC_NATIVE_CHAR = "native/char";
  private static final String TOPIC_NATIVE_STRING = "native/string";

  private static final String TOPIC_NATIVE_BOOLEAN_TRANSFORMED = "native/boolean/t";
  private static final String TOPIC_NATIVE_INT_TRANSFORMED = "native/int/t";
  private static final String TOPIC_NATIVE_SHORT_TRANSFORMED = "native/short/t";
  private static final String TOPIC_NATIVE_LONG_TRANSFORMED = "native/long/t";
  private static final String TOPIC_NATIVE_FLOAT_TRANSFORMED = "native/float/t";
  private static final String TOPIC_NATIVE_DOUBLE_TRANSFORMED = "native/double/t";
  private static final String TOPIC_NATIVE_CHAR_TRANSFORMED = "native/char/t";
  private static final String TOPIC_NATIVE_STRING_TRANSFORMED = "native/string/t";

  private static final String TOPIC_BOXED_BOOLEAN = "boxed/Boolean";
  private static final String TOPIC_BOXED_INTEGER = "boxed/Integer";
  private static final String TOPIC_BOXED_SHORT = "boxed/Short";
  private static final String TOPIC_BOXED_LONG = "boxed/Long";
  private static final String TOPIC_BOXED_FLOAT = "boxed/Float";
  private static final String TOPIC_BOXED_DOUBLE = "boxed/Double";
  private static final String TOPIC_BOXED_CHARACTER = "boxed/Character";

  private static final String TOPIC_BOXED_BOOLEAN_TRANSFORMED = "boxed/Boolean/t";
  private static final String TOPIC_BOXED_INTEGER_TRANSFORMED = "boxed/Integer/t";
  private static final String TOPIC_BOXED_SHORT_TRANSFORMED = "boxed/Short/t";
  private static final String TOPIC_BOXED_LONG_TRANSFORMED = "boxed/Long/t";
  private static final String TOPIC_BOXED_FLOAT_TRANSFORMED = "boxed/Float/t";
  private static final String TOPIC_BOXED_DOUBLE_TRANSFORMED = "boxed/Double/t";
  private static final String TOPIC_BOXED_CHARACTER_TRANSFORMED = "boxed/Character/t";

  private A model;
  private NativeTypesSyn nativeIntegers;
  private NativeTypesSyn nativeFloats;
  private NativeTypesSyn nativeChars;
  private BoxedTypesSyn boxedIntegers;
  private BoxedTypesSyn boxedFloats;
  private BoxedTypesSyn boxedChars;
  private MqttHandler receiver;
  private ReceiverData dataNormal;
  private ReceiverData dataTransformed;

  @Test
  public void checkNotJacksonReference() {
    testJaddContainReferenceToJackson(
        Paths.get("src", "test",
            "02-after-ragconnect", "defaultOnlyWrite", "RagConnect.jadd"), false);
  }

  @Override
  protected void createModel() {
    model = new A();

    nativeIntegers = new NativeTypesSyn();
    nativeFloats = new NativeTypesSyn();
    nativeChars = new NativeTypesSyn();
    model.addNativeTypesSyn(nativeIntegers);
    model.addNativeTypesSyn(nativeFloats);
    model.addNativeTypesSyn(nativeChars);

    boxedIntegers = new BoxedTypesSyn();
    boxedFloats = new BoxedTypesSyn();
    boxedChars = new BoxedTypesSyn();
    model.addBoxedTypesSyn(boxedIntegers);
    model.addBoxedTypesSyn(boxedFloats);
    model.addBoxedTypesSyn(boxedChars);

    setData("1", "1.1", "ab");
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    receiver = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(receiver.waitUntilReady(2, TimeUnit.SECONDS));

    nativeIntegers.addNativeBooleanDependency(nativeIntegers);
    nativeIntegers.addNativeIntDependency(nativeIntegers);
    nativeIntegers.addNativeShortDependency(nativeIntegers);
    nativeIntegers.addNativeLongDependency(nativeIntegers);
    nativeFloats.addNativeFloatDependency(nativeFloats);
    nativeFloats.addNativeDoubleDependency(nativeFloats);
    nativeChars.addNativeCharDependency(nativeChars);
    nativeChars.addNativeStringDependency(nativeChars);

    nativeIntegers.addNativeBooleanTransformedDependency(nativeIntegers);
    nativeIntegers.addNativeIntTransformedDependency(nativeIntegers);
    nativeIntegers.addNativeShortTransformedDependency(nativeIntegers);
    nativeIntegers.addNativeLongTransformedDependency(nativeIntegers);
    nativeFloats.addNativeFloatTransformedDependency(nativeFloats);
    nativeFloats.addNativeDoubleTransformedDependency(nativeFloats);
    nativeChars.addNativeCharTransformedDependency(nativeChars);
    nativeChars.addNativeStringTransformedDependency(nativeChars);

    boxedIntegers.addBoxedBooleanDependency(boxedIntegers);
    boxedIntegers.addBoxedIntDependency(boxedIntegers);
    boxedIntegers.addBoxedShortDependency(boxedIntegers);
    boxedIntegers.addBoxedLongDependency(boxedIntegers);
    boxedFloats.addBoxedFloatDependency(boxedFloats);
    boxedFloats.addBoxedDoubleDependency(boxedFloats);
    boxedChars.addBoxedCharDependency(boxedChars);

    boxedIntegers.addBoxedBooleanTransformedDependency(boxedIntegers);
    boxedIntegers.addBoxedIntTransformedDependency(boxedIntegers);
    boxedIntegers.addBoxedShortTransformedDependency(boxedIntegers);
    boxedIntegers.addBoxedLongTransformedDependency(boxedIntegers);
    boxedFloats.addBoxedFloatTransformedDependency(boxedFloats);
    boxedFloats.addBoxedDoubleTransformedDependency(boxedFloats);
    boxedChars.addBoxedCharTransformedDependency(boxedChars);

    dataNormal = createReceiver(false);
    dataTransformed = createReceiver(true);

    assertTrue(nativeIntegers.connectBooleanValue(mqttUri(TOPIC_NATIVE_BOOLEAN), isWriteCurrentValue()));
    assertTrue(nativeIntegers.connectIntValue(mqttUri(TOPIC_NATIVE_INT), isWriteCurrentValue()));
    assertTrue(nativeIntegers.connectShortValue(mqttUri(TOPIC_NATIVE_SHORT), isWriteCurrentValue()));
    assertTrue(nativeIntegers.connectLongValue(mqttUri(TOPIC_NATIVE_LONG), isWriteCurrentValue()));
    assertTrue(nativeFloats.connectFloatValue(mqttUri(TOPIC_NATIVE_FLOAT), isWriteCurrentValue()));
    assertTrue(nativeFloats.connectDoubleValue(mqttUri(TOPIC_NATIVE_DOUBLE), isWriteCurrentValue()));
    assertTrue(nativeChars.connectCharValue(mqttUri(TOPIC_NATIVE_CHAR), isWriteCurrentValue()));
    assertTrue(nativeChars.connectStringValue(mqttUri(TOPIC_NATIVE_STRING), isWriteCurrentValue()));

    assertTrue(nativeIntegers.connectBooleanValueTransformed(mqttUri(TOPIC_NATIVE_BOOLEAN_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(nativeIntegers.connectIntValueTransformed(mqttUri(TOPIC_NATIVE_INT_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(nativeIntegers.connectShortValueTransformed(mqttUri(TOPIC_NATIVE_SHORT_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(nativeIntegers.connectLongValueTransformed(mqttUri(TOPIC_NATIVE_LONG_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(nativeFloats.connectFloatValueTransformed(mqttUri(TOPIC_NATIVE_FLOAT_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(nativeFloats.connectDoubleValueTransformed(mqttUri(TOPIC_NATIVE_DOUBLE_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(nativeChars.connectCharValueTransformed(mqttUri(TOPIC_NATIVE_CHAR_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(nativeChars.connectStringValueTransformed(mqttUri(TOPIC_NATIVE_STRING_TRANSFORMED), isWriteCurrentValue()));

    assertTrue(boxedIntegers.connectBooleanValue(mqttUri(TOPIC_BOXED_BOOLEAN), isWriteCurrentValue()));
    assertTrue(boxedIntegers.connectIntValue(mqttUri(TOPIC_BOXED_INTEGER), isWriteCurrentValue()));
    assertTrue(boxedIntegers.connectShortValue(mqttUri(TOPIC_BOXED_SHORT), isWriteCurrentValue()));
    assertTrue(boxedIntegers.connectLongValue(mqttUri(TOPIC_BOXED_LONG), isWriteCurrentValue()));
    assertTrue(boxedFloats.connectFloatValue(mqttUri(TOPIC_BOXED_FLOAT), isWriteCurrentValue()));
    assertTrue(boxedFloats.connectDoubleValue(mqttUri(TOPIC_BOXED_DOUBLE), isWriteCurrentValue()));
    assertTrue(boxedChars.connectCharValue(mqttUri(TOPIC_BOXED_CHARACTER), isWriteCurrentValue()));

    assertTrue(boxedIntegers.connectBooleanValueTransformed(mqttUri(TOPIC_BOXED_BOOLEAN_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(boxedIntegers.connectIntValueTransformed(mqttUri(TOPIC_BOXED_INTEGER_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(boxedIntegers.connectShortValueTransformed(mqttUri(TOPIC_BOXED_SHORT_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(boxedIntegers.connectLongValueTransformed(mqttUri(TOPIC_BOXED_LONG_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(boxedFloats.connectFloatValueTransformed(mqttUri(TOPIC_BOXED_FLOAT_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(boxedFloats.connectDoubleValueTransformed(mqttUri(TOPIC_BOXED_DOUBLE_TRANSFORMED), isWriteCurrentValue()));
    assertTrue(boxedChars.connectCharValueTransformed(mqttUri(TOPIC_BOXED_CHARACTER_TRANSFORMED), isWriteCurrentValue()));
  }

  private ReceiverData createReceiver(boolean transformed) {
    ReceiverData result = new ReceiverData();

    receiver.newConnection(transformed ? TOPIC_NATIVE_BOOLEAN_TRANSFORMED : TOPIC_NATIVE_BOOLEAN, bytes -> {
      result.numberOfNativeBoolValues += 1;
      result.lastNativeBoolValue = DefaultMappings.BytesToBool(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_NATIVE_INT_TRANSFORMED : TOPIC_NATIVE_INT, bytes -> {
      result.numberOfNativeIntValues += 1;
      result.lastNativeIntValue = DefaultMappings.BytesToInt(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_NATIVE_SHORT_TRANSFORMED : TOPIC_NATIVE_SHORT, bytes -> {
      result.numberOfNativeShortValues += 1;
      result.lastNativeShortValue = DefaultMappings.BytesToShort(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_NATIVE_LONG_TRANSFORMED : TOPIC_NATIVE_LONG, bytes -> {
      result.numberOfNativeLongValues += 1;
      result.lastNativeLongValue = DefaultMappings.BytesToLong(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_NATIVE_FLOAT_TRANSFORMED : TOPIC_NATIVE_FLOAT, bytes -> {
      result.numberOfNativeFloatValues += 1;
      result.lastNativeFloatValue = DefaultMappings.BytesToFloat(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_NATIVE_DOUBLE_TRANSFORMED : TOPIC_NATIVE_DOUBLE, bytes -> {
      result.numberOfNativeDoubleValues += 1;
      result.lastNativeDoubleValue = DefaultMappings.BytesToDouble(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_NATIVE_CHAR_TRANSFORMED : TOPIC_NATIVE_CHAR, bytes -> {
      result.numberOfNativeCharValues += 1;
      result.lastNativeCharValue = DefaultMappings.BytesToChar(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_NATIVE_STRING_TRANSFORMED : TOPIC_NATIVE_STRING, bytes -> {
      result.numberOfNativeStringValues += 1;
      result.lastNativeStringValue = DefaultMappings.BytesToString(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_BOXED_BOOLEAN_TRANSFORMED : TOPIC_BOXED_BOOLEAN, bytes -> {
      result.numberOfBoxedBoolValues += 1;
      result.lastBoxedBoolValue = DefaultMappings.BytesToBool(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_BOXED_INTEGER_TRANSFORMED : TOPIC_BOXED_INTEGER, bytes -> {
      result.numberOfBoxedIntValues += 1;
      result.lastBoxedIntValue = DefaultMappings.BytesToInt(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_BOXED_SHORT_TRANSFORMED : TOPIC_BOXED_SHORT, bytes -> {
      result.numberOfBoxedShortValues += 1;
      result.lastBoxedShortValue = DefaultMappings.BytesToShort(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_BOXED_LONG_TRANSFORMED : TOPIC_BOXED_LONG, bytes -> {
      result.numberOfBoxedLongValues += 1;
      result.lastBoxedLongValue = DefaultMappings.BytesToLong(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_BOXED_FLOAT_TRANSFORMED : TOPIC_BOXED_FLOAT, bytes -> {
      result.numberOfBoxedFloatValues += 1;
      result.lastBoxedFloatValue = DefaultMappings.BytesToFloat(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_BOXED_DOUBLE_TRANSFORMED : TOPIC_BOXED_DOUBLE, bytes -> {
      result.numberOfBoxedDoubleValues += 1;
      result.lastBoxedDoubleValue = DefaultMappings.BytesToDouble(bytes);
    });
    receiver.newConnection(transformed ? TOPIC_BOXED_CHARACTER_TRANSFORMED : TOPIC_BOXED_CHARACTER, bytes -> {
      result.numberOfBoxedCharValues += 1;
      result.lastBoxedCharValue = DefaultMappings.BytesToChar(bytes);
    });
    return result;
  }

  @Override
  protected void communicateSendInitialValue() throws InterruptedException {
    // check initial value
    checkData(1, 1, false, 1, 1.1, 'a', "ab");

    // set new value
    setData("2", "2.2", "cd");

    // check new value
    checkData(2, 2, true, 2, 2.2, 'c', "cd");

    // set new value
    setData("3", "3.2", "ee");

    // check new value
    checkData(3, 2, true, 3, 3.2, 'e', "ee");
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException {
    // check initial value (will be default values)
    checkData(0, 0, null, null, null, null, null);

    // set new value
    setData("2", "2.2", "cd");

    // check new value
    checkData(1, 1, true, 2, 2.2, 'c', "cd");

    // set new value
    setData("3", "3.2", "ee");

    // check new value
    checkData(2, 1, true, 3, 3.2, 'e', "ee");
  }

  @Override
  public void closeConnections() {
    if (receiver != null) {
      receiver.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  private void setData(String integerDriver, String floatDriver, String stringDriver) {
    nativeIntegers.setDriverSyn(integerDriver);
    nativeFloats.setDriverSyn(floatDriver);
    nativeChars.setDriverSyn(stringDriver);

    boxedIntegers.setDriverSyn(integerDriver);
    boxedFloats.setDriverSyn(floatDriver);
    boxedChars.setDriverSyn(stringDriver);
  }

  private void checkData(int expectedNumberOfValues, int expectedNumberOfBoolValues,
                         Boolean expectedBool, Integer expectedInt, Double expectedDouble,
                         Character expectedChar, String expectedString) throws InterruptedException {
    TestUtils.waitForMqtt();
    for (ReceiverData data : new ReceiverData[]{dataNormal, dataTransformed}) {
      assertEquals(expectedNumberOfBoolValues, data.numberOfNativeBoolValues);
      assertEquals(expectedNumberOfValues, data.numberOfNativeIntValues);
      assertEquals(expectedNumberOfValues, data.numberOfNativeShortValues);
      assertEquals(expectedNumberOfValues, data.numberOfNativeLongValues);
      assertEquals(expectedNumberOfValues, data.numberOfNativeFloatValues);
      assertEquals(expectedNumberOfValues, data.numberOfNativeDoubleValues);
      assertEquals(expectedNumberOfValues, data.numberOfNativeCharValues);
      assertEquals(expectedNumberOfValues, data.numberOfNativeStringValues);

      assertEquals(expectedNumberOfBoolValues, data.numberOfBoxedBoolValues);
      assertEquals(expectedNumberOfValues, data.numberOfBoxedIntValues);
      assertEquals(expectedNumberOfValues, data.numberOfBoxedShortValues);
      assertEquals(expectedNumberOfValues, data.numberOfBoxedLongValues);
      assertEquals(expectedNumberOfValues, data.numberOfBoxedFloatValues);
      assertEquals(expectedNumberOfValues, data.numberOfBoxedDoubleValues);
      assertEquals(expectedNumberOfValues, data.numberOfBoxedCharValues);

      if (expectedBool != null) {
        assertEquals(expectedBool, data.lastNativeBoolValue);
        assertEquals(expectedBool, data.lastBoxedBoolValue);
      } else {
        assertFalse(data.lastNativeBoolValue);
        assertNull(data.lastBoxedBoolValue);
      }

      if (expectedInt != null) {
        assertEquals(expectedInt.intValue(), data.lastNativeIntValue);
        assertEquals(expectedInt.shortValue(), data.lastNativeShortValue);
        assertEquals(expectedInt.longValue(), data.lastNativeLongValue);
        assertEquals(expectedInt.intValue(), data.lastBoxedIntValue.intValue());
        assertEquals(expectedInt.shortValue(), data.lastBoxedShortValue.shortValue());
        assertEquals(expectedInt.longValue(), data.lastBoxedLongValue.longValue());
      } else {
        assertEquals(0, data.lastNativeIntValue);
        assertEquals(0, data.lastNativeShortValue);
        assertEquals(0, data.lastNativeLongValue);
        assertNull(data.lastBoxedIntValue);
        assertNull(data.lastBoxedShortValue);
        assertNull(data.lastBoxedLongValue);
      }

      if (expectedDouble != null) {
        assertEquals(expectedDouble.floatValue(), data.lastNativeFloatValue, TestUtils.DELTA);
        assertEquals(expectedDouble, data.lastNativeDoubleValue, TestUtils.DELTA);
        assertEquals(expectedDouble.floatValue(), data.lastBoxedFloatValue, TestUtils.DELTA);
        assertEquals(expectedDouble, data.lastBoxedDoubleValue, TestUtils.DELTA);
      } else {
        assertEquals(0f, data.lastNativeFloatValue, TestUtils.DELTA);
        assertEquals(0d, data.lastNativeDoubleValue, TestUtils.DELTA);
        assertNull(data.lastBoxedFloatValue);
        assertNull(data.lastBoxedDoubleValue);
      }

      if (expectedChar != null) {
        assertEquals(expectedChar.charValue(), data.lastNativeCharValue);
        assertEquals(expectedChar, data.lastBoxedCharValue);
      } else {
        assertEquals('\0', data.lastNativeCharValue);
        assertNull(data.lastBoxedCharValue);
      }
      assertEquals(expectedString, data.lastNativeStringValue);
    }
  }

  private static class ReceiverData {
    boolean lastNativeBoolValue;
    int numberOfNativeBoolValues = 0;
    int lastNativeIntValue;
    int numberOfNativeIntValues = 0;
    short lastNativeShortValue;
    int numberOfNativeShortValues = 0;
    long lastNativeLongValue;
    int numberOfNativeLongValues = 0;
    float lastNativeFloatValue;
    int numberOfNativeFloatValues = 0;
    double lastNativeDoubleValue;
    int numberOfNativeDoubleValues = 0;
    char lastNativeCharValue;
    int numberOfNativeCharValues = 0;
    String lastNativeStringValue;
    int numberOfNativeStringValues = 0;

    Boolean lastBoxedBoolValue;
    int numberOfBoxedBoolValues = 0;
    Integer lastBoxedIntValue;
    int numberOfBoxedIntValues = 0;
    Short lastBoxedShortValue;
    int numberOfBoxedShortValues = 0;
    Long lastBoxedLongValue;
    int numberOfBoxedLongValues = 0;
    Float lastBoxedFloatValue;
    int numberOfBoxedFloatValues = 0;
    Double lastBoxedDoubleValue;
    int numberOfBoxedDoubleValues = 0;
    Character lastBoxedCharValue;
    int numberOfBoxedCharValues = 0;
  }

}
