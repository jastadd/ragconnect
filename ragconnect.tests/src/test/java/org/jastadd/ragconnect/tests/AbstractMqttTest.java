package org.jastadd.ragconnect.tests;

import defaultOnlyRead.ast.MqttHandler;
import io.github.artsok.RepeatedIfExceptionsTest;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Base class for tests ensuring running mqtt broker.
 *
 * @author rschoene - Initial contribution
 */
@Tag("mqtt")
public abstract class AbstractMqttTest extends RagConnectTest {

  protected static final int TEST_REPETITIONS = 3;
  private static boolean checkDone = false;

  protected static MqttHandler publisher;

  /**
   * if the initial/current value shall be sent upon connecting
   */
  protected boolean writeCurrentValue;

  public boolean isWriteCurrentValue() {
    return writeCurrentValue;
  }

  @BeforeAll
  public static void createPublisherAndCheckMqttConnectionOnce() {
    boolean checkResult;
    try {
      publisher = new MqttHandler("Publisher")
          .dontSendWelcomeMessage()
          .setHost(TestUtils.getMqttHost());
      checkResult = true;
    } catch (IOException e) {
      checkResult = false;
    }
    if (!checkDone) {
      checkDone = true;
      checkResult &= publisher.waitUntilReady(2, TimeUnit.SECONDS);
    }
    if (!checkResult) {
      throw new IllegalStateException("Mqtt Broker not ready!");
    }
  }

  @Test
  public final void buildModel() {
    createModel();
  }

  @Tag("mqtt")
  @RepeatedIfExceptionsTest(repeats = TEST_REPETITIONS)
  public void testCommunicateSendInitialValue() throws IOException, InterruptedException {
    this.writeCurrentValue = true;

    createModel();
    setupReceiverAndConnect();

    logger.info("Calling communicateSendInitialValue");
    communicateSendInitialValue();
  }

  /**
   * Actual test code for communication when sending initial value.
   * @throws InterruptedException because of TestUtils.waitForMqtt()
   */
  protected abstract void communicateSendInitialValue() throws IOException, InterruptedException;

  @Tag("mqtt")
  @RepeatedIfExceptionsTest(repeats = TEST_REPETITIONS)
  public void testCommunicateOnlyUpdatedValue() throws IOException, InterruptedException {
    this.writeCurrentValue = false;

    createModel();
    setupReceiverAndConnect();

    logger.info("Calling communicateOnlyUpdatedValue");
    communicateOnlyUpdatedValue();
  }

  /**
   * Actual test code for communication without sending any value upon connecting.
   * @throws InterruptedException because of TestUtils.waitForMqtt()
   */
  protected abstract void communicateOnlyUpdatedValue() throws IOException, InterruptedException;

  /**
   * Create the model, and set required default values.
   */
  protected abstract void createModel();

  /**
   * Begin with this snippet
   * <pre>
   * {@code
   * model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);
   *
   * handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
   * assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));
   * }
   * </pre>
   *
   * And then add dependencies, initialise receiver, add connections to those receivers,
   * and finally call generated connect* methods on model elements.
   */
  protected abstract void setupReceiverAndConnect() throws IOException, InterruptedException;

  @AfterEach
  public void alwaysCloseConnections() {
    logger.debug("Closing connections");
    closeConnections();
  }

  /**
   * Write the following snippet (using your correct handler and model):
   * <pre>
   * {@code
   * if (handler != null) {
   *    handler.close();
   * }
   * if (model != null) {
   *   model.ragconnectCloseConnections();
   * }
   * }
   * </pre>
   */
  protected abstract void closeConnections();

  @AfterAll
  public static void closePublisher() {
    if (publisher != null) {
      publisher.close();
    }
  }

}
