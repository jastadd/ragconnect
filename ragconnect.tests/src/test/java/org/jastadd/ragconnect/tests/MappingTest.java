package org.jastadd.ragconnect.tests;

import mapping.ast.A;
import mapping.ast.BoxedTypes;
import mapping.ast.MqttHandler;
import mapping.ast.NativeTypes;
import org.jastadd.ragconnect.tests.utils.DefaultMappings;
import org.jastadd.ragconnect.tests.utils.TestUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "mapping".
 *
 * @author rschoene - Initial contribution
 */
public class MappingTest extends AbstractMqttTest {

  private static final String TOPIC_INPUT = "input";
  private static final String TOPIC_WRITE_NATIVE_INT = "native/int";
  private static final String TOPIC_WRITE_NATIVE_SHORT = "native/short";
  private static final String TOPIC_WRITE_NATIVE_LONG = "native/long";
  private static final String TOPIC_WRITE_NATIVE_FLOAT = "native/float";
  private static final String TOPIC_WRITE_NATIVE_DOUBLE = "native/double";
  private static final String TOPIC_WRITE_NATIVE_CHAR = "native/char";
  private static final String TOPIC_WRITE_NATIVE_BOOLEAN = "native/boolean";

  private A model;
  private NativeTypes natives;
  private BoxedTypes boxes;
  private MqttHandler handler;
  private ReceiverData data;

  @Override
  protected void createModel() {
    model = new A();
    natives = new NativeTypes();
    natives.setDriver("1");
    boxes = new BoxedTypes();
    model.setNativeTypes(natives);
    model.setBoxedTypes(boxes);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    natives.addNativeIntDependency(natives);
    natives.addNativeShortDependency(natives);
    natives.addNativeLongDependency(natives);
    natives.addNativeFloatDependency(natives);
    natives.addNativeDoubleDependency(natives);
    natives.addNativeCharDependency(natives);
    natives.addNativeBooleanDependency(natives);

    data = new ReceiverData();
    handler.newConnection(TOPIC_WRITE_NATIVE_INT, bytes -> {
      data.numberOfNativeIntValues += 1;
      data.lastNativeIntValue = DefaultMappings.BytesToInt(bytes);
    });
    handler.newConnection(TOPIC_WRITE_NATIVE_SHORT, bytes -> {
      data.numberOfNativeShortValues += 1;
      data.lastNativeShortValue = DefaultMappings.BytesToShort(bytes);
    });
    handler.newConnection(TOPIC_WRITE_NATIVE_LONG, bytes -> {
      data.numberOfNativeLongValues += 1;
      data.lastNativeLongValue = DefaultMappings.BytesToLong(bytes);
    });
    handler.newConnection(TOPIC_WRITE_NATIVE_FLOAT, bytes -> {
      data.numberOfNativeFloatValues += 1;
      data.lastNativeFloatValue = DefaultMappings.BytesToFloat(bytes);
    });
    handler.newConnection(TOPIC_WRITE_NATIVE_DOUBLE, bytes -> {
      data.numberOfNativeDoubleValues += 1;
      data.lastNativeDoubleValue = DefaultMappings.BytesToDouble(bytes);
    });
    handler.newConnection(TOPIC_WRITE_NATIVE_CHAR, bytes -> {
      data.numberOfNativeCharValues += 1;
      data.lastNativeCharValue = DefaultMappings.BytesToChar(bytes);
    });
    handler.newConnection(TOPIC_WRITE_NATIVE_BOOLEAN, bytes -> {
      data.numberOfNativeBooleanValues += 1;
      data.lastNativeBooleanValue = DefaultMappings.BytesToBool(bytes);
    });

    assertTrue(natives.connectWriteIntValue(mqttUri(TOPIC_WRITE_NATIVE_INT), isWriteCurrentValue()));
    assertTrue(natives.connectWriteShortValue(mqttUri(TOPIC_WRITE_NATIVE_SHORT), isWriteCurrentValue()));
    assertTrue(natives.connectWriteLongValue(mqttUri(TOPIC_WRITE_NATIVE_LONG), isWriteCurrentValue()));
    assertTrue(natives.connectWriteFloatValue(mqttUri(TOPIC_WRITE_NATIVE_FLOAT), isWriteCurrentValue()));
    assertTrue(natives.connectWriteDoubleValue(mqttUri(TOPIC_WRITE_NATIVE_DOUBLE), isWriteCurrentValue()));
    assertTrue(natives.connectWriteCharValue(mqttUri(TOPIC_WRITE_NATIVE_CHAR), isWriteCurrentValue()));
    assertTrue(natives.connectWriteBooleanValue(mqttUri(TOPIC_WRITE_NATIVE_BOOLEAN), isWriteCurrentValue()));

    assertTrue(natives.connectIntValue(mqttUri(TOPIC_INPUT)));
    assertTrue(natives.connectShortValue(mqttUri(TOPIC_INPUT)));
    assertTrue(natives.connectLongValue(mqttUri(TOPIC_INPUT)));
    assertTrue(natives.connectFloatValue(mqttUri(TOPIC_INPUT)));
    assertTrue(natives.connectDoubleValue(mqttUri(TOPIC_INPUT)));
    assertTrue(natives.connectCharValue(mqttUri(TOPIC_INPUT)));
    assertTrue(natives.connectBooleanValue(mqttUri(TOPIC_INPUT)));

    assertTrue(boxes.connectIntValue(mqttUri(TOPIC_INPUT)));
    assertTrue(boxes.connectShortValue(mqttUri(TOPIC_INPUT)));
    assertTrue(boxes.connectLongValue(mqttUri(TOPIC_INPUT)));
    assertTrue(boxes.connectFloatValue(mqttUri(TOPIC_INPUT)));
    assertTrue(boxes.connectDoubleValue(mqttUri(TOPIC_INPUT)));
    assertTrue(boxes.connectCharValue(mqttUri(TOPIC_INPUT)));
    assertTrue(boxes.connectBooleanValue(mqttUri(TOPIC_INPUT)));
  }

  @Override
  protected void communicateSendInitialValue() throws InterruptedException {
    checkSendData(1, 1, (short) 1, 1, 1.01f, 1.01d, (char) 1, 1, false);
    // no check for initial received data (no input set yet)

    communicateBoth(1);
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException {
    checkSendData(0, 0, (short) 0, 0, 0f, 0d, (char) 0, 0, false);
    // no check for initial received data (no input set yet)

    communicateBoth(0);
  }

  private void communicateBoth(int initialNumberOfValues) throws InterruptedException {
    // send first value
    sendAndSetData("41", "51");
    checkSendData(initialNumberOfValues + 1, 41, (short) 41, 41, 41.01f, 41.01d, (char) 41, initialNumberOfValues + 1, true);
    checkReceiveData(51, (short) 51, 51, 51.01f, 51.01d, (char) 51);

    // send same value
    sendAndSetData("41", "51");
    checkSendData(initialNumberOfValues + 1, 41, (short) 41, 41, 41.01f, 41.01d, (char) 41, initialNumberOfValues + 1, true);
    checkReceiveData(51, (short) 51, 51, 51.01f, 51.01d, (char) 51);

    // send new value
    sendAndSetData("42", "52");
    checkSendData(initialNumberOfValues + 2, 42, (short) 42, 42, 42.01f, 42.01d, (char) 42, initialNumberOfValues + 1, true);
    checkReceiveData(52, (short) 52, 52, 52.01f, 52.01d, (char) 52);
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  private void sendAndSetData(String driver, String input) {
    natives.setDriver(driver);
    publisher.publish(TOPIC_INPUT, input.getBytes());
  }

  private void checkSendData(int expectedNumberOfValues, int expectedInt, short expectedShort, long expectedLong, float expectedFloat, double expectedDouble, char expectedChar, int expectedNumberOfBooleanValues, boolean expectedBoolean) throws InterruptedException {
    TestUtils.waitForMqtt();
    assertEquals(expectedNumberOfValues, data.numberOfNativeIntValues);
    assertEquals(expectedNumberOfValues, data.numberOfNativeShortValues);
    assertEquals(expectedNumberOfValues, data.numberOfNativeLongValues);
    assertEquals(expectedNumberOfValues, data.numberOfNativeFloatValues);
    assertEquals(expectedNumberOfValues, data.numberOfNativeDoubleValues);
    assertEquals(expectedNumberOfValues, data.numberOfNativeCharValues);
    assertEquals(expectedNumberOfBooleanValues, data.numberOfNativeBooleanValues);

    if (expectedNumberOfValues == 0) {
      return;
    }
    assertEquals(expectedInt, data.lastNativeIntValue);
    assertEquals(expectedShort, data.lastNativeShortValue);
    assertEquals(expectedLong, data.lastNativeLongValue);
    assertEquals(expectedFloat, data.lastNativeFloatValue, TestUtils.DELTA);
    assertEquals(expectedDouble, data.lastNativeDoubleValue, TestUtils.DELTA);
    assertEquals(expectedChar, data.lastNativeCharValue);
    assertEquals(expectedBoolean, data.lastNativeBooleanValue);
  }

  private void checkReceiveData(int expectedInt, short expectedShort, long expectedLong, float expectedFloat, double expectedDouble, char expectedChar) {
    assertEquals(expectedInt, natives.getIntValue());
    assertEquals(expectedShort, natives.getShortValue());
    assertEquals(expectedLong, natives.getLongValue());
    assertEquals(expectedFloat, natives.getFloatValue(), TestUtils.DELTA);
    assertEquals(expectedDouble, natives.getDoubleValue(), TestUtils.DELTA);
    assertEquals(expectedChar, natives.getCharValue());
    assertTrue(natives.getBooleanValue());

    assertEquals(expectedInt, boxes.getIntValue());
    assertEquals(expectedShort, boxes.getShortValue());
    assertEquals(expectedLong, boxes.getLongValue());
    assertEquals(expectedFloat, boxes.getFloatValue(), TestUtils.DELTA);
    assertEquals(expectedDouble, boxes.getDoubleValue(), TestUtils.DELTA);
    assertEquals(expectedChar, boxes.getCharValue());
    assertEquals(true, boxes.getBooleanValue());
  }

  private static class ReceiverData {
    int lastNativeIntValue;
    int numberOfNativeIntValues = 0;
    short lastNativeShortValue;
    int numberOfNativeShortValues = 0;
    long lastNativeLongValue;
    int numberOfNativeLongValues = 0;
    float lastNativeFloatValue;
    int numberOfNativeFloatValues = 0;
    double lastNativeDoubleValue;
    int numberOfNativeDoubleValues = 0;
    char lastNativeCharValue;
    int numberOfNativeCharValues = 0;
    boolean lastNativeBooleanValue;
    int numberOfNativeBooleanValues = 0;
  }

}
