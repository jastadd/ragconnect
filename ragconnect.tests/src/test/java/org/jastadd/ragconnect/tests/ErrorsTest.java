package org.jastadd.ragconnect.tests;

import org.junit.jupiter.api.Test;

import java.io.IOException;

/**
 * Test error messages.
 *
 * @author rschoene - Initial contribution
 */
public class ErrorsTest extends AbstractCompilerTest {

  @Override
  protected String getDirectoryName() {
    return "errors";
  }

  @Override
  protected String getDefaultGrammarName() {
    return "Errors";
  }

  @Test
  void testStandardErrors() throws IOException {
    testAndCompare(getDirectoryName(), "Standard", "A", "Standard");
  }

  @Test
  void testTwoPartsErrors() throws IOException {
    testAndCompare(getDirectoryName(), "Part", "A", "Part1", "Part2");
  }
}
