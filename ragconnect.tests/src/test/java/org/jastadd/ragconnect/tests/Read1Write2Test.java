package org.jastadd.ragconnect.tests;

import org.jastadd.ragconnect.tests.utils.DefaultMappings;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import read1write2.ast.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "read-1-write-2".
 *
 * @author rschoene - Initial contribution
 */
public class Read1Write2Test extends AbstractMqttTest {

  private static final String TOPIC_SAME_READ = "same/read";
  private static final String TOPIC_SAME_WRITE_INT = "same/write/int";
  private static final String TOPIC_SAME_WRITE_STRING = "same/write/string";
  private static final String TOPIC_DIFFERENT_READ = "different/read";
  private static final String TOPIC_DIFFERENT_WRITE1_INT = "different/write1/int";
  private static final String TOPIC_DIFFERENT_WRITE1_STRING = "different/write1/string";
  private static final String TOPIC_DIFFERENT_WRITE2_INT = "different/write2/int";
  private static final String TOPIC_DIFFERENT_WRITE2_STRING = "different/write2/string";
  private static final String INITIAL_VALUE = "-1";

  private MqttHandler handler;
  private A model;
  private OnSameNonterminal onSameNonterminal;
  private OnDifferentNonterminal onDifferentNonterminal;
  private TheOther other1;
  private TheOther other2;

  private ReceiverData dataSame;
  private ReceiverData dataOther1;
  private ReceiverData dataOther2;

  @Override
  protected void createModel() {
    // Setting value for Input without dependencies does not trigger any updates
    model = new A();

    onSameNonterminal = new OnSameNonterminal();
    model.setOnSameNonterminal(onSameNonterminal);
    onSameNonterminal.setInput(INITIAL_VALUE);

    onDifferentNonterminal = new OnDifferentNonterminal();
    other1 = new TheOther();
    other2 = new TheOther();
    onDifferentNonterminal.addTheOther(other1);
    onDifferentNonterminal.addTheOther(other2);
    model.setOnDifferentNonterminal(onDifferentNonterminal);
    onDifferentNonterminal.setInput(INITIAL_VALUE);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    onSameNonterminal.addIntDependency(onSameNonterminal);
    onSameNonterminal.addStringDependency(onSameNonterminal);
    other1.addIntDependency(onDifferentNonterminal);
    other1.addStringDependency(onDifferentNonterminal);
    other2.addIntDependency(onDifferentNonterminal);
    other2.addStringDependency(onDifferentNonterminal);

    dataSame = new Read1Write2Test.ReceiverData();
    dataOther1 = new Read1Write2Test.ReceiverData();
    dataOther2 = new Read1Write2Test.ReceiverData();

    handler.newConnection(TOPIC_SAME_WRITE_INT, bytes -> {
      dataSame.numberOfIntValues += 1;
      dataSame.lastIntValue = DefaultMappings.BytesToInt(bytes);
    });
    handler.newConnection(TOPIC_SAME_WRITE_STRING, bytes -> {
      dataSame.numberOfStringValues += 1;
      dataSame.lastStringValue = DefaultMappings.BytesToString(bytes);
    });

    handler.newConnection(TOPIC_DIFFERENT_WRITE1_INT, bytes -> {
      dataOther1.numberOfIntValues += 1;
      dataOther1.lastIntValue = DefaultMappings.BytesToInt(bytes);
    });
    handler.newConnection(TOPIC_DIFFERENT_WRITE1_STRING, bytes -> {
      dataOther1.numberOfStringValues += 1;
      dataOther1.lastStringValue = DefaultMappings.BytesToString(bytes);
    });

    handler.newConnection(TOPIC_DIFFERENT_WRITE2_INT, bytes -> {
      dataOther2.numberOfIntValues += 1;
      dataOther2.lastIntValue = DefaultMappings.BytesToInt(bytes);
    });
    handler.newConnection(TOPIC_DIFFERENT_WRITE2_STRING, bytes -> {
      dataOther2.numberOfStringValues += 1;
      dataOther2.lastStringValue = DefaultMappings.BytesToString(bytes);
    });

    assertTrue(onSameNonterminal.connectInput(mqttUri(TOPIC_SAME_READ)));
    assertTrue(onSameNonterminal.connectOutInteger(mqttUri(TOPIC_SAME_WRITE_INT), isWriteCurrentValue()));
    assertTrue(onSameNonterminal.connectOutString(mqttUri(TOPIC_SAME_WRITE_STRING), isWriteCurrentValue()));

    assertTrue(onDifferentNonterminal.connectInput(mqttUri(TOPIC_DIFFERENT_READ)));
    assertTrue(other1.connectOutInteger(mqttUri(TOPIC_DIFFERENT_WRITE1_INT), isWriteCurrentValue()));
    assertTrue(other1.connectOutString(mqttUri(TOPIC_DIFFERENT_WRITE1_STRING), isWriteCurrentValue()));
    assertTrue(other2.connectOutInteger(mqttUri(TOPIC_DIFFERENT_WRITE2_INT), isWriteCurrentValue()));
    assertTrue(other2.connectOutString(mqttUri(TOPIC_DIFFERENT_WRITE2_STRING), isWriteCurrentValue()));
  }

  @Override
  protected void communicateSendInitialValue() throws InterruptedException {
    // check initial value
    checkData(1, Integer.parseInt(INITIAL_VALUE), prefixed(INITIAL_VALUE), 1, Integer.parseInt(INITIAL_VALUE), prefixed(INITIAL_VALUE));

    communicateBoth(1);
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException {
// check initial value
    checkData(0, null, null, 0, null, null);

    communicateBoth(0);
  }

  private void communicateBoth(int initialNumberOfValues) throws InterruptedException {
    // set new value
    sendData("2", "3");

    // check new value
    checkData(initialNumberOfValues + 1, 2, prefixed("2"), initialNumberOfValues + 1, 3, prefixed("3"));

    // set new value
    sendData("4", "4");

    // check new value
    checkData(initialNumberOfValues + 2, 4, prefixed("4"), initialNumberOfValues + 2, 4, prefixed("4"));

    // set new value only for same
    handler.publish(TOPIC_SAME_READ, "78".getBytes());

    // check new value
    checkData(initialNumberOfValues + 3, 78, prefixed("78"), initialNumberOfValues + 2, 4, prefixed("4"));
  }

  @Override
  public void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  private String prefixed(String s) {
    return "prefix" + s;
  }

  private void sendData(String inputSame, String inputDifferent) {
    publisher.publish(TOPIC_SAME_READ, inputSame.getBytes());
    publisher.publish(TOPIC_DIFFERENT_READ, inputDifferent.getBytes());
  }

  private void checkData(int numberOfSameValues, Integer lastSameIntValue, String lastSameStringValue,
                         int numberOfDifferentValues, Integer lastDifferentIntValue,
                         String lastDifferentStringValue) throws InterruptedException {
    TestUtils.waitForMqtt();
    dataSame.assertEqualData(numberOfSameValues, lastSameIntValue, lastSameStringValue);
    dataOther1.assertEqualData(numberOfDifferentValues,
        lastDifferentIntValue, lastDifferentStringValue);
    dataOther2.assertEqualData(numberOfDifferentValues,
        lastDifferentIntValue, lastDifferentStringValue);
  }

  private static class ReceiverData {
    int lastIntValue;
    int numberOfIntValues = 0;
    String lastStringValue;
    int numberOfStringValues = 0;

    void assertEqualData(int expectedNumberOfValues,
                         Integer expectedLastIntValue,
                         String expectedLastStringValue) {
      /* the value "-2" is never used in the test, so a test will always fail comparing to this value
         especially, it is not the initial value */
      assertEquals(expectedNumberOfValues, this.numberOfIntValues);
      assertEquals(expectedNumberOfValues, this.numberOfStringValues);
      if (expectedNumberOfValues > 0) {
        assertEquals(expectedLastIntValue != null ? expectedLastIntValue : -2, this.lastIntValue);
      }
      if (expectedNumberOfValues > 0) {
        assertEquals(expectedLastStringValue, this.lastStringValue);
      }
    }
  }
}
