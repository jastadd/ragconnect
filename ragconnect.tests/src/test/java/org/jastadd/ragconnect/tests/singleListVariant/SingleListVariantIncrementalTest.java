package org.jastadd.ragconnect.tests.singleListVariant;

import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Tag;
import singleListVariantInc.ast.MqttHandler;
import singleListVariantInc.ast.ReceiverRoot;
import singleListVariantInc.ast.Root;
import singleListVariantInc.ast.SenderRoot;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "singleList variant incremental".
 *
 * @author rschoene - Initial contribution
 */
@Tag("Incremental")
public class SingleListVariantIncrementalTest extends AbstractSingleListVariantTest {

  private Root model;
  private MqttHandler handler;

  SingleListVariantIncrementalTest() {
    super("singleListVariantInc");
  }

  @Override
  protected void createModel() {
    model = new Root();
    senderRoot = new SenderRoot();
    model.addSenderRoot((SenderRoot) senderRoot);

    ReceiverRoot localReceiverRoot = new ReceiverRoot();
    model.addReceiverRoot(localReceiverRoot);
    receiverRoot = localReceiverRoot;
    assertEquals(0, receiverRoot.getT_EmptyList().getNumChild());
  }

  @Override
  protected void setupReceiverAndConnectPart() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    // no dependencies

    data = new ReceiverData();
    handler.newConnection(TOPIC_T_all, bytes -> data.numberOfElements += 1);
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }
}
