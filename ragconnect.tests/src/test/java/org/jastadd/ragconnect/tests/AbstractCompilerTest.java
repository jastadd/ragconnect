package org.jastadd.ragconnect.tests;

import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.BeforeEach;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.jastadd.ragconnect.tests.utils.TestUtils.readFile;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * TODO: Add description.
 *
 * @author rschoene - Initial contribution
 */
public abstract class AbstractCompilerTest extends RagConnectTest {

  protected abstract String getDirectoryName();

  protected String getOutputDirectory() {
    return TestUtils.OUTPUT_DIRECTORY_PREFIX + getDirectoryName();
  }

  protected abstract String getDefaultGrammarName();

  @BeforeEach
  public void ensureOutputDirectory() {
    File outputDirectory = new File(getOutputDirectory());
    assertTrue((outputDirectory.exists() && outputDirectory.isDirectory()) || outputDirectory.mkdir());
  }

  protected Path test(String inputDirectoryName, int expectedReturnValue, String rootNode, String... connectNames) throws IOException {
    String grammarFile = Paths.get(inputDirectoryName, getDefaultGrammarName() + ".relast").toString();
    List<String> connectFiles = Arrays.stream(connectNames)
            .map(connectName -> Paths.get(inputDirectoryName,connectName + ".connect").toString())
            .collect(Collectors.toList());
    return TestUtils.runCompiler(grammarFile, connectFiles, rootNode,
            getDirectoryName(), expectedReturnValue);
  }

  protected void testAndCompare(String inputDirectoryName, String expectedName, String rootNode, String... connectNames) throws IOException {
    Path outPath = test(inputDirectoryName, 1, rootNode, connectNames);

    final String startOfErrorsPattern = "Errors:\n";
    String out = readFile(outPath, Charset.defaultCharset());
    assertThat(out).contains(startOfErrorsPattern);
    out = out.substring(out.indexOf(startOfErrorsPattern) + startOfErrorsPattern.length());

    TestUtils.assertLinesMatch(getDirectoryName(), expectedName, out);

    logger.info("ragconnect for " + expectedName + " returned:\n{}", out);
  }
}
