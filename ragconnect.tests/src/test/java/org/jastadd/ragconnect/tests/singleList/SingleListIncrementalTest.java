package org.jastadd.ragconnect.tests.singleList;

import org.junit.jupiter.api.Tag;
import singleListInc.ast.A;
import singleListInc.ast.ReceiverRoot;
import singleListInc.ast.Root;
import singleListInc.ast.SenderRoot;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test case "single list incremental".
 *
 * @author rschoene - Initial contribution
 */
@Tag("Incremental")
public class SingleListIncrementalTest extends AbstractSingleListTest {

  private Root model;

  SingleListIncrementalTest() {
    super("singleListInc");
  }

  @Override
  protected void createModel() {
    model = new Root();
    senderRoot = new SenderRoot();
    model.addSenderRoot((SenderRoot) senderRoot);

    ReceiverRoot localReceiverRoot = new ReceiverRoot();
    model.addReceiverRoot(localReceiverRoot);

    // first prepare non-wildcard lists
    for (int i = 0; i < 5; i++) {
      localReceiverRoot.addA(new A());
    }
    receiverRoot = localReceiverRoot;
    assertEquals(5, receiverRoot.getNumA());
  }

  @Override
  protected void setupReceiverAndConnectPart() {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    // no dependencies
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }
}
