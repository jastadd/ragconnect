package org.jastadd.ragconnect.tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base for all RagConnect tests.
 *
 * @author rschoene - Initial contribution
 */
public class RagConnectTest {
  protected Logger logger = LoggerFactory.getLogger(getClass());

  @BeforeEach
  public void logStart(TestInfo testInfo) {
    logger.info("Starting {}.{}", getClass().getSimpleName(), testInfo.getDisplayName());
  }

  @AfterEach
  public void logEnd(TestInfo testInfo) {
    logger.info("Finished {}.{}", getClass().getSimpleName(), testInfo.getDisplayName());
  }
}
