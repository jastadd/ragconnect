package org.jastadd.ragconnect.tests;

import attributeInc.ast.*;
import de.tudresden.inf.st.jastadd.dumpAst.ast.Dumper;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.function.Supplier;
import org.jastadd.ragconnect.tests.utils.TestChecker;

import static java.util.function.Predicate.isEqual;
import static org.assertj.core.api.Assertions.assertThat;
import static org.jastadd.ragconnect.tests.utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test case "attribute".
 *
 * @author rschoene - Initial contribution
 */
@Tag("Incremental")
public class AttributeTest extends AbstractMqttTest {

  private static final String TOPIC_WILDCARD = "attr/#";
  private static final String TOPIC_BASIC = "attr/string/basic";
  private static final String TOPIC_SIMPLE_NO_MAPPING = "attr/string/simple/plain";
  private static final String TOPIC_SIMPLE_WITH_MAPPING = "attr/string/simple/mapped";
  private static final String TOPIC_TRANSFORMED_NO_MAPPING = "attr/int/transformed/plain";
  private static final String TOPIC_TRANSFORMED_WITH_MAPPING = "attr/int/transformed/mapped";
  private static final String TOPIC_REFERENCE_TYPE_NO_MAPPING = "attr/a/ref/plain";
  private static final String TOPIC_REFERENCE_TYPE_WITH_MAPPING = "attr/a/ref/mapped";
  private static final String TOPIC_NTA_NO_MAPPING = "attr/a/nta/plain";
  private static final String TOPIC_NTA_WITH_MAPPING = "attr/a/nta/mapped";
  private static final String TOPIC_CIRCULAR_NO_MAPPING = "attr/a/circular/plain";
  private static final String TOPIC_CIRCULAR_WITH_MAPPING = "attr/a/circular/mapped";
  private static final String TOPIC_COLLECTION_NO_MAPPING = "attr/a/collection/plain";
  private static final String TOPIC_COLLECTION_WITH_MAPPING = "attr/a/collection/mapped";

  private static final String INITIAL_STRING = "initial";
  private static final String INITIAL_STRING_FOR_INT = "1";
  private static final String INITIAL_STRING_FOR_INT_PLUS_2 = Integer.toString(Integer.parseInt(INITIAL_STRING_FOR_INT) + 2);

  private static final String CHECK_BASIC = "basic";
  private static final String CHECK_SIMPLE = "simple";
  private static final String CHECK_TRANSFORMED = "transformed";
  private static final String CHECK_A = "a";
  private static final String CHECK_NTA = "nta";
  private static final String CHECK_CIRCULAR = "circular";
  private static final String CHECK_COLLECTION = "collection";

  private MqttHandler handler;
  private ReceiverData data;
  private TestChecker checker;

  private Root model;
  private SenderRoot senderString;
  private SenderRoot senderInt;
  private SenderRoot senderA;
  private ReceiverRoot receiverRoot;

  @Override
  protected void createModel() {
    model = new Root();
//    model.trace().setReceiver(TestUtils::logEvent);
    senderString = new SenderRoot().setInput(INITIAL_STRING);
    senderInt = new SenderRoot().setInput(INITIAL_STRING_FOR_INT);
    senderA = new SenderRoot().setInput(INITIAL_STRING);
    receiverRoot = new ReceiverRoot();
    model.addSenderRoot(senderString);
    model.addSenderRoot(senderInt);
    model.addSenderRoot(senderA);
    model.setReceiverRoot(receiverRoot);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException, InterruptedException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);
    handler = new MqttHandler().setHost(TestUtils.getMqttHost()).dontSendWelcomeMessage();
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    data = new ReceiverData();
    assertTrue(handler.newConnection(TOPIC_WILDCARD, bytes -> data.numberOfValues += 1));

    checker = new TestChecker();
    checker.setActualNumberOfValues(() -> data.numberOfValues)
            .setCheckForString(CHECK_BASIC, this::checkBasic)
            .setCheckForString(CHECK_SIMPLE, this::checkSimple)
            .setCheckForString(CHECK_TRANSFORMED, this::checkTransformed)
            .setCheckForString(CHECK_A, this::checkA)
            .setCheckForString(CHECK_NTA, this::checkNta)
            .setCheckForString(CHECK_CIRCULAR, this::checkCircular)
            .setCheckForString(CHECK_COLLECTION, this::checkCollection)
    ;

    // connect receive
    assertTrue(receiverRoot.connectFromBasic(mqttUri(TOPIC_BASIC)));
    assertTrue(receiverRoot.connectFromSimpleNoMapping(mqttUri(TOPIC_SIMPLE_NO_MAPPING)));
    assertTrue(receiverRoot.connectFromSimpleWithMapping(mqttUri(TOPIC_SIMPLE_WITH_MAPPING)));
    assertTrue(receiverRoot.connectFromTransformedNoMapping(mqttUri(TOPIC_TRANSFORMED_NO_MAPPING)));
    assertTrue(receiverRoot.connectFromTransformedWithMapping(mqttUri(TOPIC_TRANSFORMED_WITH_MAPPING)));
    assertTrue(receiverRoot.connectFromReferenceTypeNoMapping(mqttUri(TOPIC_REFERENCE_TYPE_NO_MAPPING)));
    assertTrue(receiverRoot.connectFromReferenceTypeWithMapping(mqttUri(TOPIC_REFERENCE_TYPE_WITH_MAPPING)));
    assertTrue(receiverRoot.connectFromNTANoMapping(mqttUri(TOPIC_NTA_NO_MAPPING)));
    assertTrue(receiverRoot.connectFromNTAWithMapping(mqttUri(TOPIC_NTA_WITH_MAPPING)));
    assertTrue(receiverRoot.connectFromCircularNoMapping(mqttUri(TOPIC_CIRCULAR_NO_MAPPING)));
    assertTrue(receiverRoot.connectFromCircularWithMapping(mqttUri(TOPIC_CIRCULAR_WITH_MAPPING)));
    assertTrue(receiverRoot.connectFromCollectionNoMapping(mqttUri(TOPIC_COLLECTION_NO_MAPPING)));
    assertTrue(receiverRoot.connectFromCollectionWithMapping(mqttUri(TOPIC_COLLECTION_WITH_MAPPING)));

    // connect send, and wait to receive (if writeCurrentValue is set)
    assertTrue(senderString.connectBasic(mqttUri(TOPIC_BASIC), isWriteCurrentValue()));
    assertTrue(senderString.connectSimple(mqttUri(TOPIC_SIMPLE_NO_MAPPING), isWriteCurrentValue()));
    assertTrue(senderString.connectSimple(mqttUri(TOPIC_SIMPLE_WITH_MAPPING), isWriteCurrentValue()));
    assertTrue(senderString.connectCollectionAttribute(mqttUri(TOPIC_COLLECTION_NO_MAPPING), isWriteCurrentValue()));
    assertTrue(senderString.connectCollectionAttribute(mqttUri(TOPIC_COLLECTION_WITH_MAPPING), isWriteCurrentValue()));

    assertTrue(senderInt.connectTransformed(mqttUri(TOPIC_TRANSFORMED_NO_MAPPING), isWriteCurrentValue()));
    assertTrue(senderInt.connectTransformed(mqttUri(TOPIC_TRANSFORMED_WITH_MAPPING), isWriteCurrentValue()));
    assertTrue(senderInt.connectCircularAttribute(mqttUri(TOPIC_CIRCULAR_NO_MAPPING), isWriteCurrentValue()));
    assertTrue(senderInt.connectCircularAttribute(mqttUri(TOPIC_CIRCULAR_WITH_MAPPING), isWriteCurrentValue()));

    assertTrue(senderA.connectToReferenceType(mqttUri(TOPIC_REFERENCE_TYPE_NO_MAPPING), isWriteCurrentValue()));
    assertTrue(senderA.connectToReferenceType(mqttUri(TOPIC_REFERENCE_TYPE_WITH_MAPPING), isWriteCurrentValue()));
    assertTrue(senderA.connectToNTA(mqttUri(TOPIC_NTA_NO_MAPPING), isWriteCurrentValue()));
    assertTrue(senderA.connectToNTA(mqttUri(TOPIC_NTA_WITH_MAPPING), isWriteCurrentValue()));

    waitForValue(senderString.basic(), receiverRoot::getFromBasic);
    waitForValue(senderString.simple(), receiverRoot::getFromSimpleNoMapping);
    waitForValue(senderInt.transformed(), receiverRoot::getFromTransformedNoMapping);
    waitForNonNull(receiverRoot::getFromCollectionNoMapping);
    waitForNonNull(receiverRoot::getFromReferenceTypeNoMapping);
    waitForNonNull(receiverRoot::getFromNTANoMapping);
  }

  @Override
  protected void communicateSendInitialValue() throws IOException, InterruptedException {
    // 13 = basic, simple(2), collection(2), transformed(2), circular(2), ref-type(2), nta(2)
    checker.addToNumberOfValues(13)
            .put(CHECK_BASIC, INITIAL_STRING)
            .put(CHECK_SIMPLE, INITIAL_STRING + "Post")
            .put(CHECK_TRANSFORMED, INITIAL_STRING_FOR_INT)
            .put(CHECK_A, INITIAL_STRING)
            .put(CHECK_NTA, INITIAL_STRING)
            .put(CHECK_CIRCULAR, INITIAL_STRING_FOR_INT_PLUS_2)
            .put(CHECK_COLLECTION, "[" + INITIAL_STRING + "]");

    if (!TestUtils.isCi()) {
      Dumper.read(model)
              .includeAttributeWhen((node, attributeName, isNTA, value) -> {
                switch (attributeName) {
                  case "basic":
                  case "simple":
                  case "collectionAttribute":
                    return node.equals(senderString);
                  case "transformed":
                  case "circularAttribute":
                    return node.equals(senderInt);
                  case "toReferenceType":
                  case "toNTA":
                    return node.equals(senderA);
                }
                return false;
              })
              .includeChildWhen((parentNode, childNode, contextName) -> !contextName.equals("Inner"))
              .setNameMethod(node -> {
                if (node instanceof SenderRoot) {
                  String suffix = node.equals(senderString) ? "(String)" :
                          node.equals(senderInt) ? "(int)" : "(reference type)";
                  return "SenderRoot " + suffix;
                }
                return node.getClass().getSimpleName();
              })
              .dumpAsPNG(Paths.get("attribute.png"));
    }

    communicateBoth();
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws IOException, InterruptedException {
    checker.put(CHECK_BASIC, (String) null)
            .put(CHECK_SIMPLE, (String) null)
            .put(CHECK_TRANSFORMED, (String) null)
            .put(CHECK_A, (String) null)
            .put(CHECK_NTA, (String) null)
            .put(CHECK_COLLECTION, (String) null);

    communicateBoth();
  }

  private void communicateBoth() throws IOException {
    // basic, simple(2), collection(2) <-- senderString
    // transformed(2), circular(2)     <-- senderInt
    // ref-type(2), nta(2)             <-- senderA
    checker.check();

    senderString.setInput("test-01");
    checker.addToNumberOfValues(5) // basic, simple(2), collection(2)
            .put(CHECK_BASIC, "test-01")
            .put(CHECK_SIMPLE, "test-01Post")
            .put(CHECK_COLLECTION, "[test-01]")
            .check();

    // no change for same value
    senderString.setInput("test-01");
    checker.check();

    senderString.addA(new A().setValue("test-02").setInner(new Inner().setInnerValue("inner")));
    checker.addToNumberOfValues(2) // collection(2)
            .put(CHECK_COLLECTION, "[test-01, test-02]")
            .check();

    senderInt.setInput("20");
    checker.addToNumberOfValues(4) // transformed(2), circular(2)
            .put(CHECK_TRANSFORMED, "20")
            .put(CHECK_CIRCULAR, "22")
            .check();

    senderA.setInput("test-03");
    checker.addToNumberOfValues(4) // ref-type(2), nta(2)
            .put(CHECK_A, "test-03")
            .put(CHECK_NTA, "test-03")
            .check();

    assertTrue(senderString.disconnectSimple(mqttUri(TOPIC_SIMPLE_NO_MAPPING)));
    assertTrue(senderString.disconnectSimple(mqttUri(TOPIC_SIMPLE_WITH_MAPPING)));
    senderString.setInput("test-04");
    checker.addToNumberOfValues(3) // basic, collection(2)
            .put(CHECK_BASIC, "test-04")
            .put(CHECK_COLLECTION, "[test-02, test-04]")
            .check();

    assertTrue(senderString.disconnectCollectionAttribute(mqttUri(TOPIC_COLLECTION_NO_MAPPING)));
    assertTrue(senderString.disconnectCollectionAttribute(mqttUri(TOPIC_COLLECTION_WITH_MAPPING)));
    senderString.setInput("test-05");
    checker.incNumberOfValues() // basic
            .put(CHECK_BASIC, "test-05")
            .check();

    assertTrue(senderA.disconnectToNTA(mqttUri(TOPIC_NTA_NO_MAPPING)));
    senderA.setInput("test-06");
    checker.addToNumberOfValues(3)
            .put(CHECK_A, "test-06")
            .check();
  }

  private <T> void waitForValue(T expectedValue, Callable<T> callable) {
    if (isWriteCurrentValue()) {
      awaitMqtt().until(callable, isEqual(expectedValue));
    }
  }

  private <T> void waitForNonNull(Callable<T> callable) {
    if (isWriteCurrentValue()) {
      awaitMqtt().until(callable, Predicate.not(isEqual(null)));
    }
  }

  private void checkBasic(String name, String expected) {
    Assertions.assertEquals(Objects.requireNonNullElse(expected, ""), receiverRoot.getFromBasic(), name);
  }

  private void checkSimple(String name, String expected) {
    if (expected != null) {
      assertEquals(expected, receiverRoot.getFromSimpleNoMapping(), "simple");
      assertEquals(expected + "post", receiverRoot.getFromSimpleWithMapping(), "simple mapped");
    } else {
      assertEquals("", receiverRoot.getFromSimpleNoMapping(), "simple null");
      assertEquals("", receiverRoot.getFromSimpleWithMapping(), "simple mapped null");
    }
  }

  private void checkTransformed(String name, String expected) {
    _checkInt(name, expected, receiverRoot::getFromTransformedNoMapping, receiverRoot::getFromTransformedWithMapping);
  }

  private void _checkInt(String name, String expected, Supplier<Integer> noMapping, Supplier<Integer> withMapping) {
    if (expected != null) {
      assertEquals(Integer.parseInt(expected), noMapping.get(), name);
      assertEquals(Integer.parseInt(expected) + 1, withMapping.get(), name + " mapped");
    } else {
      assertEquals(0, noMapping.get(), name + " null");
      assertEquals(0, withMapping.get(), name + " mapped null");
    }
  }

  private void checkA(String name, String expected) {
    if (expected != null) {
      assertA(expected, "1",
              receiverRoot.getFromReferenceTypeNoMapping(), "ref-type");
      assertA(expected + "post", "inner1",
              receiverRoot.getFromReferenceTypeWithMapping(), "ref-type mapped");
      assertA(expected + "post", "inner2",
              receiverRoot.getFromNTAWithMapping(), "nta mapped");
    } else {
      assertNull(receiverRoot.getFromReferenceTypeNoMapping(), "manual ref-type null");
      assertNull(receiverRoot.getFromReferenceTypeWithMapping(), "ref-type mapped null");
      assertNull(receiverRoot.getFromNTAWithMapping(), "nta mapped null");
    }
  }

  private void checkNta(String name, String expected) {
    if (expected != null) {
      assertA(expected, "2", receiverRoot.getFromNTANoMapping(), "nta");
    } else {
      assertNull(receiverRoot.getFromNTANoMapping(), "nta null");
    }
  }

  private void checkCircular(String name, String expected) {
    _checkInt(name, expected, receiverRoot::getFromCircularNoMapping, receiverRoot::getFromCircularWithMapping);
  }

  private void checkCollection(String name, String expected) {
    if (expected != null) {
      assertThat(receiverRoot.getFromCollectionWithMapping()).hasSizeGreaterThan(4).endsWith("post");
      checkCollectionContent(name, expected, receiverRoot.getFromCollectionNoMapping());
      checkCollectionContent(name + " mapped", expected, receiverRoot.getFromCollectionWithMapping().substring(0, receiverRoot.getFromCollectionWithMapping().length() - 4));
    } else {
      assertEquals("", receiverRoot.getFromCollectionNoMapping(), "collection null");
      assertEquals("", receiverRoot.getFromCollectionWithMapping(), "collection mapped null");
    }
  }

  private void checkCollectionContent(String name, String expected, String actual) {
    assertThat(actual).as(name)
            .startsWith("[")
            .endsWith("]");
    String[] actualValues = actual.substring(1, actual.length() - 1).split(", ");
    String[] expectedValues = expected.substring(1, expected.length() - 1).split(", ");
    assertThat(actualValues).containsExactlyInAnyOrder(expectedValues);
  }

  private void assertA(String expectedValue, String expectedInner, A actual, String message) {
    assertEquals(expectedValue, actual.getValue(), message + " value");
    assertEquals(expectedInner, actual.getInner().getInnerValue(), message + " inner");
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  private static class ReceiverData {
    int numberOfValues = 0;
  }
}
