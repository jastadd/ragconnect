package org.jastadd.ragconnect.tests.tree;

import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Test;
import tree.ast.MqttHandler;
import tree.ast.ReceiverRoot;
import tree.ast.Root;
import tree.ast.SenderRoot;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.jastadd.ragconnect.tests.utils.TestUtils.testJaddContainReferenceToJackson;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "tree manual"
 *
 * @author rschoene - Initial contribution
 */
public class TreeManualTest extends AbstractTreeTest {

  private Root model;
  private SenderRoot senderRoot;
  private MqttHandler handler;

  @Test
  public void checkJacksonReference() {
    testJaddContainReferenceToJackson(
        Paths.get("src", "test",
            "02-after-ragconnect", "tree", "RagConnect.jadd"), true);
  }

  @Override
  protected void createModel() {
    model = new Root();
    senderRoot = new SenderRoot();
    model.addSenderRoot(senderRoot);

    receiverRoot = new ReceiverRoot();
    model.addReceiverRoot((ReceiverRoot) receiverRoot);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    // add dependencies
    senderRoot.addInputDependency(senderRoot);

    data = new ReceiverData();
    handler.newConnection(TOPIC_ALFA, bytes -> data.numberOfTrees += 1);

    // connect. important: first receiver, then sender. to not miss initial value.
    assertTrue(receiverRoot.connectAlfa(mqttUri(TOPIC_ALFA)));
    assertTrue(senderRoot.connectAlfa(mqttUri(TOPIC_ALFA), isWriteCurrentValue()));
  }

  protected void setInput(int input) {
    senderRoot.setInput(input);
  }

  @Override
  protected void disconnectReceive() throws IOException {
    assertTrue(receiverRoot.disconnectAlfa(mqttUri(TOPIC_ALFA)));
  }

  @Override
  protected void disconnectSend() throws IOException {
    assertTrue(senderRoot.disconnectAlfa(mqttUri(TOPIC_ALFA)));
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }
}
