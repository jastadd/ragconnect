package org.jastadd.ragconnect.tests.list;

import list.ast.MqttHandler;
import list.ast.ReceiverRoot;
import list.ast.Root;
import list.ast.SenderRoot;
import org.jastadd.ragconnect.tests.utils.TestUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "list manual".
 *
 * @author rschoene - Initial contribution
 */
public class ListManualTest extends AbstractListTest {

  private Root model;
  private SenderRoot senderRoot;
  private MqttHandler handler;

  ListManualTest() {
    super("list");
  }

  @Override
  protected void createModel() {
    model = new Root();
    senderRoot = new SenderRoot();
    senderRoot.setInput(0);
    model.addSenderRoot(senderRoot);

    receiverRoot = new ReceiverRoot();
    model.addReceiverRoot((ReceiverRoot) receiverRoot);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    // add dependencies
    senderRoot.addInputDependencyToA(senderRoot);
    senderRoot.addInputDependencyToSingleA(senderRoot);

    data = new ReceiverData();
    dataSingle = new ReceiverData();
    handler.newConnection(TOPIC_A, bytes -> data.numberOfElements += 1);
    handler.newConnection(TOPIC_SINGLE_A, bytes -> dataSingle.numberOfElements += 1);

    // connect. important: first receivers, then senders. to not miss initial value.
    assertTrue(receiverRoot.connectAList(mqttUri(TOPIC_A)));
    assertTrue(receiverRoot.connectFromSingleAList(mqttUri(TOPIC_SINGLE_A)));
    assertTrue(receiverRoot.connectWithAddFromAList(mqttUri(TOPIC_A)));
    assertTrue(receiverRoot.connectWithAddFromSingleAList(mqttUri(TOPIC_SINGLE_A)));

    assertTrue(senderRoot.connectAList(mqttUri(TOPIC_A), isWriteCurrentValue()));
    assertTrue(senderRoot.connectSingleAList(mqttUri(TOPIC_SINGLE_A), isWriteCurrentValue()));
  }

  @Override
  protected void setInput(int input) {
    senderRoot.setInput(input);
    assertEquals(input, senderRoot.getNumA(), "size of normal NTA");
    assertEquals(1, senderRoot.getNumSingleA(), "size of single NTA");
  }

  @Override
  protected void disconnectReceive() throws IOException {
    ReceiverRoot receiverRootWithLocalType = (ReceiverRoot) receiverRoot;
    assertTrue(receiverRootWithLocalType.disconnectAList(mqttUri(TOPIC_A)));
    assertTrue(receiverRootWithLocalType.disconnectFromSingleAList(mqttUri(TOPIC_SINGLE_A)));
    assertTrue(receiverRootWithLocalType.disconnectWithAddFromAList(mqttUri(TOPIC_A)));
    assertTrue(receiverRootWithLocalType.disconnectWithAddFromSingleAList(mqttUri(TOPIC_SINGLE_A)));
  }

  @Override
  protected void disconnectSend() throws IOException {
    assertTrue(senderRoot.disconnectAList(mqttUri(TOPIC_A)));
    assertTrue(senderRoot.disconnectSingleAList(mqttUri(TOPIC_SINGLE_A)));
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }
}
