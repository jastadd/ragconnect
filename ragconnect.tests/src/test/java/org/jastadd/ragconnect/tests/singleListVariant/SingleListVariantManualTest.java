package org.jastadd.ragconnect.tests.singleListVariant;

import org.jastadd.ragconnect.tests.utils.TestUtils;
import singleListVariant.ast.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "singleList variant manual".
 *
 * @author rschoene - Initial contribution
 */
public class SingleListVariantManualTest extends AbstractSingleListVariantTest {

  private Root model;
  private MqttHandler handler;

  SingleListVariantManualTest() {
    super("singleListVariant");
  }

  @Override
  protected void createModel() {
    model = new Root();
    senderRoot = new SenderRoot();
    model.addSenderRoot((SenderRoot) senderRoot);

    ReceiverRoot localReceiverRoot = new ReceiverRoot();
    model.addReceiverRoot(localReceiverRoot);
    receiverRoot = localReceiverRoot;
    assertEquals(0, receiverRoot.getT_EmptyList().getNumChild());
  }

  @Override
  protected void setupReceiverAndConnectPart() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    // add dependencies: input
    ((SenderRoot) senderRoot).addInputDependencyToT_Empty((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addInputDependencyToT_Token((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addInputDependencyToT_OneChild((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addInputDependencyToT_OneOpt((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addInputDependencyToT_OneList((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addInputDependencyToT_TwoChildren((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addInputDependencyToT_OneOfEach((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addInputDependencyToT_Abstract((SenderRoot) senderRoot);
    // add dependencies: shouldSetOptAndList
    ((SenderRoot) senderRoot).addShouldSetOptAndListDependencyToT_OneOpt((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addShouldSetOptAndListDependencyToT_OneList((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addShouldSetOptAndListDependencyToT_OneOfEach((SenderRoot) senderRoot);

    data = new ReceiverData();
    handler.newConnection(TOPIC_T_all, bytes -> data.numberOfElements += 1);
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }
}
