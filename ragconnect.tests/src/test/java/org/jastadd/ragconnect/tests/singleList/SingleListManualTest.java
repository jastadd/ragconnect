package org.jastadd.ragconnect.tests.singleList;

import singleList.ast.A;
import singleList.ast.ReceiverRoot;
import singleList.ast.Root;
import singleList.ast.SenderRoot;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test case "single list manual".
 *
 * @author rschoene - Initial contribution
 */
public class SingleListManualTest extends AbstractSingleListTest {

  private Root model;

  SingleListManualTest() {
    super("singleList");
  }

  @Override
  protected void createModel() {
    model = new Root();
    senderRoot = new SenderRoot();
    model.addSenderRoot((SenderRoot) senderRoot);

    ReceiverRoot localReceiverRoot = new ReceiverRoot();
    model.addReceiverRoot(localReceiverRoot);

    // first prepare non-wildcard lists
    for (int i = 0; i < 5; i++) {
      localReceiverRoot.addA(new A());
    }
    receiverRoot = localReceiverRoot;
    assertEquals(5, receiverRoot.getNumA());
  }

  @Override
  protected void setupReceiverAndConnectPart() {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    // add dependencies
    ((SenderRoot) senderRoot).addInputDependencyToA1((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addInputDependencyToA2((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addInputDependencyToA3((SenderRoot) senderRoot);
    ((SenderRoot) senderRoot).addInputDependencyToA4((SenderRoot) senderRoot);
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }
}
