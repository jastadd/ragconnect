package org.jastadd.ragconnect.tests;

import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.jastadd.ragconnect.tests.utils.TestUtils.readFile;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test warning messages.
 *
 * @author rschoene - Initial contribution
 */
public class WarningsTest extends RagConnectTest {

  private static final String WARNING_DIRECTORY = "warnings/";
  private static final String OUTPUT_DIRECTORY = TestUtils.OUTPUT_DIRECTORY_PREFIX + WARNING_DIRECTORY;

  private static final String DEFAULT_GRAMMAR_NAME = "Test";

  private static final String OPTION_INCREMENTAL_PARAM = "--incremental=param";
  private static final String OPTION_TRACE_FLUSH = "--tracing=flush";
  private static final String OPTION_CACHE_ALL = "--cache=all";

  @BeforeAll
  public static void createOutputDirectory() {
    File outputDirectory = new File(OUTPUT_DIRECTORY);
    assertTrue((outputDirectory.exists() && outputDirectory.isDirectory()) || outputDirectory.mkdir());
  }

  @Test
  public void testNoDependenciesAndInc() throws IOException {
    // pass "null" as expectedName means that no warnings are expected
    test(null, "TestNoDependencies", OPTION_INCREMENTAL_PARAM, OPTION_TRACE_FLUSH);
  }

  @Test
  public void testSomeDependenciesAndInc() throws IOException {
    test("SomeDependenciesAndInc", "TestSomeDependencies",
            OPTION_INCREMENTAL_PARAM, OPTION_TRACE_FLUSH);
  }

  @Test
  public void testNoDependenciesAndNoInc() throws IOException {
    test("NoDependenciesAndNoInc", "TestNoDependencies");
  }

  @Test
  public void testNoIncAndCacheAll() throws IOException {
    test("NoDependenciesAndNoIncAndCacheAll", "TestNoDependencies", OPTION_CACHE_ALL);
    test("SomeDependenciesAndNoIncAndCacheAll", "TestSomeDependencies", OPTION_CACHE_ALL);
  }

  private void test(String expectedName, String connectName, String... additionalArguments) throws IOException {
    String grammarFile = WARNING_DIRECTORY + DEFAULT_GRAMMAR_NAME + ".relast";
    List<String> connectFiles = Collections.singletonList(WARNING_DIRECTORY + connectName + ".connect");
    Path outPath = TestUtils.runCompiler(
            grammarFile, connectFiles, "Root", WARNING_DIRECTORY, 0, additionalArguments
    );

    String out = readFile(outPath, Charset.defaultCharset());
    final boolean expectWarnings = expectedName != null;
    final String startOfWarningsPattern = "Warnings:\n";
    if (expectWarnings) {
      assertThat(out).contains(startOfWarningsPattern);
    } else {
      assertThat(out).doesNotContain(startOfWarningsPattern);
    }

    final String startOfErrorsPattern = "Errors:\n";
    assertThat(out).doesNotContain(startOfErrorsPattern);

    if (!expectWarnings) {
      return;
    }

    out = out.substring(out.indexOf(startOfWarningsPattern) + startOfWarningsPattern.length());

    logger.debug("ragconnect for " + expectedName + " returned:\n{}", out);

    TestUtils.assertLinesMatch(WARNING_DIRECTORY, expectedName, out);
  }
}
