package org.jastadd.ragconnect.tests.tree;

import org.jastadd.ragconnect.tests.AbstractMqttTest;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Tag;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Base class for test cases "tree manual" and "tree incremental".
 *
 * @author rschoene - Initial contribution
 */
@Tag("Tree")
public abstract class AbstractTreeTest extends AbstractMqttTest {
  protected static final String TOPIC_ALFA = "alfa";
  protected ReceiverData data;
  protected TestWrapperReceiverRoot receiverRoot;

  public interface TestWrapperReceiverRoot {
    TestWrapperAlfa getAlfa();
    boolean connectAlfa(String mqttUri) throws IOException;
    boolean disconnectAlfa(String mqttUri) throws IOException;
  }
  public interface TestWrapperAlfa {
    TestWrapperBravo getBravo(int i);
    int getNumBravo();
    int getNumCharlie();
    int getNumDelta();

    TestWrapperAlfa getMyself();
    TestWrapperBravo getMyBravo();
    boolean hasOptionalBravo();
    TestWrapperBravo getOptionalBravo();
    TestWrapperCharlie getCharlie(int i);
    TestWrapperDelta getDelta(int i);
    <T extends TestWrapperBravo> List<T> getMultiBravoList();
  }
  public interface TestWrapperBravo {

    TestWrapperCharlie getMyCharlie();
    boolean hasOptionalCharlie();
    TestWrapperCharlie getOptionalCharlie();
    <T extends TestWrapperCharlie> List<T> getMultiCharlieList();
    boolean hasSingleBi1Delta();
    TestWrapperDelta getSingleBi1Delta();
    <T extends TestWrapperDelta> List<T> getMultiBi2DeltaList();
    <T extends TestWrapperDelta> List<T> getMultiBi3DeltaList();
  }
  public interface TestWrapperCharlie {
    boolean hasMyAlfa();
    TestWrapperAlfa getMyAlfa();
  }
  public interface TestWrapperDelta {
    boolean hasSingleBack1Alfa();
    TestWrapperAlfa getSingleBack1Alfa();
    boolean hasSingleBack2Alfa();
    TestWrapperAlfa getSingleBack2Alfa();
    <T extends TestWrapperAlfa> List<T> getMultiBack3AlfaList();
    boolean hasSingleBack1Bravo();
    TestWrapperBravo getSingleBack1Bravo();
    TestWrapperBravo getSingleBack2Bravo();
    boolean hasSingleBack2Bravo();
    <T extends TestWrapperBravo> List<T> getMultiBack3BravoList();
  }

  @Override
  protected void communicateSendInitialValue() throws InterruptedException, IOException {
    checkTree(1, 0);

    setInput(1);
    checkTree(2, 1);

    setInput(1);
    checkTree(2, 1);

    setInput(2);
    checkTree(3, 2);

    disconnectReceive();
    setInput(1);
    checkTree(4, 2);

    disconnectSend();
    setInput(2);
    checkTree(4, 2);
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException, IOException {
    checkTree(0, null);

    setInput(1);
    checkTree(1, 1);

    setInput(1);
    checkTree(1, 1);

    setInput(2);
    checkTree(2, 2);

    disconnectReceive();
    setInput(1);
    checkTree(3, 2);

    disconnectSend();
    setInput(2);
    checkTree(3, 2);
  }

  protected abstract void disconnectReceive() throws IOException;

  protected abstract void disconnectSend() throws IOException;

  protected abstract void setInput(int input);

  protected void checkTree(int expectedCount, Integer expectedInput) throws InterruptedException {
    TestUtils.waitForMqtt();

    assertEquals(expectedCount, data.numberOfTrees);
    if (expectedInput == null) {
      assertNull(receiverRoot.getAlfa());
    } else {
      assertNotNull(receiverRoot.getAlfa());
      TestWrapperAlfa alfa = receiverRoot.getAlfa();
      assertEquals(4, alfa.getNumBravo());
      assertEquals(4, alfa.getNumCharlie());
      assertEquals(4, alfa.getNumDelta());

      TestWrapperBravo inputBravo = alfa.getBravo(expectedInput);
      TestWrapperCharlie inputCharlie = alfa.getCharlie(expectedInput);
      TestWrapperDelta inputDelta = alfa.getDelta(expectedInput);

      // Alfa -> Alfa
      assertEquals(alfa, alfa.getMyself());

      // Alfa -> Bravo
      assertEquals(inputBravo, alfa.getMyBravo());
      assertTrue(alfa.hasOptionalBravo());
      assertEquals(inputBravo, alfa.getOptionalBravo());
      assertThat(alfa.getMultiBravoList()).containsExactly(
          inputBravo, alfa.getBravo(expectedInput + 1));

      // Charlie -> Alfa
      for (int i = 0; i < 4; i++) {
        TestWrapperCharlie Charlie = alfa.getCharlie(i);
        if (i == expectedInput) {
          assertTrue(Charlie.hasMyAlfa());
          assertEquals(alfa, Charlie.getMyAlfa());
        } else {
          assertFalse(Charlie.hasMyAlfa());
        }
      }

      // Alfa <-> Delta
      for (int i = 0; i < 4; i++) {
        TestWrapperDelta Delta = alfa.getDelta(i);
        if (i == expectedInput) {
          assertTrue(Delta.hasSingleBack1Alfa());
          assertEquals(alfa, Delta.getSingleBack1Alfa());
        } else {
          assertFalse(Delta.hasSingleBack1Alfa());
        }
        if (i == expectedInput || i == expectedInput + 1) {
          assertTrue(Delta.hasSingleBack2Alfa());
          assertEquals(alfa, Delta.getSingleBack2Alfa());
          assertThat(Delta.getMultiBack3AlfaList()).containsExactly(alfa);
        } else {
          assertFalse(Delta.hasSingleBack2Alfa());
          assertThat(Delta.getMultiBack3AlfaList()).isEmpty();
        }
      }

      // Bravo -> Charlie
      for (int i = 0; i < 4; i++) {
        TestWrapperBravo Bravo = alfa.getBravo(i);

        if (i == expectedInput) {
          assertEquals(inputCharlie, Bravo.getMyCharlie());
          assertTrue(Bravo.hasOptionalCharlie());
          assertEquals(inputCharlie, Bravo.getOptionalCharlie());
          assertThat(Bravo.getMultiCharlieList()).containsExactly(
              inputCharlie, alfa.getCharlie(expectedInput + 1));
        } else {
          assertEquals(alfa.getCharlie(0), Bravo.getMyCharlie());
          assertFalse(Bravo.hasOptionalCharlie());
          assertThat(Bravo.getMultiCharlieList()).isEmpty();
        }
      }

      // Bravo <-> Delta
      for (int i = 0; i < 4; i++) {
        TestWrapperBravo Bravo = alfa.getBravo(i);
        TestWrapperDelta Delta = alfa.getDelta(i);

        if (i == expectedInput) {
          assertTrue(Bravo.hasSingleBi1Delta());
          assertTrue(Delta.hasSingleBack1Bravo());
          assertEquals(inputDelta, Bravo.getSingleBi1Delta());
          assertEquals(inputBravo, Delta.getSingleBack1Bravo());
          assertThat(Bravo.getMultiBi2DeltaList()).containsExactly(
              inputDelta, alfa.getDelta(expectedInput + 1));
        } else {
          assertFalse(Bravo.hasSingleBi1Delta());
          assertFalse(Delta.hasSingleBack1Bravo());
          assertThat(Bravo.getMultiBi2DeltaList()).isEmpty();
        }
        if (i == expectedInput || i == expectedInput + 1) {
          assertThat(Bravo.getMultiBi3DeltaList()).containsExactly(
              inputDelta, alfa.getDelta(expectedInput + 1));
          assertTrue(Delta.hasSingleBack2Bravo());
          assertEquals(inputBravo, Delta.getSingleBack2Bravo());
          assertThat(Delta.getMultiBack3BravoList()).containsExactly(
              inputBravo, alfa.getBravo(expectedInput + 1));
        } else {
          assertThat(Bravo.getMultiBi3DeltaList()).isEmpty();
          assertFalse(Delta.hasSingleBack2Bravo());
          assertThat(Delta.getMultiBack3BravoList()).isEmpty();
        }
      }
    }
  }

  protected static class ReceiverData {
    int numberOfTrees = 0;
  }

}
