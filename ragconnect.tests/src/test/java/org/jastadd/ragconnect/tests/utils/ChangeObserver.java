package org.jastadd.ragconnect.tests.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * TODO: Add description.
 *
 * @author rschoene - Initial contribution
 */
public class ChangeObserver {
  Map<Supplier<String>, String> callableToPrevious = new HashMap<>();
  private Callable<Boolean> hasChanged;

  @SafeVarargs
  public final void init(Supplier<String>... suppliers) {
    callableToPrevious.clear();
    Arrays.stream(suppliers).forEach(callable -> callableToPrevious.put(callable, callable.get()));
    hasChanged = () -> callableToPrevious.entrySet().stream()
            .anyMatch(entry -> !entry.getKey().get().equals(entry.getValue()));
  }

  public void start() {
    updatePrevious();
  }

  private void updatePrevious() {
    callableToPrevious.keySet().forEach(callable -> callableToPrevious.put(callable, callable.get()));
  }

  public void awaitChange() {
    TestUtils.awaitMqtt().until(hasChanged);
    updatePrevious();
  }

  public boolean hasChanged() {
    try {
      return hasChanged.call();
    } catch (Exception e) {
      fail(e);
      return false;
    }
  }
}
