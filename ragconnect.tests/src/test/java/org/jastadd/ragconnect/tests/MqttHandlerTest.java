package org.jastadd.ragconnect.tests;

import example.ast.MqttHandler;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Testing the {@link MqttHandler} used in the "example" test case.
 *
 * @author rschoene - Initial contribution
 */
@Tag("mqtt")
public class MqttHandlerTest extends RagConnectTest {

  @Test
  public void defaultBehaviour() {
    MqttHandler handler = new MqttHandler();
    try {
      handler.setHost(TestUtils.getMqttHost());
    } catch (IOException e) {
      fail("Fail during setHost", e);
    }
    boolean ready = handler.waitUntilReady(2, TimeUnit.SECONDS);
    assertTrue(ready);
    handler.close();
  }

  @Test
  public void testWelcomeMessage() throws Exception {
    MqttHandler welcomeMessageSubscriber = new MqttHandler();
    List<String> receivedMessages = new ArrayList<>();
    welcomeMessageSubscriber.setHost(TestUtils.getMqttHost());
    assertTrue(welcomeMessageSubscriber.waitUntilReady(2, TimeUnit.SECONDS));
    welcomeMessageSubscriber.newConnection("components", bytes -> receivedMessages.add(new String(bytes)));
    assertThat(receivedMessages).isEmpty();

    MqttHandler handler = new MqttHandler();
    handler.setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));
    TestUtils.waitForMqtt();

    assertEquals(1, receivedMessages.size());
  }

  @Test
  public void testDontSendWelcomeMessage() throws Exception {
    MqttHandler welcomeMessageSubscriber = new MqttHandler();
    List<String> receivedMessages = new ArrayList<>();
    welcomeMessageSubscriber.setHost(TestUtils.getMqttHost());
    assertTrue(welcomeMessageSubscriber.waitUntilReady(2, TimeUnit.SECONDS));
    welcomeMessageSubscriber.newConnection("components", bytes -> receivedMessages.add(new String(bytes)));
    assertThat(receivedMessages).isEmpty();

    MqttHandler handler = new MqttHandler().dontSendWelcomeMessage();
    handler.setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));
    TestUtils.waitForMqtt();

    assertThat(receivedMessages).isEmpty();
  }

}
