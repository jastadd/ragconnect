package org.jastadd.ragconnect.tests;

import com.google.protobuf.InvalidProtocolBufferException;
import config.Config.RobotConfig;
import example.ast.*;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.jastadd.ragconnect.tests.utils.TestChecker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import robot.RobotStateOuterClass.RobotState;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.jastadd.ragconnect.tests.utils.TestUtils.waitForMqtt;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "example".
 *
 * @author rschoene - Initial contribution
 */
public class ExampleTest extends AbstractMqttTest {

  private static final String TOPIC_CONFIG = "robot/config";
  private static final String TOPIC_JOINT1 = "robot/arm/joint1";
  private static final String TOPIC_JOINT2 = "robot/arm/joint2";

  private Model model;
  private RobotArm robotArm;
  private Link link1;
  private Link link2;
  private MqttHandler handler;
  private TestChecker checker;
  private ReceiverData data;

  @BeforeEach
  public void resetTestCounter() {
    TestCounter.reset();
  }

  @Override
  protected void createModel() {
    model = new Model();

    ZoneModel zoneModel = new ZoneModel();

    IntPosition firstPosition = makePosition(0, 0, 0);
    IntPosition secondPosition = makePosition(-1, 0, 0);
    IntPosition thirdPosition = makePosition(1, 0, 0);

    Zone safetyZone = new Zone();
    safetyZone.addCoordinate(new Coordinate(firstPosition));
    safetyZone.addCoordinate(new Coordinate(secondPosition));
    safetyZone.addCoordinate(new Coordinate(thirdPosition));
    zoneModel.addSafetyZone(safetyZone);
    model.setZoneModel(zoneModel);

    robotArm = new RobotArm();

    link1 = new Link();
    link1.setName("joint1");
    link1.setCurrentPosition(firstPosition);

    link2 = new Link();
    link2.setName("joint2");
    link2.setCurrentPosition(secondPosition);

    EndEffector endEffector = new EndEffector();
    endEffector.setName("gripper");
    endEffector.setCurrentPosition(makePosition(2, 2, 3));

    robotArm.addLink(link1);
    robotArm.addLink(link2);
    robotArm.setEndEffector(endEffector);
    model.setRobotArm(robotArm);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    // add dependencies
    robotArm.addDependency1(link1);
    robotArm.addDependency1(link2);
    robotArm.addDependency1(robotArm.getEndEffector());

    data = new ReceiverData();

    checker = new TestChecker();
    checker.setActualNumberOfValues(() -> data.numberOfConfigs)
            .setCheckForObject(TOPIC_JOINT1, (name, expected) -> assertEquals(expected, link1.getCurrentPosition(), name))
            .setCheckForObject(TOPIC_JOINT2, (name, expected) -> assertEquals(expected, link2.getCurrentPosition(), name))
    ;

    handler.newConnection(TOPIC_CONFIG, bytes -> {
      data.numberOfConfigs += 1;
      try {
        data.lastConfig = RobotConfig.parseFrom(bytes);
        data.failedLastConversion = false;
      } catch (InvalidProtocolBufferException e) {
        data.failedLastConversion = true;
      }
    });

    assertTrue(robotArm.connectAppropriateSpeed(mqttUri(TOPIC_CONFIG), isWriteCurrentValue()));
    assertTrue(link1.connectCurrentPosition(mqttUri(TOPIC_JOINT1)));
    assertTrue(link2.connectCurrentPosition(mqttUri(TOPIC_JOINT2)));
  }

  @Override
  protected void communicateSendInitialValue() throws InterruptedException {
    // joint is currently within the safety zone, so speed should be low
    checker.incNumberOfValues()
            .put(TOPIC_JOINT1, makePosition(0, 0, 0))
            .put(TOPIC_JOINT2, makePosition(-1, 0, 0));
    checkData(0, 0, 1, 1, 1, false);
    assertEquals(robotArm.speedLow(), data.lastConfig.getSpeed(), TestUtils.DELTA);

    communicateBoth(false);
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException {
// no value should have been sent
    checker.put(TOPIC_JOINT1, makePosition(0, 0, 0))
            .put(TOPIC_JOINT2, makePosition(-1, 0, 0));
    checkData(0, 0, 1, 1, 1, true);

    communicateBoth(true);
  }

  private void communicateBoth(boolean failFirstConversion) {
    // change position of the first joint out of the safety zone, second still in
    sendData(TOPIC_JOINT1, 0.2f, 0.2f, 0.2f);

    // still in safety zone, hence, no value should have been sent
    checker.put(TOPIC_JOINT1, makePosition(2, 2, 2));
    checkData(1, 1, 2, 2, 2, failFirstConversion);

    // change position of second joint also out of the safety zone, now speed must be high
    sendData(TOPIC_JOINT2, 0.3f, 0.4f, 0.5f);

    checker.incNumberOfValues();
    checker.put(TOPIC_JOINT2, makePosition(3, 4, 5));
    checkData(2, 2, 3, 3, 3, false);
    assertEquals(robotArm.speedHigh(), data.lastConfig.getSpeed(), TestUtils.DELTA);

    // change position of second joint, no change after mapping
    sendData(TOPIC_JOINT2, 0.33f, 0.42f, 0.51f);

    checker.put(TOPIC_JOINT2, makePosition(3, 4, 5));
    checkData(3, 3, 3, 3, 3, false);

    // change position of second joint, still out of the safety zone, no update should be sent
    sendData(TOPIC_JOINT2, 1.3f, 2.4f, 3.5f);

    checker.put(TOPIC_JOINT2, makePosition(13, 24, 35));
    checkData(4, 4, 4, 4, 4, false);
  }

  private void checkData(int expectedNumberParseLinkState,
                         int expectedNumberLinkStateToIntPosition,
                         int expectedNumberInSafetyZone,
                         int expectedNumberCreateSpeedMessage,
                         int expectedNumberSerializeRobotConfig,
                         boolean failedLastConversion) {
    checker.check();
    try {
      waitForMqtt();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    assertEquals(expectedNumberParseLinkState, TestCounter.INSTANCE.numberParseLinkState);
    assertEquals(expectedNumberLinkStateToIntPosition, TestCounter.INSTANCE.numberLinkStateToIntPosition);
    assertEquals(expectedNumberInSafetyZone, TestCounter.INSTANCE.numberInSafetyZone);
    assertEquals(expectedNumberCreateSpeedMessage, TestCounter.INSTANCE.numberCreateSpeedMessage);
    assertEquals(expectedNumberSerializeRobotConfig, TestCounter.INSTANCE.numberSerializeRobotConfig);
    assertEquals(failedLastConversion, data.failedLastConversion);
  }

  @Override
  public void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  @Test
  public void testFailedConversion() throws IOException {
    createModel();
    setupReceiverAndConnect();
    int numberOfPreviousConfigs = data.numberOfConfigs;

    publisher.publish(TOPIC_JOINT1, "not-a-pandaLinkState".getBytes());
    assertEquals(numberOfPreviousConfigs, data.numberOfConfigs);
    assertTrue(data.failedLastConversion);
  }

  private void sendData(String topic, float x, float y, float z) {
    publisher.publish(topic, RobotState.newBuilder()
        .setPosition(RobotState.Position.newBuilder().setX(x).setY(y).setZ(z).build())
        .build()
        .toByteArray()
    );
  }

  private static IntPosition makePosition(int x, int y, int z) {
    return IntPosition.of(x, y, z);
  }

  private static class ReceiverData {
    RobotConfig lastConfig;
    boolean failedLastConversion = true;
    int numberOfConfigs = 0;
  }
}
