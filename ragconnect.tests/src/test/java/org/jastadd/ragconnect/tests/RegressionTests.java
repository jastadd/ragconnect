package org.jastadd.ragconnect.tests;

import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Test;
import via.ast.A;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Regression tests for fixed issues.
 *
 * @author rschoene - Initial contribution
 */
public class RegressionTests extends RagConnectTest {

  private static final String REGRESSION_TEST_OUTPUT_DIRECTORY = "regression-test/";

  @Test
  public void issue22() {
    // use model of "via" test case as it uses both mqtt and rest as protocols
    A a = new A();
    try {
      // should fail because of missing scheme
      assertFalse(a.connectBoth2BothInput("missing/scheme"));

      // should fail because of missing host
      assertFalse(a.connectBoth2BothInput("mqtt://"));

      // should fail because of missing part
      assertFalse(a.connectBoth2BothInput("mqtt://localhost"));

      // should fail because of unknown scheme
      assertFalse(a.connectBoth2BothInput("badScheme://host/some/topic"));
    } catch (IOException e) {
      fail(e);
    }
  }

  @Test
  public void issue27() throws IOException {
    String grammarFile = "regression-tests/issue27/Test.relast";
    String connectFile = "regression-tests/issue27/Test.connect";
    grammarFile = ensureNoTrailingNewLine(grammarFile);
    connectFile = ensureNoTrailingNewLine(connectFile);
    TestUtils.runCompiler(grammarFile, Collections.singletonList(connectFile), "A", REGRESSION_TEST_OUTPUT_DIRECTORY, 0);
  }

  private String ensureNoTrailingNewLine(String inputFileSuffix) throws IOException {
    int dotIndex = inputFileSuffix.lastIndexOf('.');
    String outFileSuffix = inputFileSuffix.substring(0, dotIndex) + ".noNewLine" + inputFileSuffix.substring(dotIndex);
    Path inputPath = Paths.get(TestUtils.INPUT_DIRECTORY_PREFIX).resolve(inputFileSuffix);
    Path outputPath = Paths.get(TestUtils.INPUT_DIRECTORY_PREFIX).resolve(outFileSuffix);

    String content = Files.readString(inputPath);
    Files.writeString(outputPath, content.stripTrailing(), StandardOpenOption.CREATE);

    return outFileSuffix;
  }
}
