package org.jastadd.ragconnect.tests;

import incremental.ast.A;
import incremental.ast.B;
import incremental.ast.MqttHandler;
import org.jastadd.ragconnect.tests.utils.DefaultMappings;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Tag;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Testcase "Incremental Dependency".
 *
 * @author rschoene - Initial contribution
 */
@Tag("Incremental")
public class IncrementalDependencyTest extends AbstractMqttTest {

  private static final String TOPIC_IN = "in/a";
  private static final String TOPIC_OUT_A = "out/a";
  private static final String TOPIC_OUT_B1 = "out/b1";
  private static final String TOPIC_OUT_B2 = "out/b2";

  private MqttHandler handler;
  private A model;
  private B b1;
  private B b2;

  private ReceiverData dataA;
  private ReceiverData dataB1;
  private ReceiverData dataB2;

  @Override
  protected void createModel() {
    model = new A();
    model.setInput("Start");
    b1 = new B();
    b2 = new B();
    model.addB(b1);
    model.addB(b2);
    model.ragconnectCheckIncremental();
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler("TestHandler")
        .dontSendWelcomeMessage()
        .setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    // no dependencies for the model are set here

    dataA = new ReceiverData();
    dataB1 = new ReceiverData();
    dataB2 = new ReceiverData();

    handler.newConnection(TOPIC_OUT_A, bytes -> {
      dataA.numberOfStringValues += 1;
      dataA.lastStringValue = DefaultMappings.BytesToString(bytes);
    });
    handler.newConnection(TOPIC_OUT_B1, bytes -> {
      dataB1.numberOfStringValues += 1;
      dataB1.lastStringValue = DefaultMappings.BytesToString(bytes);
    });
    handler.newConnection(TOPIC_OUT_B2, bytes -> {
      dataB2.numberOfStringValues += 1;
      dataB2.lastStringValue = DefaultMappings.BytesToString(bytes);
    });

    assertTrue(model.connectInput(mqttUri(TOPIC_IN)));
    assertTrue(model.connectOutputOnA(mqttUri(TOPIC_OUT_A), isWriteCurrentValue()));
    assertTrue(b1.connectOutputOnB(mqttUri(TOPIC_OUT_B1), isWriteCurrentValue()));
    assertTrue(b2.connectOutputOnB(mqttUri(TOPIC_OUT_B2), isWriteCurrentValue()));
  }

  @Override
  protected void communicateSendInitialValue() throws InterruptedException, IOException {
    // check initial value
    TestUtils.waitForMqtt();
    checkData(1, "aStart",
        "bStartPostfix",
        "bStartPostfix");

    communicateBoth(1);
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException, IOException {
    // check initial value
    TestUtils.waitForMqtt();
    checkData(0, null,
        null,
        null);

    communicateBoth(0);
  }

  private void communicateBoth(int initialNumberOfValues) throws InterruptedException, IOException {
    // send and check new value
    sendData("102");
    checkData(initialNumberOfValues + 1, "a102",
        "b102Postfix",
        "b102Postfix");

    // send and check same value
    sendData("102");
    checkData(initialNumberOfValues + 1, "a102",
        "b102Postfix",
        "b102Postfix");

    // send and check new value
    sendData("202");
    checkData(initialNumberOfValues + 2, "a202",
        "b202Postfix",
        "b202Postfix");

    // send and check new value (b1 should not change)
    assertTrue(b1.disconnectOutputOnB(mqttUri(TOPIC_OUT_B1)));
    sendData("302");
    checkData(initialNumberOfValues + 3, "a302",
            initialNumberOfValues + 2, "b202Postfix",
            initialNumberOfValues + 3, "b302Postfix");

    // disconnecting again should yield false
    assertFalse(b1.disconnectOutputOnB(mqttUri(TOPIC_OUT_B1)));

    // send and check new value (b1 and b2 should not change)
    assertTrue(b2.disconnectOutputOnB(mqttUri(TOPIC_OUT_B2)));
    sendData("402");
    checkData(initialNumberOfValues + 4, "a402",
            initialNumberOfValues + 2, "b202Postfix",
            initialNumberOfValues + 3, "b302Postfix");

    // send and check new value (nothing should not change)
    assertTrue(model.disconnectOutputOnA(mqttUri(TOPIC_OUT_A)));
    sendData("502");
    checkData(initialNumberOfValues + 4, "a402",
            initialNumberOfValues + 2, "b202Postfix",
            initialNumberOfValues + 3, "b302Postfix");
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  private void sendData(String input) throws InterruptedException {
    publisher.publish(TOPIC_IN, input.getBytes());
    TestUtils.waitForMqtt();
  }

  private void checkData(int expectedNumberOfValues, String expectedLastAValue,
                         String expectedLastB1Value, String expectedLastB2Value) {
    checkData(expectedNumberOfValues, expectedLastAValue,
        expectedNumberOfValues, expectedLastB1Value,
        expectedNumberOfValues, expectedLastB2Value);
  }

  private void checkData(int expectedNumberOfAValues, String expectedLastAValue,
                         int expectedNumberOfB1Values, String expectedLastB1Value,
                         int expectedNumberOfB2Values, String expectedLastB2Value) {
    dataA.assertEqualData(expectedNumberOfAValues, expectedLastAValue);
    dataB1.assertEqualData(expectedNumberOfB1Values, expectedLastB1Value);
    dataB2.assertEqualData(expectedNumberOfB2Values, expectedLastB2Value);
  }

  private static class ReceiverData {
    String lastStringValue;
    int numberOfStringValues = 0;

    public void assertEqualData(int expectedNumberOfValues, String expectedLastValue) {
      assertEquals(expectedNumberOfValues, this.numberOfStringValues);
      assertEquals(expectedLastValue, this.lastStringValue);
    }
  }
}
