package org.jastadd.ragconnect.tests.tokenValue;

import tokenValueSend.ast.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "tokenValueSend manual".
 *
 * @author rschoene - Initial contribution
 */
public class TokenValueSendManualTest extends AbstractTokenValueSendTest {

  private A model;

  TokenValueSendManualTest() {
    super("tokenValueSend");
  }

  @Override
  protected void createModel() {
    // Setting value for Input without dependencies does not trigger any updates
    model = new A();

    model.setOnlySend(new OnlySend().setValue(INITIAL_VALUE));
    model.setReceiveAndSend(new ReceiveAndSend().setValue(INITIAL_VALUE));
    model.setReceiveSendAndDepend(new ReceiveSendAndDepend().setValue(INITIAL_VALUE));
  }


  @Override
  public void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  @Override
  protected void connectModel() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    model.getReceiveSendAndDepend().addDependency1(model.getReceiveSendAndDepend());

    assertTrue(model.getOnlySend().connectValue(mqttUri(TOPIC_SEND_ONE), isWriteCurrentValue()));
    assertTrue(model.getReceiveAndSend().connectValue(mqttUri(TOPIC_RECEIVE_TWO)));
    assertTrue(model.getReceiveAndSend().connectValue(mqttUri(TOPIC_SEND_TWO), isWriteCurrentValue()));
    assertTrue(model.getReceiveSendAndDepend().connectValue(mqttUri(TOPIC_RECEIVE_THREE_VALUE)));
    assertTrue(model.getReceiveSendAndDepend().connectValue(mqttUri(TOPIC_SEND_THREE_VALUE), isWriteCurrentValue()));
    assertTrue(model.getReceiveSendAndDepend().connectOtherOutput(mqttUri(TOPIC_SEND_THREE_OTHER), isWriteCurrentValue()));
  }

  @Override
  protected boolean disconnect(String objectIdentifier, String targetIdentifier, String topic) throws IOException {
    switch (objectIdentifier) {
      case "two":
        ReceiveAndSend receiveAndSend = model.getReceiveAndSend();
        switch (targetIdentifier) {
          case "SendValue": return receiveAndSend.disconnectSendValue(topic);
          case "ReceiveValue": return receiveAndSend.disconnectReceiveValue(topic);
        }
        break;
      case "three":
        ReceiveSendAndDepend receiveSendAndDepend = model.getReceiveSendAndDepend();
        switch (targetIdentifier) {
          case "SendValue": return receiveSendAndDepend.disconnectSendValue(topic);
          case "ReceiveValue": return receiveSendAndDepend.disconnectReceiveValue(topic);
        }
        break;
    }
    throw new IllegalArgumentException(objectIdentifier + " on " + targetIdentifier + " for " + topic);
  }

  @Override
  protected String valueOfReceiveAndSend() {
    return model.getReceiveAndSend().getValue();
  }

  @Override
  protected void setData(String inputOne, String inputTwo, String inputThree) {
    model.getOnlySend().setValue(inputOne);
    model.getReceiveAndSend().setValue(inputTwo);
    model.getReceiveSendAndDepend().setValue(inputThree);
  }
}
