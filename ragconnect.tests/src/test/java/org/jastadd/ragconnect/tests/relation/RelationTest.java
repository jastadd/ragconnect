package org.jastadd.ragconnect.tests.relation;

import org.jastadd.ragconnect.tests.AbstractMqttTest;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Tag;
import relationInc.ast.*;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;

import org.jastadd.ragconnect.tests.utils.TestChecker;
import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test case "relation".
 *
 * @author rschoene - Initial contribution
 */
@Tag("Incremental")
public class RelationTest extends AbstractMqttTest {

  private static final String TOPIC_WILDCARD = "rel/#";
  private static final String TOPIC_MY_A = "rel/my_a";
  private static final String TOPIC_OPTIONAL_A = "rel/optional_a";
  private static final String TOPIC_MANY_A = "rel/many_a";
  private static final String TOPIC_BI_MY_A = "rel/bi_my_a";
  private static final String TOPIC_BI_OPTIONAL_A = "rel/bi_optional_a";
  private static final String TOPIC_BI_MANY_A = "rel/bi_many_a";
  private static final String TOPIC_MY_B = "rel/my_b";
  private static final String TOPIC_OPTIONAL_B = "rel/optional_b";
  private static final String TOPIC_MANY_B = "rel/many_b";
  private static final String TOPIC_BI_MY_B = "rel/bi_my_b";
  private static final String TOPIC_BI_OPTIONAL_B = "rel/bi_optional_b";
  private static final String TOPIC_BI_MANY_B = "rel/bi_many_b";

  private MqttHandler handler;
  private ReceiverData data;
  private TestChecker checker;

  private Root model;
  private SenderRoot senderUni;
  private SenderRoot senderBi;
  private ReceiverRoot receiverRoot;

  @Override
  protected void createModel() {
    model = new Root();
//    model.trace().setReceiver(TestUtils::logEvent);
    senderUni = createSenderRoot("uni");
    senderUni.setMyA(uniA(1));
    senderUni.setMyB(uniB(1));

    senderBi = createSenderRoot("bi");
    senderBi.setBiMyA(biA(1));
    senderBi.setBiMyB(biB(1));

    receiverRoot = new ReceiverRoot();
    model.addSenderRoot(senderUni);
    model.addSenderRoot(senderBi);
    model.setReceiverRoot(receiverRoot);
  }

  private SenderRoot createSenderRoot(String name) {
    SenderRoot result = new SenderRoot();
    A dummyA = createA(name + "-dummyA");
    result.addA(dummyA);
    result.addA(createA(name + "-a1"));
    result.addA(createA(name + "-a2"));
    result.addA(createA(name + "-a3"));

    B dummyB = createB(name + "-dummyB");
    result.addB(dummyB);
    result.addB(createB(name + "-b1"));
    result.addB(createB(name + "-b2"));
    result.addB(createB(name + "-b3"));

    result.setMyA(dummyA);
    result.setBiMyA(dummyA);
    result.setMyB(dummyB);
    result.setBiMyB(dummyB);

    return result;
  }

  private A createA(String value) {
    return new A().setValue(value)
            .setInner(new Inner().setInnerValue("inner-" + value));
  }

  private B createB(String value) {
    return new B().setValue(value)
            .setInner(new Inner().setInnerValue("inner-" + value));
  }

  private A uniA(int index) {
    return senderUni.getA(index);
  }

  private B uniB(int index) {
    return senderUni.getB(index);
  }

  private A biA(int index) {
    return senderBi.getA(index);
  }

  private B biB(int index) {
    return senderBi.getB(index);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException, InterruptedException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);
    handler = new MqttHandler().setHost(TestUtils.getMqttHost()).dontSendWelcomeMessage();
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    data = new ReceiverData();
    assertTrue(handler.newConnection(TOPIC_WILDCARD, bytes -> data.numberOfValues += 1));

    checker = new TestChecker();
    checker.setActualNumberOfValues(() -> data.numberOfValues)
            .setCheckForString(TOPIC_MY_A,
                    (name, expected) -> assertNullOrA(expected, receiverRoot.getFromMyA(), name))
            .setCheckForString(TOPIC_OPTIONAL_A,
                    (name, expected) -> assertNullOrA(expected, receiverRoot.getFromOptionalA(), name))
            .setCheckForTuple(TOPIC_MANY_A,
                    (name, expected) -> assertListEqualsForA(expected.toList(), receiverRoot.getFromManyAList(), name))
            .setCheckForString(TOPIC_BI_MY_A,
                    (name, expected) -> assertNullOrA(expected, receiverRoot.getFromBiMyA(), name))
            .setCheckForString(TOPIC_BI_OPTIONAL_A,
                    (name, expected) -> assertNullOrA(expected, receiverRoot.getFromBiOptionalA(), name))
            .setCheckForTuple(TOPIC_BI_MANY_A,
                    (name, expected) -> assertListEqualsForA(expected.toList(), receiverRoot.getFromBiManyAList(), name))
            .setCheckForString(TOPIC_MY_B,
                    (name, expected) -> assertNullOrB(expected, receiverRoot.getFromMyB(), name))
            .setCheckForString(TOPIC_OPTIONAL_B,
                    (name, expected) -> assertNullOrB(expected, receiverRoot.getFromOptionalB(), name))
            .setCheckForTuple(TOPIC_MANY_B,
                    (name, expected) -> assertListEqualsForB(expected.toList(), receiverRoot.getFromManyB(), name))
            .setCheckForString(TOPIC_BI_MY_B,
                    (name, expected) -> assertNullOrB(expected, receiverRoot.getFromBiMyB(), name))
            .setCheckForString(TOPIC_BI_OPTIONAL_B,
                    (name, expected) -> assertNullOrB(expected, receiverRoot.getFromBiOptionalB(), name))
            .setCheckForTuple(TOPIC_BI_MANY_B,
                    (name, expected) -> assertListEqualsForB(expected.toList(), receiverRoot.getFromBiManyB(), name));

    // connect receive
    assertTrue(receiverRoot.connectFromMyA(mqttUri(TOPIC_MY_A)));
    assertTrue(receiverRoot.connectFromOptionalA(mqttUri(TOPIC_OPTIONAL_A)));
    assertTrue(receiverRoot.connectFromManyAList(mqttUri(TOPIC_MANY_A)));
    assertTrue(receiverRoot.connectFromBiMyA(mqttUri(TOPIC_BI_MY_A)));
    assertTrue(receiverRoot.connectFromBiOptionalA(mqttUri(TOPIC_BI_OPTIONAL_A)));
    assertTrue(receiverRoot.connectFromBiManyAList(mqttUri(TOPIC_BI_MANY_A)));
    assertTrue(receiverRoot.connectFromMyB(mqttUri(TOPIC_MY_B)));
    assertTrue(receiverRoot.connectFromOptionalB(mqttUri(TOPIC_OPTIONAL_B)));
    assertTrue(receiverRoot.connectFromManyB(mqttUri(TOPIC_MANY_B)));
    assertTrue(receiverRoot.connectFromBiMyB(mqttUri(TOPIC_BI_MY_B)));
    assertTrue(receiverRoot.connectFromBiOptionalB(mqttUri(TOPIC_BI_OPTIONAL_B)));
    assertTrue(receiverRoot.connectFromBiManyB(mqttUri(TOPIC_BI_MANY_B)));

    // connect send, and wait to receive (if writeCurrentValue is set)
    assertTrue(senderUni.connectMyA(mqttUri(TOPIC_MY_A), isWriteCurrentValue()));
    assertTrue(senderUni.connectOptionalA(mqttUri(TOPIC_OPTIONAL_A), isWriteCurrentValue()));
    assertTrue(senderUni.connectManyA(mqttUri(TOPIC_MANY_A), isWriteCurrentValue()));

    assertTrue(senderBi.connectBiMyA(mqttUri(TOPIC_BI_MY_A), isWriteCurrentValue()));
    assertTrue(senderBi.connectBiOptionalA(mqttUri(TOPIC_BI_OPTIONAL_A), isWriteCurrentValue()));
    assertTrue(senderBi.connectBiManyA(mqttUri(TOPIC_BI_MANY_A), isWriteCurrentValue()));

    assertTrue(senderUni.connectMyB(mqttUri(TOPIC_MY_B), isWriteCurrentValue()));
    assertTrue(senderUni.connectOptionalB(mqttUri(TOPIC_OPTIONAL_B), isWriteCurrentValue()));
    assertTrue(senderUni.connectManyB(mqttUri(TOPIC_MANY_B), isWriteCurrentValue()));

    assertTrue(senderBi.connectBiMyB(mqttUri(TOPIC_BI_MY_B), isWriteCurrentValue()));
    assertTrue(senderBi.connectBiOptionalB(mqttUri(TOPIC_BI_OPTIONAL_B), isWriteCurrentValue()));
    assertTrue(senderBi.connectBiManyB(mqttUri(TOPIC_BI_MANY_B), isWriteCurrentValue()));
  }

  @Override
  protected void communicateSendInitialValue() throws IOException {
    checker.addToNumberOfValues(8)
            .put(TOPIC_MY_A, "uni-a1")
            .put(TOPIC_OPTIONAL_A, (String) null)
            .put(TOPIC_MANY_A, tuple())
            .put(TOPIC_BI_MY_A, "bi-a1")
            .put(TOPIC_BI_OPTIONAL_A, (String) null)
            .put(TOPIC_BI_MANY_A, tuple())
            .put(TOPIC_MY_B, "uni-b1")
            .put(TOPIC_OPTIONAL_B, (String) null)
            .put(TOPIC_MANY_B, tuple())
            .put(TOPIC_BI_MY_B, "bi-b1")
            .put(TOPIC_BI_OPTIONAL_B, (String) null)
            .put(TOPIC_BI_MANY_B, tuple());

    communicateBoth();
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws IOException, InterruptedException {
    checker.put(TOPIC_MY_A, (String) null)
            .put(TOPIC_OPTIONAL_A, (String) null)
            .put(TOPIC_MANY_A, tuple())
            .put(TOPIC_BI_MY_A, (String) null)
            .put(TOPIC_BI_OPTIONAL_A, (String) null)
            .put(TOPIC_BI_MANY_A, tuple())
            .put(TOPIC_MY_B, (String) null)
            .put(TOPIC_OPTIONAL_B, (String) null)
            .put(TOPIC_MANY_B, tuple())
            .put(TOPIC_BI_MY_B, (String) null)
            .put(TOPIC_BI_OPTIONAL_B, (String) null)
            .put(TOPIC_BI_MANY_B, tuple());

    communicateBoth();
  }

  protected void communicateBoth() throws IOException {
    checker.check();

    // myA -> uni-a1, myB -> uni-b1
    // --- testing unmapped unidirectional normal role --- //

    uniA(1).setValue("test-1");
    checker.incNumberOfValues().put(TOPIC_MY_A, "test-1:inner-uni-a1").check();

    senderUni.setMyA(uniA(2));
    checker.incNumberOfValues().put(TOPIC_MY_A, "uni-a2").check();

    // changing something that was previously the relation target must not trigger a message
    uniA(1).setValue("test-2-ignored");
    checker.check();

    uniA(2).setValue("test-3");
    checker.incNumberOfValues().put(TOPIC_MY_A, "test-3:inner-uni-a2").check();

    uniA(2).getInner().setInnerValue("test-4");
    checker.incNumberOfValues().put(TOPIC_MY_A, "test-3:test-4").check();

    // setting a new relation target resulting in the same serialization must not trigger a message
    uniA(1).setValue("test-3");
    uniA(1).getInner().setInnerValue("test-4");
    senderUni.setMyA(uniA(1));
    checker.check();

    // --- testing unmapped unidirectional optional role --- //

    // reset a2
    uniA(2).setValue("uni-a2");
    uniA(2).getInner().setInnerValue("inner-uni-a2");

    senderUni.setOptionalA(uniA(2));
    checker.incNumberOfValues().put(TOPIC_OPTIONAL_A, "uni-a2").check();

    uniA(2).setValue("test-5");
    checker.incNumberOfValues().put(TOPIC_OPTIONAL_A, "test-5:inner-uni-a2").check();

    senderUni.setOptionalA(uniA(1));
    checker.incNumberOfValues().put(TOPIC_OPTIONAL_A, "test-3:test-4").check();

    // change a nonterminal target of two relations must trigger two messages
    uniA(1).getInner().setInnerValue("test-6");
    checker.addToNumberOfValues(2)
            .put(TOPIC_MY_A, "test-3:test-6")
            .put(TOPIC_OPTIONAL_A, "test-3:test-6")
            .check();

    // setting an optional relation to null is allowed, but must not trigger a message
    senderUni.setOptionalA(null);
    checker.check();

    // setting the previous nonterminal as relation target again won't trigger a message
    senderUni.setOptionalA(uniA(1));
    checker.check();

    // --- testing unmapped unidirectional list role --- //

    senderUni.addManyA(uniA(3));
    checker.incNumberOfValues().put(TOPIC_MANY_A, tuple("uni-a3")).check();

    uniA(3).setValue("test-7");
    checker.incNumberOfValues().put(TOPIC_MANY_A, tuple("test-7:inner-uni-a3")).check();

    senderUni.addManyA(uniA(2));
    checker.incNumberOfValues().put(TOPIC_MANY_A, tuple("test-7:inner-uni-a3", "test-5:inner-uni-a2")).check();

    senderUni.addManyA(uniA(1));
    checker.incNumberOfValues().put(TOPIC_MANY_A, tuple("test-7:inner-uni-a3", "test-5:inner-uni-a2", "test-3:test-6")).check();

    uniA(2).getInner().setInnerValue("test-8");
    checker.incNumberOfValues().put(TOPIC_MANY_A, tuple("test-7:inner-uni-a3", "test-5:test-8", "test-3:test-6")).check();

    senderUni.removeManyA(uniA(2));
    checker.incNumberOfValues().put(TOPIC_MANY_A, tuple("test-7:inner-uni-a3", "test-3:test-6")).check();

    // disconnect my-a, optional-a, many-a - resetting afterwards must not trigger a message
    senderUni.disconnectMyA(mqttUri(TOPIC_MY_A));
    senderUni.disconnectOptionalA(mqttUri(TOPIC_OPTIONAL_A));
    senderUni.disconnectManyA(mqttUri(TOPIC_MANY_A));
    uniA(1).setValue("a1");
    uniA(1).getInner().setInnerValue("inner-a1");
    uniA(2).setValue("a2");
    uniA(2).getInner().setInnerValue("inner-a2");
    uniA(3).setValue("a3");
    uniA(3).getInner().setInnerValue("inner-a3");
    checker.check();

    // "reset" values in receiver-root to make check method call shorted
    receiverRoot.setFromMyA(createA("uni-a1"));
    receiverRoot.setFromOptionalA(null);
    receiverRoot.setFromManyAList(new JastAddList<>());
    checker.put(TOPIC_MY_A, "uni-a1")
            .put(TOPIC_OPTIONAL_A, (String) null)
            .put(TOPIC_MANY_A, tuple());
    checker.check();

    // biMyA -> bi-a1, biMyB -> bi-b1
    // --- testing unmapped bidirectional normal role --- //
    biA(1).setValue("test-9");
    checker.incNumberOfValues().put(TOPIC_BI_MY_A, "test-9:inner-bi-a1").check();

    // set opposite role of relation must trigger message
    biA(2).setToMyA(senderBi);
    checker.incNumberOfValues().put(TOPIC_BI_MY_A, "bi-a2").check();

    // changing something that was previously the relation target must not trigger a message
    biA(1).setValue("test-9-ignored");
    checker.check();

    biA(2).setValue("test-10");
    checker.incNumberOfValues().put(TOPIC_BI_MY_A, "test-10:inner-bi-a2").check();

    biA(2).getInner().setInnerValue("test-11");
    checker.incNumberOfValues().put(TOPIC_BI_MY_A, "test-10:test-11").check();

    // setting a new relation target resulting in the same serialization must not trigger a message
    biA(1).setValue("test-10");
    biA(1).getInner().setInnerValue("test-11");
    biA(1).setToMyA(senderBi);
    checker.check();

    // --- testing unmapped bidirectional optional role --- //
    // reset a2
    biA(2).setValue("bi-a2");
    biA(2).getInner().setInnerValue("inner-bi-a2");

    senderBi.setBiOptionalA(biA(2));
    checker.incNumberOfValues().put(TOPIC_BI_OPTIONAL_A, "bi-a2").check();

    biA(2).setValue("test-12");
    checker.incNumberOfValues().put(TOPIC_BI_OPTIONAL_A, "test-12:inner-bi-a2").check();

    // set opposite role of relation must trigger message
    biA(1).setToOptionalA(senderBi);
    checker.incNumberOfValues().put(TOPIC_BI_OPTIONAL_A, "test-10:test-11").check();

    // change a nonterminal target of two relations must trigger two messages
    biA(1).getInner().setInnerValue("test-13");
    checker.addToNumberOfValues(2)
            .put(TOPIC_BI_MY_A, "test-10:test-13")
            .put(TOPIC_BI_OPTIONAL_A, "test-10:test-13").check();

    // setting an optional relation to null is allowed, but must not trigger a message
    senderBi.setBiOptionalA(null);
    checker.check();

    // setting the previous nonterminal as relation target again won't trigger a message
    biA(1).setToOptionalA(senderBi);
    checker.check();

    // --- testing unmapped bidirectional list role --- //
    biA(3).setToManyA(senderBi);
    checker.incNumberOfValues().put(TOPIC_BI_MANY_A, tuple("bi-a3")).check();

    biA(3).setValue("test-14");
    checker.incNumberOfValues().put(TOPIC_BI_MANY_A, tuple("test-14:inner-bi-a3")).check();

    senderBi.addBiManyA(biA(2));
    checker.incNumberOfValues().put(TOPIC_BI_MANY_A, tuple("test-14:inner-bi-a3", "test-12:inner-bi-a2")).check();

    biA(1).setToManyA(senderBi);
    checker.incNumberOfValues().put(TOPIC_BI_MANY_A, tuple("test-14:inner-bi-a3", "test-12:inner-bi-a2", "test-10:test-13")).check();

    biA(2).getInner().setInnerValue("test-15");
    // currently, an additional message is sent at bi_optional_a for biA1 as the serialization includes relations from A to SenderRoot and the relation to ToManyA was added for biA1.
    // this appears to be a bug in either jastadd or ragconnect
    // numberOfValues should actually be only 37 here.
    checker.addToNumberOfValues(2).put(TOPIC_BI_MANY_A, tuple("test-14:inner-bi-a3", "test-12:test-15", "test-10:test-13")).check();

    senderBi.removeBiManyA(biA(2));
    // the bug from above does not occur here, although the situation is similar
    // so, only one message is sent
    checker.incNumberOfValues().put(TOPIC_BI_MANY_A, tuple("test-14:inner-bi-a3", "test-10:test-13")).check();

    biA(3).setToManyA(null);
    checker.incNumberOfValues().put(TOPIC_BI_MANY_A, tuple("test-10:test-13")).check();

    // change a nonterminal target of three relations must trigger three messages
    biA(1).setValue("test-16");
    checker.addToNumberOfValues(3)
            .put(TOPIC_BI_MY_A, "test-16:test-13")
            .put(TOPIC_BI_OPTIONAL_A, "test-16:test-13")
            .put(TOPIC_BI_MANY_A, tuple("test-16:test-13"))
            .check();

    // disconnect bi-my-a, bi-optional-a, bi-many-a - resetting afterwards must not trigger a message
    senderBi.disconnectBiMyA(mqttUri(TOPIC_BI_MY_A));
    senderBi.disconnectBiOptionalA(mqttUri(TOPIC_BI_OPTIONAL_A));
    senderBi.disconnectBiManyA(mqttUri(TOPIC_BI_MANY_A));
    biA(1).setValue("bi-a1");
    biA(1).getInner().setInnerValue("inner-bi-a1");
    biA(2).setValue("bi-a2");
    biA(2).getInner().setInnerValue("inner-bi-a2");
    biA(3).setValue("bi-a3");
    biA(3).getInner().setInnerValue("inner-bi-a3");
    checker.check();

    // "reset" values in receiver-root to make check method call shorted
    receiverRoot.setFromBiMyA(createA("bi-a1"));
    receiverRoot.setFromBiOptionalA(null);
    receiverRoot.setFromBiManyAList(new JastAddList<>());
    checker.put(TOPIC_BI_MY_A, "bi-a1")
            .put(TOPIC_BI_OPTIONAL_A, (String) null)
            .put(TOPIC_BI_MANY_A, tuple())
            .check();

    // --- testing transformed unidirectional normal role --- //
    uniB(1).setValue("test-17");
    checker.incNumberOfValues().put(TOPIC_MY_B, "test-17:inner-uni-b1").check();

    senderUni.setMyB(uniB(2));
    checker.incNumberOfValues().put(TOPIC_MY_B, "uni-b2").check();

    // changing something that was previously the relation target must not trigger a message
    uniB(1).setValue("test-17-ignored");
    checker.check();

    uniB(2).setValue("test-18");
    checker.incNumberOfValues().put(TOPIC_MY_B, "test-18:inner-uni-b2").check();

    uniB(2).getInner().setInnerValue("test-19");
    checker.incNumberOfValues().put(TOPIC_MY_B, "test-18:test-19").check();

    // setting a new relation target resulting in the same serialization must not trigger a message
    uniB(1).setValue("test-18");
    uniB(1).getInner().setInnerValue("test-19");
    senderUni.setMyB(uniB(1));
    checker.check();

    // --- testing transformed unidirectional optional role --- //

    // reset a2
    uniB(2).setValue("uni-b2");
    uniB(2).getInner().setInnerValue("inner-uni-b2");

    senderUni.setOptionalB(uniB(2));
    checker.incNumberOfValues().put(TOPIC_OPTIONAL_B, "uni-b2").check();

    uniB(2).setValue("test-20");
    checker.incNumberOfValues().put(TOPIC_OPTIONAL_B, "test-20:inner-uni-b2").check();

    senderUni.setOptionalB(uniB(1));
    checker.incNumberOfValues().put(TOPIC_OPTIONAL_B, "test-18:test-19").check();

    // change a nonterminal target of two relations must trigger two messages
    uniB(1).getInner().setInnerValue("test-21");
    checker.addToNumberOfValues(2)
            .put(TOPIC_MY_B, "test-18:test-21")
            .put(TOPIC_OPTIONAL_B, "test-18:test-21")
            .check();

    // setting an optional relation to null is allowed, but must not trigger a message
    senderUni.setOptionalB(null);
    checker.check();

    // setting the previous nonterminal as relation target again won't trigger a message
    senderUni.setOptionalB(uniB(1));
    checker.check();

    // --- testing transformed unidirectional list role --- //
    senderUni.addManyB(uniB(3));
    checker.incNumberOfValues().put(TOPIC_MANY_B, tuple("uni-b3")).check();

    uniB(3).setValue("test-22");
    checker.incNumberOfValues().put(TOPIC_MANY_B, tuple("test-22:inner-uni-b3")).check();

    senderUni.addManyB(uniB(2));
    checker.incNumberOfValues().put(TOPIC_MANY_B, tuple("test-22:inner-uni-b3", "test-20:inner-uni-b2")).check();

    senderUni.addManyB(uniB(1));
    checker.incNumberOfValues().put(TOPIC_MANY_B, tuple("test-22:inner-uni-b3", "test-20:inner-uni-b2", "test-18:test-21")).check();

    uniB(2).getInner().setInnerValue("test-23");
    checker.incNumberOfValues().put(TOPIC_MANY_B, tuple("test-22:inner-uni-b3", "test-20:test-23", "test-18:test-21")).check();

    senderUni.removeManyB(uniB(2));
    checker.incNumberOfValues().put(TOPIC_MANY_B, tuple("test-22:inner-uni-b3", "test-18:test-21")).check();

    // disconnect my-b, optional-b, many-b - resetting afterwards must not trigger a message
    senderUni.disconnectMyB(mqttUri(TOPIC_MY_B));
    senderUni.disconnectOptionalB(mqttUri(TOPIC_OPTIONAL_B));
    senderUni.disconnectManyB(mqttUri(TOPIC_MANY_B));
    uniB(1).setValue("b1");
    uniB(1).getInner().setInnerValue("inner-b1");
    uniB(2).setValue("b2");
    uniB(2).getInner().setInnerValue("inner-b2");
    uniB(3).setValue("b3");
    uniB(3).getInner().setInnerValue("inner-b3");

    // "reset" values in receiver-root to make check method call shorted
    receiverRoot.setFromMyB("uni-b1+inner-uni-b1");
    receiverRoot.setFromOptionalB(null);
    receiverRoot.setFromManyB(null);
    checker.put(TOPIC_MY_B, "uni-b1")
            .put(TOPIC_OPTIONAL_B, (String) null)
            .put(TOPIC_MANY_B, tuple())
            .check();

    // --- testing transformed bidirectional normal role --- //
    biB(1).setValue("test-24");
    checker.incNumberOfValues().put(TOPIC_BI_MY_B, "test-24:inner-bi-b1").check();

    // set opposite role of relation must trigger message
    biB(2).setToMyB(senderBi);
    checker.incNumberOfValues().put(TOPIC_BI_MY_B, "bi-b2").check();

    // changing something that was previously the relation target must not trigger a message
    biB(1).setValue("test-24-ignored");
    checker.check();

    biB(2).setValue("test-25");
    checker.incNumberOfValues().put(TOPIC_BI_MY_B, "test-25:inner-bi-b2").check();

    biB(2).getInner().setInnerValue("test-26");
    checker.incNumberOfValues().put(TOPIC_BI_MY_B, "test-25:test-26").check();

    // setting a new relation target resulting in the same serialization must not trigger a message
    biB(1).setValue("test-25");
    biB(1).getInner().setInnerValue("test-26");
    biB(1).setToMyB(senderBi);
    checker.check();

    // --- testing transformed bidirectional optional role --- //
    // reset b2
    biB(2).setValue("bi-b2");
    biB(2).getInner().setInnerValue("inner-bi-b2");

    senderBi.setBiOptionalB(biB(2));
    checker.incNumberOfValues().put(TOPIC_BI_OPTIONAL_B, "bi-b2").check();

    biB(2).setValue("test-27");
    checker.incNumberOfValues().put(TOPIC_BI_OPTIONAL_B, "test-27:inner-bi-b2").check();

    // set opposite role of relation must trigger message
    biB(1).setToOptionalB(senderBi);
    checker.incNumberOfValues().put(TOPIC_BI_OPTIONAL_B, "test-25:test-26").check();

    // change a nonterminal target of two relations must trigger two messages
    biB(1).getInner().setInnerValue("test-28");
    checker.addToNumberOfValues(2)
            .put(TOPIC_BI_MY_B, "test-25:test-28")
            .put(TOPIC_BI_OPTIONAL_B, "test-25:test-28")
            .check();

    // setting an optional relation to null is allowed, but must not trigger a message
    senderBi.setBiOptionalB(null);
    checker.check();

    // setting the previous nonterminal as relation target again won't trigger a message
    biB(1).setToOptionalB(senderBi);
    checker.check();

    // --- testing transformed bidirectional list role --- //
    biB(3).setToManyB(senderBi);
    checker.incNumberOfValues().put(TOPIC_BI_MANY_B, tuple("bi-b3")).check();

    biB(3).setValue("test-29");
    checker.incNumberOfValues().put(TOPIC_BI_MANY_B, tuple("test-29:inner-bi-b3")).check();

    senderBi.addBiManyB(biB(2));
    checker.incNumberOfValues().put(TOPIC_BI_MANY_B, tuple("test-29:inner-bi-b3", "test-27:inner-bi-b2")).check();

    biB(1).setToManyB(senderBi);
    checker.incNumberOfValues().put(TOPIC_BI_MANY_B, tuple("test-29:inner-bi-b3", "test-27:inner-bi-b2", "test-25:test-28")).check();

    biB(2).getInner().setInnerValue("test-30");
    // the bug appearing in the unmapped bidirectional list case does not appear here, because here only a string representation of value + inner value is sent. so changes to relations do not trigger a message
    checker.incNumberOfValues().put(TOPIC_BI_MANY_B, tuple("test-29:inner-bi-b3", "test-27:test-30", "test-25:test-28")).check();

    senderBi.removeBiManyB(biB(2));
    // the bug from above does not occur here, although the situation is similar
    // so, only one message is sent
    checker.incNumberOfValues().put(TOPIC_BI_MANY_B, tuple("test-29:inner-bi-b3", "test-25:test-28")).check();

    biB(3).setToManyB(null);
    checker.incNumberOfValues().put(TOPIC_BI_MANY_B, tuple("test-25:test-28")).check();

    // change a nonterminal target of three relations must trigger three messages
    biB(1).setValue("test-31");
    checker.addToNumberOfValues(3)
            .put(TOPIC_BI_MY_B, "test-31:test-28")
            .put(TOPIC_BI_OPTIONAL_B, "test-31:test-28")
            .put(TOPIC_BI_MANY_B, tuple("test-31:test-28"))
            .check();

    // disconnect bi-my-a, bi-optional-a, bi-many-a - resetting afterwards must not trigger a message
    senderBi.disconnectBiMyB(mqttUri(TOPIC_BI_MY_B));
    senderBi.disconnectBiOptionalB(mqttUri(TOPIC_BI_OPTIONAL_B));
    senderBi.disconnectBiManyB(mqttUri(TOPIC_BI_MANY_B));
    biB(1).setValue("bi-b1");
    biB(1).getInner().setInnerValue("inner-bi-b1");
    biB(2).setValue("bi-b2");
    biB(2).getInner().setInnerValue("inner-bi-b2");
    biB(3).setValue("bi-b3");
    biB(3).getInner().setInnerValue("inner-bi-b3");
    checker.check();

    logger.debug(model.ragconnectEvaluationCounterSummary());
  }

  private void assertNullOrA(String expectedValue, A actual, String alias) {
    if (expectedValue == null) {
      assertNull(actual, alias);
      return;
    }
    final String expectedInner;
    if (expectedValue.contains(":")) {
      String[] tokens = expectedValue.split(":");
      assertEquals(2, tokens.length);
      expectedValue = tokens[0];
      expectedInner = tokens[1];
    } else {
      expectedInner = "inner-" + expectedValue;
    }
    assertThat(actual.getValue()).describedAs(alias + ".Value").isEqualTo(expectedValue);
    assertThat(actual.getInner()).describedAs(alias + ".inner != null").isNotNull();
    assertThat(actual.getInner().getInnerValue()).describedAs(alias + ".inner.Value").isEqualTo(expectedInner);
  }

  private void assertListEqualsForA(List<Object> expected, JastAddList<A> actual, String alias) {
    assertEquals(expected.size(), actual.getNumChild(), alias + ".size");
    for (int i = 0, expectedSize = expected.size(); i < expectedSize; i++) {
      String s = (String) expected.get(i);
      assertNullOrA(s, actual.getChild(i), alias + "[" + i + "]");
    }
  }

  private void assertNullOrB(String expectedValue, String actual, String alias) {
    final String expectedTransformed;
    if (expectedValue == null) {
      expectedTransformed = "";
    } else {
      final String expectedInner;
      if (expectedValue.contains(":")) {
        String[] tokens = expectedValue.split(":");
        assertEquals(2, tokens.length);
        expectedValue = tokens[0];
        expectedInner = tokens[1];
      } else {
        expectedInner = "inner-" + expectedValue;
      }
      expectedTransformed = expectedValue + "+" + expectedInner;
    }
    assertEquals(expectedTransformed, actual, alias);
  }

  private void assertListEqualsForB(List<Object> expected, String actual, String alias) {
    String[] actualTokens = actual.isEmpty() ? new String[0] : actual.split(";");
    assertEquals(expected.size(), actualTokens.length, alias + ".size");
    for (int i = 0, expectedSize = expected.size(); i < expectedSize; i++) {
      String s = (String) expected.get(i);
      assertNullOrB(s, actualTokens[i], alias + "[" + i + "]");
    }
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  private static class ReceiverData {
    int numberOfValues = 0;
  }
}
