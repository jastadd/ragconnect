package org.jastadd.ragconnect.tests;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;

/**
 * Use cases that just need to compile.
 *
 * @author rschoene - Initial contribution
 */
public class PassingTest extends AbstractCompilerTest {
  @Override
  protected String getDirectoryName() {
    return "passing";
  }

  @Override
  protected String getDefaultGrammarName() {
    return "Test";
  }

  protected void run(String inputDirectoryName, String rootNode) throws IOException {
    super.test(Paths.get("passing", inputDirectoryName).toString(),
            0, rootNode, getDefaultGrammarName());
  }

  @Test
  public void testInheritance() throws IOException {
    run("inheritance", "Root");
  }
}
