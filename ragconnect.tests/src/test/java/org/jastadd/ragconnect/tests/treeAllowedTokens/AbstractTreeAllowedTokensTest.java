package org.jastadd.ragconnect.tests.treeAllowedTokens;

import org.jastadd.ragconnect.tests.AbstractMqttTest;
import org.jastadd.ragconnect.tests.utils.DefaultMappings;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Tag;

import java.io.IOException;
import java.time.Instant;
import java.time.Period;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Base class for test cases "tree allowed tokens manual" and "tree allowed tokens incremental".
 *
 * @author rschoene - Initial contribution
 */
@Tag("Tree")
public abstract class AbstractTreeAllowedTokensTest extends AbstractMqttTest {

  protected static final String TOPIC_INPUT1TRUE = "input1/true";
  protected static final String TOPIC_INPUT1FALSE = "input1/false";
  protected static final String TOPIC_INPUT2 = "input2";
  protected static final String TOPIC_INPUT3 = "input3";
  protected static final String TOPIC_ALFA = "alfa";
  protected static final String TOPIC_ALFA_PRIMITIVE = "primitive";

  protected static final String INSTANT_A = "1999-12-03T10:15:30Z";
  protected static final String INSTANT_B = "2011-12-03T10:15:30Z";
  protected static final String INSTANT_C = "2012-12-03T10:15:30Z";

  protected TestWrapperReceiverRoot receiverRoot;

  public interface TestWrapperReceiverRoot {
    TestWrapperAlfa getAlfa();
    TestWrapperAlfa getAlfaPrimitive();
    boolean connectAlfa(String mqttUri) throws IOException;
    boolean connectAlfaPrimitive(String mqttUri) throws IOException;
    boolean disconnectAlfa(String mqttUri) throws IOException;
    boolean disconnectAlfaPrimitive(String mqttUri) throws IOException;
  }
  public interface TestWrapperAlfa {
    boolean getBooleanValue();
    int getIntValue();
    short getShortValue();
    long getLongValue();
    float getFloatValue();
    double getDoubleValue();
    String getStringValue();
    char getCharValue();
    Instant getInstantValue();
    Period getPeriodValue();
  }

  @Override
  protected void communicateSendInitialValue() throws InterruptedException, IOException {
    checkTree(1, false, 0, INSTANT_A, 0);
    checkPrimitiveTree(1, INSTANT_A);

    // flag: false. sendInput1WhenFalse(2)
    sendInput1WhenFalse(2);
    checkTree(2, false, 2, INSTANT_A, 0);
    checkPrimitiveTree(1, INSTANT_A);

    // flag: false. setFlag(false) -> no change
    setFlag(false);
    checkTree(2, false, 2, INSTANT_A, 0);
    checkPrimitiveTree(1, INSTANT_A);

    // flag: false. setFlag(true)
    setFlag(true);
    checkTree(3, true, 0, INSTANT_A, 0);
    checkPrimitiveTree(1, INSTANT_A);

    // flag: true. sendInput1WhenFalse(3) -> no change
    sendInput1WhenFalse(3);
    checkTree(3, true, 0, INSTANT_A, 0);
    checkPrimitiveTree(1, INSTANT_A);

    // flag: true. sendInput1WhenTrue(4)
    sendInput1WhenTrue(4);
    checkTree(4, true, 4, INSTANT_A, 0);
    checkPrimitiveTree(1, INSTANT_A);

    // sendInput2(INSTANT_B)
    sendInput2(INSTANT_B);
    checkTree(5, true, 4, INSTANT_B, 0);
    checkPrimitiveTree(2, INSTANT_B);

    // sendInput2(INSTANT_B) -> no change
    sendInput2(INSTANT_B);
    checkTree(5, true, 4, INSTANT_B, 0);
    checkPrimitiveTree(2, INSTANT_B);

    // sendInput3(5.1)
    sendInput3(5.1);
    checkTree(6, true, 4, INSTANT_B, 5.1);
    checkPrimitiveTree(2, INSTANT_B);

    // sendInput3(5.1) -> no change
    sendInput3(5.1);
    checkTree(6, true, 4, INSTANT_B, 5.1);
    checkPrimitiveTree(2, INSTANT_B);

    // sendInput3(7) -> send, but not receive
    disconnectReceive();
    sendInput3(7);
    checkTree(7, true, 4, INSTANT_B, 5.1);
    checkPrimitiveTree(2, INSTANT_B);

    // sendInput3(8) -> not sent
    disconnectSend();
    sendInput3(8);
    checkTree(7, true, 4, INSTANT_B, 5.1);
    checkPrimitiveTree(2, INSTANT_B);
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException, IOException {
    checkTree(0, false, null, null, 0);
    checkPrimitiveTree(0, null);

    // flag: false. sendInput1WhenFalse(12)
    sendInput1WhenFalse(12);
    checkTree(1, false, 12, INSTANT_A, 0);
    checkPrimitiveTree(0, null);

    // flag: false. setFlag(false) -> no change
    setFlag(false);
    checkTree(1, false, 12, INSTANT_A, 0);
    checkPrimitiveTree(0, null);

    // flag: false. setFlag(true)
    setFlag(true);
    checkTree(2, true, 0, INSTANT_A, 0);
    checkPrimitiveTree(0, null);

    // flag: true. sendInput1WhenFalse(13) -> no change
    sendInput1WhenFalse(13);
    checkTree(2, true, 0, INSTANT_A, 0);
    checkPrimitiveTree(0, null);

    // flag: true. sendInput1WhenTrue(14)
    sendInput1WhenTrue(14);
    checkTree(3, true, 14, INSTANT_A, 0);
    checkPrimitiveTree(0, null);

    // sendInput2(INSTANT_C)
    sendInput2(INSTANT_C);
    checkTree(4, true, 14, INSTANT_C, 0);
    checkPrimitiveTree(1, INSTANT_C);

    // sendInput2(INSTANT_C) -> no change
    sendInput2(INSTANT_C);
    checkTree(4, true, 14, INSTANT_C, 0);
    checkPrimitiveTree(1, INSTANT_C);

    // sendInput3(15.1)
    sendInput3(15.1);
    checkTree(5, true, 14, INSTANT_C, 15.1);
    checkPrimitiveTree(1, INSTANT_C);

    // sendInput3(15.1) -> no change
    sendInput3(15.1);
    checkTree(5, true, 14, INSTANT_C, 15.1);
    checkPrimitiveTree(1, INSTANT_C);

    // sendInput3(7) -> send, but not receive
    disconnectReceive();
    sendInput3(7);
    checkTree(6, true, 14, INSTANT_C, 15.1);
    checkPrimitiveTree(1, INSTANT_C);

    // sendInput3(8) -> not sent
    disconnectSend();
    sendInput3(8);
    checkTree(6, true, 14, INSTANT_C, 15.1);
    checkPrimitiveTree(1, INSTANT_C);
  }

  protected abstract void disconnectReceive() throws IOException;

  protected abstract void disconnectSend() throws IOException;

  protected void sendInput1WhenFalse(int value) {
    publisher.publish(TOPIC_INPUT1FALSE, DefaultMappings.IntToBytes(value));
  }

  protected void sendInput1WhenTrue(int value) {
    publisher.publish(TOPIC_INPUT1TRUE, DefaultMappings.IntToBytes(value));
  }

  protected void sendInput2(String value) {
    publisher.publish(TOPIC_INPUT2, DefaultMappings.StringToBytes(value));
  }

  protected void sendInput3(double value) {
    publisher.publish(TOPIC_INPUT3, DefaultMappings.DoubleToBytes(value));
  }

  protected abstract void setFlag(boolean value);

  protected void checkTree(int expectedCount, boolean expectedBooleanValue, Integer expectedIntValue, String expectedStringValue, double expectedDoubleValue) throws InterruptedException {
    TestUtils.waitForMqtt();

    assertEquals(expectedCount, data.numberOfTrees);
    if (expectedStringValue == null) {
      assertNull(receiverRoot.getAlfa());
    } else {
      assertNotNull(receiverRoot.getAlfa());
      TestWrapperAlfa alfa = receiverRoot.getAlfa();

      assertEquals(expectedBooleanValue, alfa.getBooleanValue());
      assertEquals(expectedIntValue, alfa.getIntValue());
      assertEquals(expectedIntValue.shortValue(), alfa.getShortValue());
      assertEquals(expectedIntValue.longValue(), alfa.getLongValue());

      assertEquals(expectedDoubleValue, alfa.getFloatValue(), TestUtils.DELTA);
      assertEquals(expectedDoubleValue, alfa.getDoubleValue(), TestUtils.DELTA);

      assertEquals(expectedStringValue, alfa.getStringValue());
      assertEquals(expectedStringValue.charAt(0), alfa.getCharValue());
      assertEquals(Instant.parse(expectedStringValue), alfa.getInstantValue());
      assertEquals(Period.of(0, 0, expectedIntValue), alfa.getPeriodValue());

      checkMyEnum(alfa, expectedBooleanValue);
    }
  }

  protected void checkPrimitiveTree(int expectedCount, String expectedStringValue) {
    assertEquals(expectedCount, data.numberOfPrimitiveTrees);
    if (expectedStringValue == null) {
      assertNull(receiverRoot.getAlfaPrimitive());
    } else {
      assertNotNull(receiverRoot.getAlfaPrimitive());

      TestWrapperAlfa alfaPrimitive = receiverRoot.getAlfaPrimitive();
      assertEquals(expectedStringValue, alfaPrimitive.getStringValue());
    }
  }

  protected abstract void checkMyEnum(TestWrapperAlfa alfa, boolean expectedBooleanValue);

  protected ReceiverData data;

  protected static class ReceiverData {
    int numberOfTrees = 0;
    int numberOfPrimitiveTrees = 0;
  }

}
