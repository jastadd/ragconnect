package org.jastadd.ragconnect.tests.treeAllowedTokens;

import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Test;
import treeAllowedTokens.ast.*;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.jastadd.ragconnect.tests.utils.TestUtils.testJaddContainReferenceToJackson;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test case "tree allowed tokens manual"
 *
 * @author rschoene - Initial contribution
 */
public class TreeAllowedTokensManualTest extends AbstractTreeAllowedTokensTest {

  private Root model;
  private SenderRoot senderRoot;
  private MqttHandler handler;

  @Test
  public void checkJacksonReference() {
    testJaddContainReferenceToJackson(
        Paths.get("src", "test",
            "02-after-ragconnect", "treeAllowedTokens", "RagConnect.jadd"), true);
  }

  @Override
  protected void createModel() {
    model = new Root();
    senderRoot = new SenderRoot();
    senderRoot.setFlag(false);
    senderRoot.setInput2(INSTANT_A);
    model.addSenderRoot(senderRoot);

    receiverRoot = new ReceiverRoot();
    model.addReceiverRoot((ReceiverRoot) receiverRoot);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    // add dependencies
    senderRoot.addFlagDependency(senderRoot);
    senderRoot.addInput1WhenFlagIsFalseDependency(senderRoot);
    senderRoot.addInput1WhenFlagIsTrueDependency(senderRoot);
    senderRoot.addInput2Dependency(senderRoot);
    senderRoot.addInput3Dependency(senderRoot);
    senderRoot.addPrimitiveInput2Dependency(senderRoot);

    data = new ReceiverData();
    handler.newConnection(TOPIC_ALFA, bytes -> data.numberOfTrees += 1);
    handler.newConnection(TOPIC_ALFA_PRIMITIVE, bytes -> data.numberOfPrimitiveTrees += 1);

    // connect. important: first receiver, then sender. to not miss initial value.
    assertTrue(senderRoot.connectInput1WhenFlagIsFalse(mqttUri(TOPIC_INPUT1FALSE)));
    assertTrue(senderRoot.connectInput1WhenFlagIsTrue(mqttUri(TOPIC_INPUT1TRUE)));
    assertTrue(senderRoot.connectInput2(mqttUri(TOPIC_INPUT2)));
    assertTrue(senderRoot.connectInput3(mqttUri(TOPIC_INPUT3)));
    assertTrue(receiverRoot.connectAlfa(mqttUri(TOPIC_ALFA)));
    assertTrue(receiverRoot.connectAlfaPrimitive(mqttUri(TOPIC_ALFA_PRIMITIVE)));
    assertTrue(senderRoot.connectAlfa(mqttUri(TOPIC_ALFA), isWriteCurrentValue()));
    assertTrue(senderRoot.connectAlfaPrimitive(mqttUri(TOPIC_ALFA_PRIMITIVE), isWriteCurrentValue()));
  }

  protected void setFlag(boolean value) {
    senderRoot.setFlag(value);
  }

  @Override
  protected void checkMyEnum(TestWrapperAlfa alfa, boolean expectedBooleanValue) {
    assertEquals(expectedBooleanValue ? MyEnum.TRUE : MyEnum.FALSE, ((Alfa) alfa).getEnumValue());
  }

  @Override
  protected void disconnectReceive() throws IOException {
    assertTrue(receiverRoot.disconnectAlfa(mqttUri(TOPIC_ALFA)));
    assertTrue(receiverRoot.disconnectAlfaPrimitive(mqttUri(TOPIC_ALFA_PRIMITIVE)));
  }

  @Override
  protected void disconnectSend() throws IOException {
    assertTrue(senderRoot.disconnectInput1WhenFlagIsFalse(mqttUri(TOPIC_INPUT1FALSE)));
    assertTrue(senderRoot.disconnectInput1WhenFlagIsTrue(mqttUri(TOPIC_INPUT1TRUE)));
    assertTrue(senderRoot.disconnectInput2(mqttUri(TOPIC_INPUT2)));
    assertTrue(senderRoot.disconnectInput3(mqttUri(TOPIC_INPUT3)));
    assertTrue(senderRoot.disconnectAlfa(mqttUri(TOPIC_ALFA)));
    assertTrue(senderRoot.disconnectAlfaPrimitive(mqttUri(TOPIC_ALFA_PRIMITIVE)));
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }
}
