package org.jastadd.ragconnect.tests;

import org.jastadd.ragconnect.tests.utils.DefaultMappings;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import read2write1.ast.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "read-2-write-1".
 *
 * @author rschoene - Initial contribution
 */
public class Read2Write1Test extends AbstractMqttTest {

  private static final String TOPIC_SAME_READ1 = "same/read1";
  private static final String TOPIC_SAME_READ2 = "same/read2";
  private static final String TOPIC_SAME_WRITE_INT = "same/write/int";
  private static final String TOPIC_DIFFERENT_READ1 = "different/read1";
  private static final String TOPIC_DIFFERENT_READ2 = "different/read2";
  private static final String TOPIC_DIFFERENT_WRITE1_INT = "different/write1/int";
  private static final String TOPIC_DIFFERENT_WRITE2_INT = "different/write2/int";
  private static final String INITIAL_VALUE = "0";

  private MqttHandler handler;
  private A model;
  private OnSameNonterminal onSameNonterminal;
  private OnDifferentNonterminal onDifferentNonterminal;
  private TheOther other1;
  private TheOther other2;

  private ReceiverData dataSame;
  private ReceiverData dataOther1;
  private ReceiverData dataOther2;

  @Override
  protected void createModel() {
    // Setting value for Input without dependencies does not trigger any updates
    model = new A();

    onSameNonterminal = new OnSameNonterminal();
    model.setOnSameNonterminal(onSameNonterminal);
    onSameNonterminal.setInput1(INITIAL_VALUE);
    onSameNonterminal.setInput2(INITIAL_VALUE);

    onDifferentNonterminal = new OnDifferentNonterminal();
    other1 = new TheOther();
    other2 = new TheOther();
    onDifferentNonterminal.addTheOther(other1);
    onDifferentNonterminal.addTheOther(other2);
    model.setOnDifferentNonterminal(onDifferentNonterminal);
    onDifferentNonterminal.setInput1(INITIAL_VALUE);
    onDifferentNonterminal.setInput2(INITIAL_VALUE);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    onSameNonterminal.addInt1Dependency(onSameNonterminal);
    onSameNonterminal.addInt2Dependency(onSameNonterminal);
    other1.addInt1Dependency(onDifferentNonterminal);
    other1.addInt2Dependency(onDifferentNonterminal);
    other2.addInt1Dependency(onDifferentNonterminal);
    other2.addInt2Dependency(onDifferentNonterminal);

    dataSame = new Read2Write1Test.ReceiverData();
    dataOther1 = new Read2Write1Test.ReceiverData();
    dataOther2 = new Read2Write1Test.ReceiverData();

    handler.newConnection(TOPIC_SAME_WRITE_INT, bytes -> {
      dataSame.numberOfIntValues += 1;
      dataSame.lastIntValue = DefaultMappings.BytesToInt(bytes);
    });

    handler.newConnection(TOPIC_DIFFERENT_WRITE1_INT, bytes -> {
      dataOther1.numberOfIntValues += 1;
      dataOther1.lastIntValue = DefaultMappings.BytesToInt(bytes);
    });

    handler.newConnection(TOPIC_DIFFERENT_WRITE2_INT, bytes -> {
      dataOther2.numberOfIntValues += 1;
      dataOther2.lastIntValue = DefaultMappings.BytesToInt(bytes);
    });

    assertTrue(onSameNonterminal.connectInput1(mqttUri(TOPIC_SAME_READ1)));
    assertTrue(onSameNonterminal.connectInput2(mqttUri(TOPIC_SAME_READ2)));
    assertTrue(onSameNonterminal.connectOutInteger(mqttUri(TOPIC_SAME_WRITE_INT), isWriteCurrentValue()));

    assertTrue(onDifferentNonterminal.connectInput1(mqttUri(TOPIC_DIFFERENT_READ1)));
    assertTrue(onDifferentNonterminal.connectInput2(mqttUri(TOPIC_DIFFERENT_READ2)));
    assertTrue(other1.connectOutInteger(mqttUri(TOPIC_DIFFERENT_WRITE1_INT), isWriteCurrentValue()));
    assertTrue(other2.connectOutInteger(mqttUri(TOPIC_DIFFERENT_WRITE2_INT), isWriteCurrentValue()));
  }

  @Override
  protected void communicateSendInitialValue() throws InterruptedException {
    // check initial value
    checkData(1, Integer.parseInt(INITIAL_VALUE + INITIAL_VALUE),
        1, Integer.parseInt(INITIAL_VALUE + INITIAL_VALUE));

    communicateBoth(1);
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException {
    // check initial value
    checkData(0, null,
            0, null);

    communicateBoth(0);
  }

  private void communicateBoth(int initialNumberOfValues) throws InterruptedException {
    // set new value
    sendData(true, "2", true, "3");

    // check new value. same: 2, 0. different: 3, 0.
    checkData(initialNumberOfValues + 1, 20,
            initialNumberOfValues + 1, 30);

    // set new value
    sendData(false, "4", false, "4");

    // check new value. same: 2, 4. different: 3, 4.
    checkData(initialNumberOfValues + 2, 24,
            initialNumberOfValues + 2, 34);

    // set new value only for same
    publisher.publish(TOPIC_SAME_READ1, "78".getBytes());

    // check new value. same: 78, 4. different: 3, 4.
    checkData(initialNumberOfValues + 3, 784,
            initialNumberOfValues + 2, 34);
  }

  @Override
  public void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  private void sendData(boolean useSameInput1, String inputSame,
                        boolean useDifferentInput1, String inputDifferent) {
    publisher.publish(useSameInput1 ? TOPIC_SAME_READ1 : TOPIC_SAME_READ2,
        inputSame.getBytes());
    publisher.publish(useDifferentInput1 ? TOPIC_DIFFERENT_READ1 : TOPIC_DIFFERENT_READ2,
        inputDifferent.getBytes());
  }

  private void checkData(int numberOfSameValues, Integer lastSameIntValue,
                         int numberOfDifferentValues, Integer lastDifferentIntValue)
      throws InterruptedException {
    TestUtils.waitForMqtt();
    /* the value "-2" is never used in the test, so a test will always fail comparing to this value
     especially, it is not the initial value */
    dataSame.assertEqualData(numberOfSameValues, lastSameIntValue);
    dataOther1.assertEqualData(numberOfDifferentValues, lastDifferentIntValue);
    dataOther2.assertEqualData(numberOfDifferentValues, lastDifferentIntValue);
  }

  private static class ReceiverData {
    int lastIntValue;
    int numberOfIntValues = 0;

    void assertEqualData(int expectedNumberOfValues, Integer expectedLastIntValue) {
      /* the value "-2" is never used in the test, so a test will always fail comparing to this value
         especially, it is not the initial value */
      assertEquals(expectedNumberOfValues, this.numberOfIntValues);
      if (expectedNumberOfValues > 0) {
        assertEquals(expectedLastIntValue != null ? expectedLastIntValue : -2, this.lastIntValue);
      }
    }
  }

}
