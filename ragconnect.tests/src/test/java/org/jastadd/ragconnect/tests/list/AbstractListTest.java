package org.jastadd.ragconnect.tests.list;

import org.jastadd.ragconnect.tests.AbstractMqttTest;
import org.jastadd.ragconnect.tests.utils.IntList;
import org.jastadd.ragconnect.tests.utils.TestChecker;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;

import static org.jastadd.ragconnect.tests.utils.IntList.list;
import static org.jastadd.ragconnect.tests.utils.TestUtils.testJaddContainReferenceToJackson;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Base class for test cases "list manual" and "list incremental".
 *
 * @author rschoene - Initial contribution
 */
@Tag("List")
public abstract class AbstractListTest extends AbstractMqttTest {

  public interface TestWrapperJastAddList<T> extends Iterable<T> {
    int getNumChild();
  }
  public interface TestWrapperReceiverRoot {
    TestWrapperJastAddList<? extends TestWrapperA> getAList();
    TestWrapperJastAddList<? extends TestWrapperA> getAs();
    int getNumA();
    TestWrapperA getA(int index);

    TestWrapperJastAddList<? extends TestWrapperA> getFromSingleAList();
    TestWrapperJastAddList<? extends TestWrapperA> getFromSingleAs();

    TestWrapperJastAddList<? extends TestWrapperA> getWithAddFromAList();
    TestWrapperJastAddList<? extends TestWrapperA> getWithAddFromAs();

    TestWrapperJastAddList<? extends TestWrapperA> getWithAddFromSingleAList();
    TestWrapperJastAddList<? extends TestWrapperA> getWithAddFromSingleAs();

    boolean connectAList(String mqttUri) throws IOException;
    boolean connectFromSingleAList(String mqttUri) throws IOException;
    boolean connectWithAddFromAList(String mqttUri) throws IOException;
    boolean connectWithAddFromSingleAList(String mqttUri) throws IOException;
  }
  public interface TestWrapperA {
    AbstractListTest.TestWrapperB getB(int i);
    int getNumB();
    int getID();
  }
  public interface TestWrapperB {
    int getID();
  }

  protected static final String TOPIC_A = "a";
  protected static final String TOPIC_SINGLE_A = "single-a";
  protected TestChecker checker;

  protected TestWrapperReceiverRoot receiverRoot;
  protected ReceiverData data;
  protected ReceiverData dataSingle;

  private final String shortName;

  AbstractListTest(String shortName) {
    this.shortName = shortName;
    checker = new TestChecker();
    this.checker.setActualNumberOfValues(() -> data.numberOfElements);
  }

  @Test
  public void checkJacksonReference() {
    testJaddContainReferenceToJackson(
        Paths.get("src", "test",
            "02-after-ragconnect", shortName, "RagConnect.jadd"), true);
  }

  @Override
  protected void communicateSendInitialValue() throws IOException {
    checker.incNumberOfValues();
    checkTree(1, list(), list(0), list(), list(0));

    communicateBoth(1, 0);
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws IOException {
    checkTree(0, list(), list(), list(), list());

    communicateBoth(0, null);
  }

  private void communicateBoth(int initialNumberOfValues, Integer firstElementOfWithAddFromSingleA) throws IOException {
    setInput(1);
    checker.incNumberOfValues();
    checkTree(initialNumberOfValues + 1, list(1), list(1), list(1),
            list(firstElementOfWithAddFromSingleA, 1));

    setInput(1);
    checkTree(initialNumberOfValues + 1, list(1), list(1), list(1),
            list(firstElementOfWithAddFromSingleA, 1));

    setInput(2);
    checker.incNumberOfValues();
    checkTree(initialNumberOfValues + 2, list(1, 2), list(2), list(1, 1, 2),
            list(firstElementOfWithAddFromSingleA, 1, 2));

    setInput(3);
    checker.incNumberOfValues();
    checkTree(initialNumberOfValues + 3, list(1, 2, 3), list(3), list(1, 1, 2, 1, 2, 3),
            list(firstElementOfWithAddFromSingleA, 1, 2, 3));

    disconnectReceive();
    setInput(4);
    checker.incNumberOfValues();
    checkTree(initialNumberOfValues + 4, list(1, 2, 3), list(3), list(1, 1, 2, 1, 2, 3),
            list(firstElementOfWithAddFromSingleA, 1, 2, 3));

    disconnectSend();
    setInput(5);
    checkTree(initialNumberOfValues + 4, list(1, 2, 3), list(3), list(1, 1, 2, 1, 2, 3),
            list(firstElementOfWithAddFromSingleA, 1, 2, 3));
  }

  protected abstract void disconnectReceive() throws IOException;

  protected abstract void disconnectSend() throws IOException;

  protected abstract void setInput(int input);

  private void checkTree(int expectedTransmissions, IntList normalA, IntList fromSingleA, IntList withAddA, IntList withAddFromSingleA) {
    checker.check();

    assertEquals(expectedTransmissions, data.numberOfElements, "transmissions for normal");
    assertEquals(expectedTransmissions, dataSingle.numberOfElements, "transmissions for single");

    checkList(normalA.toList(), receiverRoot.getNumA(), receiverRoot::getA);
    checkList(normalA.toList(), receiverRoot.getAList(), true);
    checkList(normalA.toList(), receiverRoot.getAs(), true);

    checkList(fromSingleA.toList(), receiverRoot.getFromSingleAList(), false);
    checkList(fromSingleA.toList(), receiverRoot.getFromSingleAs(), false);

    checkList(withAddA.toList(), receiverRoot.getWithAddFromAList(), true);
    checkList(withAddA.toList(), receiverRoot.getWithAddFromAs(), true);

    checkList(withAddFromSingleA.toList(), receiverRoot.getWithAddFromSingleAList(), false);
    checkList(withAddFromSingleA.toList(), receiverRoot.getWithAddFromSingleAs(), false);
  }

  private void checkList(List<Integer> expectedList, int numChildren, Function<Integer, TestWrapperA> getA) {
    assertEquals(expectedList.size(), numChildren, "same list size");
    for (int index = 0; index < expectedList.size(); index++) {
      TestWrapperA a = getA.apply(index);
      assertEquals(expectedList.get(index), a.getID(), "correct ID for A");
      assertEquals(1, a.getNumB(), "one B child");
      assertEquals(expectedList.get(index) + 1, a.getB(0).getID(), "correct ID for B child");
    }
  }

  private void checkList(List<Integer> expectedList, TestWrapperJastAddList<? extends TestWrapperA> actualList, boolean expectB) {
    assertEquals(expectedList.size(), actualList.getNumChild(), "same list size");
    int index = 0;
    for (TestWrapperA a : actualList) {
      assertEquals(expectedList.get(index), a.getID(), "correct ID for A");
      if (expectB) {
        assertEquals(1, a.getNumB(), "one B child");
        assertEquals(expectedList.get(index) + 1, a.getB(0).getID(), "correct ID for B child");
      }
      index++;
    }
  }

  protected static class ReceiverData {
    int numberOfElements = 0;
  }

}
