package org.jastadd.ragconnect.tests.singleList;

import org.jastadd.ragconnect.tests.AbstractMqttTest;
import org.jastadd.ragconnect.tests.utils.IntList;
import org.jastadd.ragconnect.tests.utils.TestChecker;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import singleList.ast.MqttHandler;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

import static org.jastadd.ragconnect.tests.utils.TestUtils.*;
import static org.jastadd.ragconnect.tests.utils.IntList.list;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Base class for test cases "singleList manual" and "singleList incremental".
 *
 * @author rschoene - Initial contribution
 */
@Tag("List")
@Tag("SingleList")
public abstract class AbstractSingleListTest extends AbstractMqttTest {

  private final TestChecker checker;
  protected MqttHandler handler;

  public interface TestWrapperJastAddList<T> extends Iterable<T> {
    int getNumChild();
  }
  public interface TestWrapperReceiverRoot {
    TestWrapperJastAddList<? extends TestWrapperA> getAList();
    TestWrapperJastAddList<? extends TestWrapperA> getAs();
    int getNumA();
    int getNumWithAddA();
    TestWrapperA getA(int index);

    TestWrapperJastAddList<? extends TestWrapperA> getWithAddAList();
    TestWrapperJastAddList<? extends TestWrapperA> getUsingWildcardAList();
    TestWrapperJastAddList<? extends TestWrapperA> getUsingWildcardWithAddAList();

    @SuppressWarnings("unused") boolean connectA(String mqttUri) throws IOException;
    boolean connectA(String mqttUri, int index) throws IOException;
    boolean connectUsingWildcardA(String mqttUri) throws IOException;
    @SuppressWarnings("unused") boolean connectUsingWildcardA(String mqttUri, int index) throws IOException;
    boolean connectWithAddA(String mqttUri) throws IOException;
    boolean connectUsingWildcardWithAddA(String mqttUri) throws IOException;

    boolean disconnectA(String mqttUri) throws IOException;
    boolean disconnectUsingWildcardA(String mqttUri) throws IOException;
    boolean disconnectWithAddA(String mqttUri) throws IOException;
    boolean disconnectUsingWildcardWithAddA(String mqttUri) throws IOException;
  }
  @SuppressWarnings("UnusedReturnValue")
  public interface TestWrapperSenderRoot {
    boolean connectA1(String mqttUri, boolean writeCurrentValue) throws IOException;
    boolean connectA2(String mqttUri, boolean writeCurrentValue) throws IOException;
    boolean connectA3(String mqttUri, boolean writeCurrentValue) throws IOException;
    boolean connectA4(String mqttUri, boolean writeCurrentValue) throws IOException;
    boolean connectInOutput(String mqttUri, boolean writeCurrentValue) throws IOException;

    boolean disconnectA1(String mqttUri) throws IOException;
    boolean disconnectA2(String mqttUri) throws IOException;
    boolean disconnectA3(String mqttUri) throws IOException;
    boolean disconnectA4(String mqttUri) throws IOException;
    boolean disconnectInOutput(String mqttUri) throws IOException;

    TestWrapperSenderRoot setInput1(int input);
    TestWrapperSenderRoot setInput2(int input);
    TestWrapperSenderRoot setInput3(int input);
    TestWrapperSenderRoot setInput4(int input);
    TestWrapperSenderRoot setInOutput(int input);

    TestWrapperA getA1();
    TestWrapperA getA2();
    TestWrapperA getA3();
    TestWrapperA getA4();
  }
  public interface TestWrapperA {
    int getID();
  }

  AbstractSingleListTest(String shortName) {
    this.shortName = shortName;
    this.checker = new TestChecker();
    this.checker.setActualNumberOfValues(() -> data.numberOfElements);
  }

  protected static final String TOPIC_A_1 = "a/first";
  protected static final String TOPIC_A_2 = "a/second";
  protected static final String TOPIC_A_3 = "a/third";
  protected static final String TOPIC_A_4 = "a/fourth";
  protected static final String TOPIC_A_5_INOUT = "a/special";
  protected static final String TOPIC_A_WILDCARD = "a/#";

  protected TestWrapperSenderRoot senderRoot;
  protected TestWrapperReceiverRoot receiverRoot;
  protected ReceiverData data;

  private final String shortName;

  @Test
  public void checkJacksonReference() {
    testJaddContainReferenceToJackson(
        Paths.get("src", "test",
            "02-after-ragconnect", shortName, "RagConnect.jadd"), true);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException, InterruptedException {
    // late model initialization
    senderRoot.setInput1(0);
    senderRoot.setInput2(0);
    senderRoot.setInput3(0);
    senderRoot.setInput4(0);
    senderRoot.setInOutput(0);

    setupReceiverAndConnectPart();

    // connect. important: first receivers, then senders. to not miss initial value.

    // receive: explicit topic subscription
    assertTrue(receiverRoot.connectA(mqttUri(TOPIC_A_1), 0));
    assertTrue(receiverRoot.connectA(mqttUri(TOPIC_A_2), 1));
    assertTrue(receiverRoot.connectA(mqttUri(TOPIC_A_3), 2));
    assertTrue(receiverRoot.connectA(mqttUri(TOPIC_A_4), 3));
    assertTrue(receiverRoot.connectA(mqttUri(TOPIC_A_5_INOUT), 4));

    assertTrue(receiverRoot.connectWithAddA(mqttUri(TOPIC_A_1)));
    assertTrue(receiverRoot.connectWithAddA(mqttUri(TOPIC_A_2)));
    assertTrue(receiverRoot.connectWithAddA(mqttUri(TOPIC_A_3)));
    assertTrue(receiverRoot.connectWithAddA(mqttUri(TOPIC_A_4)));
    assertTrue(receiverRoot.connectWithAddA(mqttUri(TOPIC_A_5_INOUT)));

    // receive: wildcard subscription
    assertTrue(receiverRoot.connectUsingWildcardA(mqttUri(TOPIC_A_WILDCARD)));
    assertTrue(receiverRoot.connectUsingWildcardWithAddA(mqttUri(TOPIC_A_WILDCARD)));

    // send: explicit topics, wait between connections to ensure correct arrival at receiver
    handler = new MqttHandler().dontSendWelcomeMessage().setHost(TestUtils.getMqttHost());
    data = new ReceiverData();
    handler.waitUntilReady(2, TimeUnit.SECONDS);
    handler.newConnection("#", bytes -> {
      data.valueSentSinceLastCheck.set(true);
      data.numberOfElements += 1;
    });

    assertTrue(senderRoot.connectA4(mqttUri(TOPIC_A_4), isWriteCurrentValue()));
    waitForValue();

    assertTrue(senderRoot.connectA3(mqttUri(TOPIC_A_3), isWriteCurrentValue()));
    waitForValue();

    assertTrue(senderRoot.connectA2(mqttUri(TOPIC_A_2), isWriteCurrentValue()));
    waitForValue();

    assertTrue(senderRoot.connectA1(mqttUri(TOPIC_A_1), isWriteCurrentValue()));
    waitForValue();

    assertTrue(senderRoot.connectInOutput(mqttUri(TOPIC_A_5_INOUT), isWriteCurrentValue()));
    // no need to wait here, because first "checkTree" will wait anyway
  }

  private void waitForValue() {
    if (isWriteCurrentValue()) {
      awaitMqtt().until(() -> data.valueSentSinceLastCheck.getAndSet(false));
    }
  }

  abstract protected void setupReceiverAndConnectPart() throws IOException;

  @Override
  protected void communicateSendInitialValue() throws InterruptedException, IOException {
    checker.addToNumberOfValues(5);
    checkTree(list(1, 2, 3, 4, 0), list(4, 3, 2, 1, 0), // normal: (normal / wildcard)
            list(4, 3, 2, 1, 0), list(4, 3, 2, 1, 0)); // withAdd: (normal / wildcard)

    // A1 will be 2 (1+1, previously 1)
    setInput(1, 1);
    checker.incNumberOfValues();
    checkTree(list(2, 2, 3, 4, 0), list(4, 3, 2, 2, 0), // normal: (normal / wildcard)
            list(4, 3, 2, 1, 0, 2), list(4, 3, 2, 1, 0, 2)); // withAdd: (normal / wildcard)

    // A1 should stay at 2
    setInput(1, 1);
    checkTree(list(2, 2, 3, 4, 0), list(4, 3, 2, 2, 0), // normal: (normal / wildcard)
            list(4, 3, 2, 1, 0, 2), list(4, 3, 2, 1, 0, 2)); // withAdd: (normal / wildcard)

    // A1 will be 3 (2+1, previously 2)
    setInput(1, 2);
    checker.incNumberOfValues();
    checkTree(list(3, 2, 3, 4, 0), list(4, 3, 2, 3, 0), // normal: (normal / wildcard)
            list(4, 3, 2, 1, 0, 2, 3), list(4, 3, 2, 1, 0, 2, 3)); // withAdd: (normal / wildcard)

    // InOut will be 5 (previously 0)
    setInput(5, 5);
    checker.incNumberOfValues();
    checkTree(list(3, 2, 3, 4, 5), list(4, 3, 2, 3, 5), // normal: (normal / wildcard)
            list(4, 3, 2, 1, 0, 2, 3, 5), list(4, 3, 2, 1, 0, 2, 3, 5)); // withAdd: (normal / wildcard)

    // A3 will be 7 (4+3, previously 3)
    setInput(3, 4);
    checker.incNumberOfValues();
    checkTree(list(3, 2, 7, 4, 5), list(4, 7, 2, 3, 5), // normal: (normal / wildcard)
            list(4, 3, 2, 1, 0, 2, 3, 5, 7), list(4, 3, 2, 1, 0, 2, 3, 5, 7)); // withAdd: (normal / wildcard)

    // A2 will be sent, but not received
    disconnectReceive();
    setInput(2, 5);
    checker.incNumberOfValues();
    checkTree(list(3, 2, 7, 4, 5), list(4, 7, 2, 3, 5), // normal: (normal / wildcard)
            list(4, 3, 2, 1, 0, 2, 3, 5, 7), list(4, 3, 2, 1, 0, 2, 3, 5, 7)); // withAdd: (normal / wildcard)

    // A2 will not be sent
    disconnectSend();
    setInput(2, 7);
    checkTree(list(3, 2, 7, 4, 5), list(4, 7, 2, 3, 5), // normal: (normal / wildcard)
            list(4, 3, 2, 1, 0, 2, 3, 5, 7), list(4, 3, 2, 1, 0, 2, 3, 5, 7)); // withAdd: (normal / wildcard)
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException, IOException {
    checkTree(list(0, 0, 0, 0, 0), list(), // normal
            list(), list()); // withAdd

    // A1 will be 2 (1+1, previously 1)
    setInput(1, 1);
    checker.incNumberOfValues();
    checkTree(list(2, 0, 0, 0, 0), list(2), // normal
            list(2), list(2)); // withAdd

    // A1 should stay at 2
    setInput(1, 1);
    checkTree(list(2, 0, 0, 0, 0), list(2), // normal
            list(2), list(2)); // withAdd

    // A1 will be 3 (2+1, previously 2)
    setInput(1, 2);
    checker.incNumberOfValues();
    checkTree(list(3, 0, 0, 0, 0), list(3), // normal
            list(2, 3), list(2, 3)); // withAdd

    // InOut will be 5 (previously 0)
    setInput(5, 5);
    checker.incNumberOfValues();
    checkTree(list(3, 0, 0, 0, 5), list(3, 5), // normal
            list(2, 3, 5), list(2, 3, 5)); // withAdd

    // A3 will be 7 (4+3, previously 3)
    setInput(3, 4);
    checker.incNumberOfValues();
    checkTree(list(3, 0, 7, 0, 5), list(3, 5, 7), // normal
            list(2, 3, 5, 7), list(2, 3, 5, 7)); // withAdd

    // A2 will be sent, but not received
    disconnectReceive();
    setInput(2, 5);
    checker.incNumberOfValues();
    checkTree(list(3, 0, 7, 0, 5), list(3, 5, 7), // normal
            list(2, 3, 5, 7), list(2, 3, 5, 7)); // withAdd

    // A2 will not be sent
    disconnectSend();
    setInput(2, 7);
    checkTree(list(3, 0, 7, 0, 5), list(3, 5, 7), // normal
            list(2, 3, 5, 7), list(2, 3, 5, 7)); // withAdd
  }

  protected void disconnectReceive() throws IOException {
    assertTrue(receiverRoot.disconnectA(mqttUri(TOPIC_A_1)));
    assertTrue(receiverRoot.disconnectA(mqttUri(TOPIC_A_2)));
    assertTrue(receiverRoot.disconnectA(mqttUri(TOPIC_A_3)));
    assertTrue(receiverRoot.disconnectA(mqttUri(TOPIC_A_4)));
    assertTrue(receiverRoot.disconnectA(mqttUri(TOPIC_A_5_INOUT)));

    assertTrue(receiverRoot.disconnectWithAddA(mqttUri(TOPIC_A_1)));
    assertTrue(receiverRoot.disconnectWithAddA(mqttUri(TOPIC_A_2)));
    assertTrue(receiverRoot.disconnectWithAddA(mqttUri(TOPIC_A_3)));
    assertTrue(receiverRoot.disconnectWithAddA(mqttUri(TOPIC_A_4)));
    assertTrue(receiverRoot.disconnectWithAddA(mqttUri(TOPIC_A_5_INOUT)));

    // receive: wildcard subscription
    assertTrue(receiverRoot.disconnectUsingWildcardA(mqttUri(TOPIC_A_WILDCARD)));
    assertTrue(receiverRoot.disconnectUsingWildcardWithAddA(mqttUri(TOPIC_A_WILDCARD)));
  }

  protected void disconnectSend() throws IOException {
    assertTrue(senderRoot.disconnectA4(mqttUri(TOPIC_A_4)));
    assertTrue(senderRoot.disconnectA3(mqttUri(TOPIC_A_3)));
    assertTrue(senderRoot.disconnectA2(mqttUri(TOPIC_A_2)));
    assertTrue(senderRoot.disconnectA1(mqttUri(TOPIC_A_1)));
    assertTrue(senderRoot.disconnectInOutput(mqttUri(TOPIC_A_5_INOUT)));
  }

  protected void setInput(int index, int input) {
    int actualComputedValue;
    switch (index) {
      case 1: senderRoot.setInput1(input); actualComputedValue = senderRoot.getA1().getID(); break;
      case 2: senderRoot.setInput2(input); actualComputedValue = senderRoot.getA2().getID(); break;
      case 3: senderRoot.setInput3(input); actualComputedValue = senderRoot.getA3().getID(); break;
      case 4: senderRoot.setInput4(input); actualComputedValue = senderRoot.getA4().getID(); break;
      case 5: senderRoot.setInOutput(input); return;
      default: fail("Wrong index " + index); return;
    }
    assertEquals(input + index, actualComputedValue, "ID value of single A");
  }

  private void checkTree(IntList normalA, IntList usingWildcardA, IntList withAddA, IntList usingWildcardWithAddA) {
    checker.check();

    checkList(normalA.toList(), receiverRoot.getNumA(), receiverRoot::getA);
    checkList(normalA.toList(), receiverRoot.getAList());

    checkList(usingWildcardA.toList(), receiverRoot.getUsingWildcardAList());

    checkList(withAddA.toList(), receiverRoot.getWithAddAList());

    checkList(usingWildcardWithAddA.toList(), receiverRoot.getUsingWildcardWithAddAList());
  }

  private void checkList(List<Integer> expectedList, int numChildren, Function<Integer, TestWrapperA> getA) {
    assertEquals(expectedList.size(), numChildren, "same list size");
    for (int index = 0; index < expectedList.size(); index++) {
      TestWrapperA a = getA.apply(index);
      assertEquals(expectedList.get(index), a.getID(), "correct ID for A");
    }
  }

  private void checkList(List<Integer> expectedList, TestWrapperJastAddList<? extends TestWrapperA> actualList) {
    assertEquals(expectedList.size(), actualList.getNumChild(), "same list size");
    int index = 0;
    for (TestWrapperA a : actualList) {
      assertEquals(expectedList.get(index), a.getID(), "correct ID for A");
      index++;
    }
  }

  protected static class ReceiverData {
    int numberOfElements = 0;
    AtomicBoolean valueSentSinceLastCheck = new AtomicBoolean(false);
  }

}
