package org.jastadd.ragconnect.tests;

import indexedSendInc.ast.*;
import io.github.artsok.RepeatedIfExceptionsTest;
import org.assertj.core.api.Assertions;
import org.assertj.core.groups.Tuple;
import org.jastadd.ragconnect.tests.utils.TestChecker;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Tag;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static org.assertj.core.groups.Tuple.tuple;
import static org.jastadd.ragconnect.tests.utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "indexedSend (Incremental)".
 *
 * @author rschoene - Initial contribution
 */
@Tag("Incremental")
public class IndexedSendTest extends AbstractMqttTest {

  private static final String TOPIC_A_MANY_NORMAL_WILDCARD = "a-many/#";
  private static final String TOPIC_A_MANY_NORMAL_0 = "a-many/0";
  private static final String TOPIC_A_MANY_NORMAL_1 = "a-many/1";
  private static final String TOPIC_A_MANY_SUFFIX_WILDCARD = "a-many-suffix/#";
  private static final String TOPIC_A_MANY_SUFFIX_0 = "a-many-suffix/0";
  private static final String TOPIC_A_MANY_SUFFIX_1 = "a-many-suffix/1";
  private static final String TOPIC_A_MANY_SUFFIX_2 = "a-many-suffix/2";

  private static final String CHECK_MANY_A = "many-a";
  private static final String CHECK_WITH_SUFFIX = "many-a-with-suffix";

  private boolean connectNTAsInstead;

  private MqttHandler handler;
  private ReceiverData data;
  private TestChecker checker;

  private Root model;
  private SenderRoot senderRoot;
  private ReceiverRoot receiverRoot;

  private A listA0;
  private A listA1;
  private A listA0InSuffix;
  private A listA1InSuffix;

  @Override
  protected void createModel() {
    model = new Root();
    // model.trace().setReceiver(TestUtils::logEvent);
    senderRoot = new SenderRoot();
    receiverRoot = new ReceiverRoot();
    model.setSenderRoot(senderRoot);
    model.setReceiverRoot(receiverRoot);

    listA0 = createA("am0");
    listA1 = createA("am1");
    listA0InSuffix = createA("am0");
    listA1InSuffix = createA("am1");

    senderRoot.addMultipleA(listA0);
    senderRoot.addMultipleA(listA1);
    senderRoot.addMultipleAWithSuffix(listA0InSuffix);
    senderRoot.addMultipleAWithSuffix(listA1InSuffix);
  }

  private A createA(String value) {
    return new A().setValue(value).setInner(new Inner("inner" + value));
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException, InterruptedException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);
    handler = new MqttHandler().setHost(TestUtils.getMqttHost()).dontSendWelcomeMessage();
    assertTrue(handler.waitUntilReady(2, TimeUnit.SECONDS));

    data = new ReceiverData();
    assertTrue(handler.newConnection(TOPIC_A_MANY_NORMAL_WILDCARD, bytes -> data.numberOfValues += 1));
    assertTrue(handler.newConnection(TOPIC_A_MANY_SUFFIX_WILDCARD, bytes -> data.numberOfValues += 1));

    checker = new TestChecker();
    checker.setActualNumberOfValues(() -> data.numberOfValues)
            .setCheckForTuple(CHECK_MANY_A, (name, expected) ->
                    checkList(name, expected, receiverRoot::getManyAList))
            .setCheckForTuple(CHECK_WITH_SUFFIX, (name, expected) ->
                    checkList(name, expected, receiverRoot::getManyAWithSuffixList));

    // connect receive
    assertTrue(receiverRoot.connectManyA(mqttUri(TOPIC_A_MANY_NORMAL_WILDCARD)));
    assertTrue(receiverRoot.connectManyAWithSuffix(mqttUri(TOPIC_A_MANY_SUFFIX_WILDCARD)));

    // connect send, and wait to receive (if writeCurrentValue is set)
    assertConnectAOrMultipleA(TOPIC_A_MANY_NORMAL_0, 0);
    waitForValue(receiverRoot::getNumManyA, 1);

    assertConnectAOrMultipleA(TOPIC_A_MANY_NORMAL_1, 1);
    waitForValue(receiverRoot::getNumManyA, 2);

    assertConnectComputedAOrMultipleAWithSuffix(TOPIC_A_MANY_SUFFIX_0, 0);
    waitForValue(receiverRoot::getNumManyAWithSuffix, 1);

    assertConnectComputedAOrMultipleAWithSuffix(TOPIC_A_MANY_SUFFIX_1, 1);
    waitForValue(receiverRoot::getNumManyAWithSuffix, 2);
  }

  private void assertConnectAOrMultipleA(String topic, int index) throws IOException {
    assertTrue(connectNTAsInstead ?
            senderRoot.connectA(mqttUri(topic), index, isWriteCurrentValue()) :
            senderRoot.connectMultipleA(mqttUri(topic), index, isWriteCurrentValue()));
  }

  private void assertConnectComputedAOrMultipleAWithSuffix(String topic, int index) throws IOException {
    assertTrue(connectNTAsInstead ?
            senderRoot.connectComputedA(mqttUri(topic), index, isWriteCurrentValue()) :
            senderRoot.connectMultipleAWithSuffix(mqttUri(topic), index, isWriteCurrentValue()));
  }

  private void assertDisconnectComputedAOrMultipleAWithSuffix(String topic) throws IOException {
    assertTrue(connectNTAsInstead ?
            senderRoot.disconnectComputedA(mqttUri(topic)) :
            senderRoot.disconnectMultipleAWithSuffix(mqttUri(topic)));
  }

  private void checkList(String name, Tuple expected, Supplier<JastAddList<A>> actual) {
    Assertions.assertThat(actual.get()).extracting("Value")
            .as(name)
            .containsExactlyElementsOf(expected.toList());
  }

  private void waitForValue(Callable<Integer> callable, int expectedValue) {
    if (isWriteCurrentValue()) {
      awaitMqtt().until(callable, Predicate.isEqual(expectedValue));
    }
  }

  @Tag("mqtt")
  @RepeatedIfExceptionsTest(repeats = TEST_REPETITIONS)
  public void testCommunicateSendInitialValueWithNTAs() throws IOException, InterruptedException {
    this.writeCurrentValue = true;
    this.connectNTAsInstead = true;

    try {
      createModel();
      setupReceiverAndConnect();

      logger.info("Calling communicateSendInitialValue");
      communicateSendInitialValue();
    } finally {
      this.connectNTAsInstead = false;
    }
  }

  @Tag("mqtt")
  @RepeatedIfExceptionsTest(repeats = TEST_REPETITIONS)
  public void testCommunicateOnlyUpdatedValueWithNTAs() throws IOException, InterruptedException {
    this.writeCurrentValue = false;
    this.connectNTAsInstead = true;

    try {
      createModel();
      setupReceiverAndConnect();

      logger.info("Calling communicateOnlyUpdatedValue");
      communicateOnlyUpdatedValue();
    } finally {
      this.connectNTAsInstead = false;
    }
  }

  @Override
  protected void communicateSendInitialValue() throws IOException, InterruptedException {
    checker.addToNumberOfValues(4)
            .put(CHECK_MANY_A, tuple("am0", "am1"))
            .put(CHECK_WITH_SUFFIX, tuple("am0post", "am1post"));

    communicateBoth("am1", "am0post");
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws IOException, InterruptedException {
    checker.put(CHECK_MANY_A, tuple())
            .put(CHECK_WITH_SUFFIX, tuple());

    communicateBoth(null, null);
  }

  private void communicateBoth(String manyAtIndex1, String suffixAtIndex0) throws IOException {
    // Sink.ManyA           <-- Root.MultipleA
    // Sink.ManyAWithSuffix <-- Root.MultipleAWithSuffix
    checker.check();

    assertEquals(listA0.getValue(), senderRoot._ragconnect_MultipleA(0).getValue());
    listA0.setValue("changedValue");
    assertEquals(listA0.getValue(), senderRoot._ragconnect_MultipleA(0).getValue());

    checker.incNumberOfValues()
            .put(CHECK_MANY_A, manyAtIndex1 != null ? tuple("changedValue", manyAtIndex1) : tuple("changedValue"))
            .check();

    // setting same value must not change data, and must not trigger a new sent message
    listA0.setValue("changedValue");
    checker.check();

    listA1.setValue("");
    checker.incNumberOfValues().put(CHECK_MANY_A, tuple("changedValue", "")).check();

    // first element in suffix-list
    listA1InSuffix.setValue("re");
    checker.incNumberOfValues()
            .put(CHECK_WITH_SUFFIX, suffixAtIndex0 != null ? tuple(suffixAtIndex0, "repost") : tuple("repost"))
            .check();

    // adding a new element does not automatically send it
    A listA2InSuffix = createA("out");
    senderRoot.addMultipleAWithSuffix(listA2InSuffix);
    checker.check();

    // only after connecting it, the element gets sent (for SendInitialValue case)
    assertConnectComputedAOrMultipleAWithSuffix(TOPIC_A_MANY_SUFFIX_2, 2);
    if (isWriteCurrentValue()) {
      checker.incNumberOfValues()
              .put(CHECK_WITH_SUFFIX, suffixAtIndex0 != null ? tuple(suffixAtIndex0, "repost", "outpost") : tuple("repost", "outpost"));
    }
    checker.check();

    // changing the value of the newly added element will send it
    listA2InSuffix.setValue("goal");
    checker.incNumberOfValues()
            .put(CHECK_WITH_SUFFIX, suffixAtIndex0 != null ? tuple(suffixAtIndex0, "repost", "goalpost") : tuple("repost", "goalpost"));
    checker.check();

    // after successful disconnect for index 0, no messages will be sent
    assertDisconnectComputedAOrMultipleAWithSuffix(TOPIC_A_MANY_SUFFIX_0);
    listA0InSuffix.setValue("willBeIgnored");
    checker.check();

    // for index 1 (not disconnected), messages will be sent still
    listA1InSuffix.setValue("sign");
    checker.incNumberOfValues()
            .put(CHECK_WITH_SUFFIX, suffixAtIndex0 != null ? tuple(suffixAtIndex0, "signpost", "goalpost") : tuple("signpost", "goalpost"))
            .check();

    // after successful disconnect for index 1, no messages will be sent anymore
    assertDisconnectComputedAOrMultipleAWithSuffix(TOPIC_A_MANY_SUFFIX_1);
    listA1InSuffix.setValue("willBeIgnored");
    checker.check();
  }

  @Override
  protected void closeConnections() {
    if (handler != null) {
      handler.close();
    }
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

  private static class ReceiverData {
    int numberOfValues = 0;
  }
}
