package org.jastadd.ragconnect.tests;

import defaultOnlyRead.ast.A;
import defaultOnlyRead.ast.BoxedTypes;
import defaultOnlyRead.ast.NativeTypes;
import org.jastadd.ragconnect.tests.utils.DefaultMappings;
import org.jastadd.ragconnect.tests.utils.TestUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static org.jastadd.ragconnect.tests.utils.TestUtils.mqttUri;
import static org.jastadd.ragconnect.tests.utils.TestUtils.testJaddContainReferenceToJackson;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test case "defaultOnlyRead".
 *
 * @author rschoene - Initial contribution
 */
public class DefaultOnlyReadTest extends AbstractMqttTest {

  private static final String TOPIC_NATIVE_INT = "native/boolean";
  private static final String TOPIC_NATIVE_BOOLEAN = "native/int";
  private static final String TOPIC_NATIVE_SHORT = "native/short";
  private static final String TOPIC_NATIVE_LONG = "native/long";
  private static final String TOPIC_NATIVE_FLOAT = "native/float";
  private static final String TOPIC_NATIVE_DOUBLE = "native/double";
  private static final String TOPIC_NATIVE_CHAR = "native/char";
  private static final String TOPIC_NATIVE_STRING = "native/string";

  private static final String TOPIC_BOXED_BOOLEAN = "boxed/boolean";
  private static final String TOPIC_BOXED_INTEGER = "boxed/Integer";
  private static final String TOPIC_BOXED_SHORT = "boxed/Short";
  private static final String TOPIC_BOXED_LONG = "boxed/Long";
  private static final String TOPIC_BOXED_FLOAT = "boxed/Float";
  private static final String TOPIC_BOXED_DOUBLE = "boxed/Double";
  private static final String TOPIC_BOXED_CHARACTER = "boxed/Character";

  private A model;
  private NativeTypes integers;
  private NativeTypes floats;
  private NativeTypes chars;
  private BoxedTypes allBoxed;

  @Test
  public void checkNotJacksonReference() {
    testJaddContainReferenceToJackson(
        Paths.get("src", "test",
            "02-after-ragconnect", "defaultOnlyRead", "RagConnect.jadd"), false);
  }

  @Override
  protected void createModel() {
    model = new A();
    integers = new NativeTypes();
    model.addNativeTypes(integers);
    floats = new NativeTypes();
    model.addNativeTypes(floats);
    chars = new NativeTypes();
    model.addNativeTypes(chars);
    allBoxed = new BoxedTypes();
    model.addBoxedTypes(allBoxed);
  }

  @Override
  protected void setupReceiverAndConnect() throws IOException {
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    assertTrue(integers.connectBooleanValue(mqttUri(TOPIC_NATIVE_BOOLEAN)));
    assertTrue(integers.connectIntValue(mqttUri(TOPIC_NATIVE_INT)));
    assertTrue(integers.connectShortValue(mqttUri(TOPIC_NATIVE_SHORT)));
    assertTrue(integers.connectLongValue(mqttUri(TOPIC_NATIVE_LONG)));
    assertTrue(floats.connectFloatValue(mqttUri(TOPIC_NATIVE_FLOAT)));
    assertTrue(floats.connectDoubleValue(mqttUri(TOPIC_NATIVE_DOUBLE)));
    assertTrue(chars.connectCharValue(mqttUri(TOPIC_NATIVE_CHAR)));
    assertTrue(chars.connectStringValue(mqttUri(TOPIC_NATIVE_STRING)));

    assertTrue(integers.connectBooleanValueTransformed(mqttUri(TOPIC_NATIVE_BOOLEAN)));
    assertTrue(integers.connectIntValueTransformed(mqttUri(TOPIC_NATIVE_INT)));
    assertTrue(integers.connectShortValueTransformed(mqttUri(TOPIC_NATIVE_SHORT)));
    assertTrue(integers.connectLongValueTransformed(mqttUri(TOPIC_NATIVE_LONG)));
    assertTrue(floats.connectFloatValueTransformed(mqttUri(TOPIC_NATIVE_FLOAT)));
    assertTrue(floats.connectDoubleValueTransformed(mqttUri(TOPIC_NATIVE_DOUBLE)));
    assertTrue(chars.connectCharValueTransformed(mqttUri(TOPIC_NATIVE_CHAR)));
    assertTrue(chars.connectStringValueTransformed(mqttUri(TOPIC_NATIVE_STRING)));

    assertTrue(allBoxed.connectBooleanValue(mqttUri(TOPIC_BOXED_BOOLEAN)));
    assertTrue(allBoxed.connectIntValue(mqttUri(TOPIC_BOXED_INTEGER)));
    assertTrue(allBoxed.connectShortValue(mqttUri(TOPIC_BOXED_SHORT)));
    assertTrue(allBoxed.connectLongValue(mqttUri(TOPIC_BOXED_LONG)));
    assertTrue(allBoxed.connectFloatValue(mqttUri(TOPIC_BOXED_FLOAT)));
    assertTrue(allBoxed.connectDoubleValue(mqttUri(TOPIC_BOXED_DOUBLE)));
    assertTrue(allBoxed.connectCharValue(mqttUri(TOPIC_BOXED_CHARACTER)));

    assertTrue(allBoxed.connectBooleanValueTransformed(mqttUri(TOPIC_BOXED_BOOLEAN)));
    assertTrue(allBoxed.connectIntValueTransformed(mqttUri(TOPIC_BOXED_INTEGER)));
    assertTrue(allBoxed.connectShortValueTransformed(mqttUri(TOPIC_BOXED_SHORT)));
    assertTrue(allBoxed.connectLongValueTransformed(mqttUri(TOPIC_BOXED_LONG)));
    assertTrue(allBoxed.connectFloatValueTransformed(mqttUri(TOPIC_BOXED_FLOAT)));
    assertTrue(allBoxed.connectDoubleValueTransformed(mqttUri(TOPIC_BOXED_DOUBLE)));
    assertTrue(allBoxed.connectCharValueTransformed(mqttUri(TOPIC_BOXED_CHARACTER)));
  }

  @Override
  protected void communicateSendInitialValue() {
    // empty
  }

  @Override
  protected void communicateOnlyUpdatedValue() throws InterruptedException {
    final boolean expectedBooleanValue = true;
    final int expectedIntValue = 1;
    final short expectedShortValue = 2;
    final long expectedLongValue = 3L;
    final float expectedFloatValue = 4.1f;
    final double expectedDoubleValue = 5.2;
    final char expectedCharValue = 'c';
    final String expectedStringValue = "6.3";

    publisher.publish(TOPIC_NATIVE_BOOLEAN, DefaultMappings.BoolToBytes(expectedBooleanValue));
    publisher.publish(TOPIC_NATIVE_INT, DefaultMappings.IntToBytes(expectedIntValue));
    publisher.publish(TOPIC_NATIVE_SHORT, DefaultMappings.ShortToBytes(expectedShortValue));
    publisher.publish(TOPIC_NATIVE_LONG, DefaultMappings.LongToBytes(expectedLongValue));
    publisher.publish(TOPIC_NATIVE_FLOAT, DefaultMappings.FloatToBytes(expectedFloatValue));
    publisher.publish(TOPIC_NATIVE_DOUBLE, DefaultMappings.DoubleToBytes(expectedDoubleValue));
    publisher.publish(TOPIC_NATIVE_CHAR, DefaultMappings.CharToBytes(expectedCharValue));
    publisher.publish(TOPIC_NATIVE_STRING, DefaultMappings.StringToBytes(expectedStringValue));

    publisher.publish(TOPIC_BOXED_BOOLEAN, DefaultMappings.BoolToBytes(expectedBooleanValue));
    publisher.publish(TOPIC_BOXED_INTEGER, DefaultMappings.IntToBytes(expectedIntValue));
    publisher.publish(TOPIC_BOXED_SHORT, DefaultMappings.ShortToBytes(expectedShortValue));
    publisher.publish(TOPIC_BOXED_LONG, DefaultMappings.LongToBytes(expectedLongValue));
    publisher.publish(TOPIC_BOXED_FLOAT, DefaultMappings.FloatToBytes(expectedFloatValue));
    publisher.publish(TOPIC_BOXED_DOUBLE, DefaultMappings.DoubleToBytes(expectedDoubleValue));
    publisher.publish(TOPIC_BOXED_CHARACTER, DefaultMappings.CharToBytes(expectedCharValue));

    TestUtils.waitForMqtt();

    assertEquals(expectedBooleanValue, integers.getBooleanValue());
    assertEquals(expectedIntValue, integers.getIntValue());
    assertEquals(expectedShortValue, integers.getShortValue());
    assertEquals(expectedLongValue, integers.getLongValue());
    assertEquals(expectedFloatValue, floats.getFloatValue(), TestUtils.DELTA);
    assertEquals(expectedDoubleValue, floats.getDoubleValue(), TestUtils.DELTA);
    assertEquals(expectedCharValue, chars.getCharValue());
    assertEquals(expectedStringValue, chars.getStringValue());

    assertEquals(expectedBooleanValue, integers.getBooleanValueTransformed());
    assertEquals(expectedIntValue, integers.getIntValueTransformed());
    assertEquals(expectedShortValue, integers.getShortValueTransformed());
    assertEquals(expectedLongValue, integers.getLongValueTransformed());
    assertEquals(expectedFloatValue, floats.getFloatValueTransformed(), TestUtils.DELTA);
    assertEquals(expectedDoubleValue, floats.getDoubleValueTransformed(), TestUtils.DELTA);
    assertEquals(expectedCharValue, chars.getCharValueTransformed());
    assertEquals(expectedStringValue, chars.getStringValueTransformed());

    assertEquals(expectedBooleanValue, allBoxed.getBooleanValue());
    assertEquals(expectedIntValue, allBoxed.getIntValue().intValue());
    assertEquals(expectedShortValue, allBoxed.getShortValue().shortValue());
    assertEquals(expectedLongValue, allBoxed.getLongValue().longValue());
    assertEquals(expectedFloatValue, allBoxed.getFloatValue(), TestUtils.DELTA);
    assertEquals(expectedDoubleValue, allBoxed.getDoubleValue(), TestUtils.DELTA);
    assertEquals(expectedCharValue, allBoxed.getCharValue().charValue());

    assertEquals(expectedBooleanValue, allBoxed.getBooleanValueTransformed());
    assertEquals(expectedIntValue, allBoxed.getIntValueTransformed().intValue());
    assertEquals(expectedShortValue, allBoxed.getShortValueTransformed().shortValue());
    assertEquals(expectedLongValue, allBoxed.getLongValueTransformed().longValue());
    assertEquals(expectedFloatValue, allBoxed.getFloatValueTransformed(), TestUtils.DELTA);
    assertEquals(expectedDoubleValue, allBoxed.getDoubleValueTransformed(), TestUtils.DELTA);
    assertEquals(expectedCharValue, allBoxed.getCharValueTransformed().charValue());
  }

  @Override
  public void closeConnections() {
    if (model != null) {
      model.ragconnectCloseConnections();
    }
  }

}
