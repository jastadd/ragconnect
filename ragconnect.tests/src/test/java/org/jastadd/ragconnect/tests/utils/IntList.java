package org.jastadd.ragconnect.tests.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * TODO: Add description.
 *
 * @author rschoene - Initial contribution
 */
public class IntList {
  private final List<Integer> integers;

  public IntList(Integer... values) {
    integers = Arrays.stream(values).filter(Objects::nonNull).collect(Collectors.toList());
  }

  public List<Integer> toList() {
    return integers;
  }

  public List<Integer> toAbsList() {
    return integers.stream().map(Math::abs).collect(Collectors.toList());
  }

  public static IntList list(Integer... values) {
    return new IntList(values);
  }
}
