# Forwarding

Idea: Use send definitions targeting only non-NTAs (thus, requiring the creation of implicit NTAs).
Also test context-free port definitions targeting non-NTAs to test compatibility of those two features.

Note: When a type occurs in a list, the context-free port definition will force an (implicit) indexed send for that context.
