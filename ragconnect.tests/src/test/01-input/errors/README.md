Ideas for errors:

- Read-Update
    - the token must be resolvable within the parent type
    - the Token must not be a TokenNTA (i.e., check for `!Token.getNTA()`)
    - type of first mapping must be `byte[]`
    - type of last mapping must be type of the Token
    - types of mappings must match (modulo inheritance)
- Write-Update
    - the token must be resolvable within the parent type
    - Token must be a TokenNTA (i.e., check for `Token.getNTA()`)
    - type of first mapping must be type of Token
    - type of last mapping must be `byte[]`
    - types of mappings must match (modulo inheritance)
    - no more than one write mapping for each TokenComponent
- for all type checks, there are three cases regarding the two types to check against:
    1) both are nonterminal types, check with grammar
    2) both are known classes, check with `Class.forName()` and subclass-checking-methods
    3) otherwise issue warning, that types could not be matched
- dependency-definition
    - There **must be** a write update definition for the target token
        - Otherwise there are missing update and write methods used in the virtual setter
    - Both, source and target must be resolvable within the parent type
    - The name of a dependency definition must not be equal to a list-node on the source
    - There must not be two dependency definitions with the same name
