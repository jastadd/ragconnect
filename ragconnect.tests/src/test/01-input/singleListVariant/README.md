# Single List

Idea: send and receive single values for lists of subtrees.
Test different variants of the structure/shape of the send/received value.

## Execution-Model

TODO: check again (old model copied from `singleList`)

```
SenderRoot/ReceiverRoot
  |- T_Empty       ::= /* empty */ ;
  |- T_Token       ::= <Value:String> ;
  |- T_OneChild    ::= Other ;
  |- T_OneOpt      ::= [Other] ;
  |- T_OneList     ::= Other* ;
  |- T_TwoChildren ::= Left:Other Right:Other ;
  |- T_OneOfEach   ::= First:Other [Second:Other] Third:Other* <Fourth:String> ;
  |- abstract T_Abstract ::= <ValueAbstract> ;
```

## Computation

```
T.ID = Input
T.token = Input
T.Other.ID = Input + 1
```

## Execution-Trace (SendInitialValue)

Inputs:

- 1
- 1
- 2
- 3

| Input | [A1,A2,A3,A4,IO] | # | A* | UsingWcA | WithAddA | UsingWcWithAddA:A |
|---|---|---|---|---|---|---|
| * | [1,2,3,4,0] | 5 | [1,2,3,4,0] | [1,2,3,4,0] | [1,2,3,4,0] | [1,2,3,4,0] |
| I1:1 | [2,2,3,4,0] | 6 | [2,2,3,4,0] | [2,2,3,4,0] | [1,2,3,4,0,2] | [1,2,3,4,0,2] |
| I1:1 | [2,2,3,4,0] | 6 | [2,2,3,4,0] | [2,2,3,4,0] | [1,2,3,4,0,2] | [1,2,3,4,0,2] |
| I1:2 | [3,2,3,4,0] | 7 | [3,2,3,4,0] | [3,2,3,4,0] | [1,2,3,4,0,2,3] | [1,2,3,4,0,2,3] |
| IO:5 | [3,2,3,4,5] | 8 | [3,2,3,4,5] | [3,2,3,4,5] | [1,2,3,4,0,2,3,5] | [1,2,3,4,0,2,3,5]
| I3:4 | [3,2,7,4,5] | 9 | [3,2,7,4,5] | [3,2,7,4,5] | [1,2,3,4,0,2,3,5,7] | [1,2,3,4,0,2,3,5,7] |

*: (1:0, 2:0, 3:0, 4:0, 5:0)

## Execution-Trace (OnlyUpdate)

| Input | [A1,A2,A3,A4,IO] | # | A* | UsingWcA | WithAddA | UsingWcWithAddA:A |
|---|---|---|---|---|---|---|
| * | [-,-,-,-,-] | 0 | [0,0,0,0,0] | [] | [] | [] |
| I1:1 | [2,-,-,-,-] | 1 | [2,0,0,0,0] | [2] | [2] | [2] |
| I1:1 | [2,-,-,-,-] | 1 | [2,0,0,0,0] | [2] | [2] | [2] |
| I1:2 | [3,-,-,-,-] | 2 | [3,0,0,0,0] | [3] | [2,3] | [2,3] |
| IO:5 | [2,-,-,-,5] | 3 | [3,0,0,0,5] | [3,5] | [2,3,5] | [2,3,5]
| I3:4 | [2,-,7,-,5] | 4 | [3,0,7,0,5] | [3,5,7] | [2,3,5,7] | [2,3,5,7] |

*: (1:0, 2:0, 3:0, 4:0, 5:0)
