# Indexed Send

Idea: Use send definitions on (parts of) a list to send only some of its elements, similar to `receive indexed`.
