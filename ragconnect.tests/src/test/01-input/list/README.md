# List

Idea: send and receive lists of subtrees.
Once without incremental evaluation (i.e., using manual dependencies), and the other time with incremental evaluation

## Execution-Model

```
SenderRoot                                    ReceiverRoot
|- A* ---( mqtt: a ) ---+------> A* --------------------|
|                       \------> WidthAddFromA:A* ------|   
|- SingleA:A*                ,-> FromSingleA:A* --------|
     \---( mqtt: single-a ) -+-> WithAddFromSingleA:A* -|
```

## Execution-Trace (SendInitialValue)

| Input | # | A* | WidthAddFromA | FromSingleA | WithAddFromSingleA:A |
|---|---|---|---|---|---|
| 0 | 1 | [] | [0] | [] | [0] |
| 1 | 2 | [1] | [1] | [1] | [0,1] |
| 1 | 2 | [1] | [1] | [1] | [0,1] |
| 2 | 3 | [1,2] | [2] | [1,1,2] | [0,1,2] |
| 3 | 4 | [1,2,3] | [3] | [1,1,2,1,2,3] | [0,1,2,3] |

## Execution-Trace (OnlyUpdate)

| Input | # | A* | WidthAddFromA | FromSingleA | WithAddFromSingleA:A |
|---|---|---|---|---|---|
| - | 0 | [] | [] | [] | [] |
| 1 | 1 | [1] | [1] | [1] | [1] |
| 1 | 1 | [1] | [1] | [1] | [1] |
| 2 | 2 | [1,2] | [2] | [1,1,2] | [1,2] |
| 3 | 3 | [1,2,3] | [3] | [1,1,2,1,2,3] | [1,2,3] |
