mkdocs==1.4.2
mkdocs-git-revision-date-localized-plugin==1.1.0
mkdocs-macros-plugin==0.7.0
mkdocs-material==8.5.10
mkdocs-material-extensions==1.1
Jinja2==3.1.2
MarkupSafe==2.1.1
