import os

ragconnectVersionFileName = '../ragconnect.base/src/main/resources/ragconnectVersion.properties'


def get_version():
    if os.environ.get('CI_COMMIT_BRANCH', 'unknown') == 'dev':
        return 'dev'
    with open(ragconnectVersionFileName) as ragconnectVersionFile:
        versionFileContent = ragconnectVersionFile.read()
    return versionFileContent[versionFileContent.rindex('version=') + 8:].strip()


def define_env(env):
    """
    This is the hook for defining variables, macros and filters

    - variables: the dictionary that contains the environment variables
    - macro: a decorator function, to declare a macro.
    """
    env.conf['site_name'] = 'RagConnect ' + get_version()

    @env.macro
    def ragconnect_version():
        return get_version()


if __name__ == '__main__':
    print(get_version())
