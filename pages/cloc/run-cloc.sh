#!/bin/bash
if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  echo "-s -> skip running cloc commands"
  echo "-a -> produce files.csv with code count for single src files"
  exit
fi
if [ "$1" != "-s" ]; then
  #  --force-lang=Java,jrag --force-lang=Java,jadd
  DEF_FILE=my_definitions.txt
  echo "Export language definitions"
  cloc --quiet --write-lang-def="$DEF_FILE"
  for f in cloc-def-*.txt;
  do
  	cat $f >> "$DEF_FILE"
  done
  REPO_ROOT="../.."
  CLOC_CMD="cloc --exclude-lang=JSON --read-lang-def=my_definitions.txt --exclude-list-file=.clocignore --quiet --hide-rate"
  #  --ignored=bad-files.txt
  cloc_double() {
    f=$1
    shift
    echo "Creating $f.txt"
    $CLOC_CMD --report-file="$f.txt" $@
    echo "Creating $f.md"
    $CLOC_CMD --md --report-file=tmp.md $@
    tail -n+3 tmp.md > "$f.md"
    rm tmp.md
  }
  make_page() {
    echo "# Evaluation Metrics: Lines of Code"
    echo
    echo "## Manually written generator code"
    echo
    cat ragconnect.base-src-result.md
    echo
    echo "## Generated generator code"
    cat ragconnect.base-gen-result.md
    echo
    echo "## Manually written test code"
    cat ragconnect.tests-src-result.md
    echo
    echo "## Generated test code"
    cat ragconnect.tests-gen-result.md
  }
fi
if [ "$1" != "-s" ] && [ "$1" != "-a" ]; then
  echo "Running cloc with new definitions"
  cloc_double "ragconnect.base-src-result" --found=ragconnect.base-src-found.txt ${REPO_ROOT}/ragconnect.base/src/main/
  cloc_double "ragconnect.base-gen-result" --found=ragconnect.base-gen-found.txt ${REPO_ROOT}/ragconnect.base/src/gen/jastadd ${REPO_ROOT}/ragconnect.base/src/gen/java
  cloc_double "ragconnect.tests-src-result" --found=ragconnect.tests-src-found.txt ${REPO_ROOT}/ragconnect.tests/src/test/01-input/ ${REPO_ROOT}/ragconnect.tests/src/test/java/
  cloc_double "ragconnect.tests-gen-result" --found=ragconnect.tests-gen-found.txt ${REPO_ROOT}/ragconnect.tests/src/test/02-after-ragconnect/ ${REPO_ROOT}/ragconnect.tests/src/test/java-gen
  $CLOC_CMD --sum-reports --report_file=ragconnect ragconnect.base-src-result.txt ragconnect.base-gen-result.txt ragconnect.tests-src-result.txt ragconnect.tests-gen-result.txt
  echo "Creating ../docs/cloc.md"
  make_page > ../docs/cloc.md
fi
if [ "$1" == "-a" ]; then
  echo "filename,code" > files.csv
  for f in $(find ${REPO_ROOT}/ragconnect.base/src/main/ ${REPO_ROOT}/ragconnect.base/src/gen/jastadd-sources/ -type f); do
    printf '.'
    echo $f,$($CLOC_CMD --json $f | jq '.SUM.code') >> files.csv
  done
  echo
  exit
fi


# cat ragconnect.base-src-result.txt
# cat ragconnect.base.file
# cat ragconnect.tests-result.txt

echo "LOC stats:"
echo "Language                     files          blank        comment           code"
( for t in *-result.txt ; do echo -e "==> $t <=="; grep -v -e '---' -e 'SUM' -e 'Language' -e 'github' $t; done)

echo 
echo "Summary:"
grep -v -e '---' -e 'SUM' -e 'Language' -e 'github' ragconnect.file
