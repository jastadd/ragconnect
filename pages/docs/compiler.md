# Compiler options

The compiler is JastAdd-compliant, i.e., it accepts all flags available for JastAdd, though there is no process how to chain pre-processors _yet_.
Additional options are as follows.

## Table with available options

| Name                         | Required (Default) | Description                                                                                                                                                                           |
|------------------------------|--------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `--rootNode`                 | Yes                | Root node in the base grammar.                                                                                                                                                        |
| `--protocols`                | No (`mqtt`)        | Protocols to enable, one of `java` (experimental), `mqtt`, `rest`, see [Handlers][handlers] for details.                                                                              |
| `--printYaml`                | No (false)         | Print out YAML instead of generating files.                                                                                                                                           |
| `--verbose`                  | No (false)         | Print more messages while compiling.                                                                                                                                                  |
| `--logReads`                 | No (false)         | Enable logging for every received message.                                                                                                                                            |
| `--logWrites`                | No (false)         | Enable logging for every sent message.                                                                                                                                                |
| `--logIncremental`           | No (false)         | Enable logging for observer in incremental dependency tracking.                                                                                                                       |
| `--logTarget`                | No (`console`)     | Logging target to use, currently available: `console, slf4j`.                                                                                                                         |
| `--experimental-jastadd-329` | No (false)         | Use tracing events `INC_FLUSH_START` and `INC_FLUSH_END` ([JastAdd issue #329][jastadd-issue-329]), see [section about automatic dependency tracking][automatic-dependency-tracking]. |
| `--incremental`              | No (false)         | Enables incremental dependency tracking (if `tracing` is also set appropriately).                                                                                                     |
| `--tracing[=flush]`          | No (false)         | Enables incremental dependency tracking (if `incremental` is also set appropriately).                                                                                                 |
| `--version`                  | No (false)         | Print version info and exit (reused JastAdd option)                                                                                                                                   |
| `--o`                        | No (`.`)           | Output directory (reused JastAdd option)                                                                                                                                              |

All files to be processed have to be passed as arguments.
Their type is deduced by the file extension (`ast` and `relast` for input grammars, `connect` and `ragconnect` for RagConnect definitions file).

## Additional software dependencies

Using RagConnect itself does not introduce dependencies.
However, depending on the selected protocols and/or used features, additional dependencies are required when using the generated code.

### Used features

#### Automatic dependency tracking

- Condition: When passing `--incremental` and `--tracing=flush` to RagConnect
- Required runtime dependencies: _none_
- Required options for RelAST compiler: _none_
- Required options for JastAdd:
    - `--incremental=param` (enable incremental evaluation)
    - `--tracing=flush` (enable tracing of events)
    - `--cache=all` (set all attributes to be cached)
- Remarks:
    - Other (additional) values passed to those two options must be equal (e.g., `--incremental=param` passed to RagConnect must be also passed to JastAdd)
    - Other values besides `flush` can be added to `--tracing`
    - [Feature description](using.md#dependency-tracking-automatically-derived)

#### (Safer) Automatic dependency tracking

- Condition: When passing `--experimental-jastadd-329` to RagConnect
- Required runtime dependencies: _none_
- Required options for RelAST compiler: _none_
- Required options for JastAdd: _none_
- Remarks:
    - JastAdd version has to support `INC_FLUSH_START` and `INC_FLUSH_END` (i.e., has [issue #329][jastadd-issue-329] resolved)
    - [Feature description](using.md#dependency-tracking-automatically-derived)

#### Tree/List Ports

- Condition: When using ports along with default mappings for subtrees
- Required runtime dependencies:
    - `group: 'com.fasterxml.jackson.core', name: 'jackson-core', version: '2.12.1'`
    - `group: 'com.fasterxml.jackson.core', name: 'jackson-databind', version: '2.12.1'`
- Required options for RelAST compiler:
    - `--serializer=jackson`
- Required options for JastAdd: _none_
- Remarks:
    - [Feature description](using.md#an-advanced-example)

#### Logging Target SLF4J

- Condition: When passing `--logTarget=slf4j` to RagConnect
- Required runtime dependencies:
    - `group: 'org.slf4j', name: 'slf4j-api', version: '1.7.0'`
- Required options for RelAST compiler: _none_
- Required options for JastAdd: _none_
- Remarks:
    - Additionally, a slf4j binding is required, see [the slf4j user manual][slf4j]

[jastadd-issue-329]: https://bitbucket.org/jastadd/jastadd2/issues/329/add-event-for-completion-of-flush
[automatic-dependency-tracking]: using.md#dependency-tracking-automatically-derived
[handlers]: handlers.md
