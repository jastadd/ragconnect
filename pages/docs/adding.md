# Adding `RagConnect` to your project

If you want to use `RagConnect`, either use the latest [pre-build version](#use-packaged-version) or clone the repository and [build it yourself](#build-from-source).
Either way, a task for [compiling RelAst specifications](#compiling-relast-specifications) has to be specified.

## Use packaged version

Check the [package overview page](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/packages) to find the latest version.

To use it, three steps are needed. First add this GitLab as a repository in your `build.gradle`:

```
repositories {
    maven {
        name "gitlab-maven"
        url "https://git-st.inf.tu-dresden.de/api/v4/groups/jastadd/-/packages/maven"
    }
}
```

Next, add `RagConnect` as a dependency:

```
configurations {
    ragconnectClasspath
}

dependencies {
    ragconnectClasspath group: 'de.tudresden.inf.st', name: 'ragconnect', version: '{{ragconnect_version()}}'
}
```

Finally, add a task to compile your specification:

```
task ragConnect(type: JavaExec) {
    group = 'Build'
    main = 'org.jastadd.ragconnect.compiler.Compiler'
    classpath = configurations.ragconnectClasspath

    args([
            '--verbose',
            '--o=src/gen/jastadd',
            'src/main/jastadd/GoalModel.relast',
            'src/main/jastadd/GoalModel.connect',
            '--rootNode=GoalModel'
    ])
}
```

You might need to add another task for [compiling relast specifications](#compiling-relast-specifications).

## Build from source

If you want to plan to extend `RagConnect`, the suggested way is to first build the jar from the [RagConnect repository](https://git-st.inf.tu-dresden.de/jastadd/ragconnect) (if you only want to _use_ it, consider using [the packaged version](#use-packaged-version)).

```bash
git clone https://git-st.inf.tu-dresden.de/jastadd/ragconnect.git
cd ragconnect
./gradlew jar
ls ragconnect.base/build/libs/
```

This `ragconnect-{{ragconnect_version()}}.jar` can then be copied to your project.
Please note, that you can safely use `ragconnect.jar` as filename, because the version can always be printed using `java -jar path/to/ragconnect.jar --version`.

```bash
cp ragconnect.base/build/libs/ragconnect-{{ragconnect_version()}}.jar ../your-project/libs/ragconnect.jar
cd ../your-project/
```

Finally, this jar has to be integrated into your build process.
In case, [Gradle](https://gradle.org/) is used, a task could look like the following (example taken from the [ros2rag use case](https://git-st.inf.tu-dresden.de/jastadd/ros2rag)).
The path to the jar file may need to be changed according to your project structure.

```groovy
task ragConnect(type: JavaExec) {
    group = 'Build'
    main = '-jar'

    args([
            '../libs/ragconnect.jar',
            '--verbose',
            '--o=src/gen/jastadd',
            'src/main/jastadd/GoalModel.relast',
            'src/main/jastadd/GoalModel.connect',
            '--rootNode=GoalModel'
    ])
}
```

You might need to add another task for [compiling relast specifications](#compiling-relast-specifications).

## Compiling RelAst specifications

The task to compile `RagConnect` specifications is typically accompanied by a task to invoke the [RelAst compiler](http://relational-rags.eu/) and the [JastAdd gradle plugin](https://plugins.gradle.org/plugin/org.jastadd).
Currently, the parameter `--useJastAddNames` is **required**, and it may cause incompatibilities if not set.
The additional arguments `--listClass`, `--jastAddList` and `--resolverHelper` to relast are not required.
Please see the user manual of the RelAst compiler for more information.

```groovy
task relastToJastAdd(type: JavaExec) {
    group = 'Build'
    main = "-jar"

    args(["../libs/relast.jar",
            "--grammarName=./src/gen/jastadd/model",
            "--useJastAddNames",
            "--listClass=java.util.ArrayList",
            "--jastAddList=JastAddList",
            "--resolverHelper",
            "--file",
            "src/gen/jastadd/GoalModel.relast",
            "src/gen/jastadd/RagConnect.relast"])
}

jastadd {
...
}
```

One also has to specify the dependencies to get correct ordering of tasks.

```groovy
generateAst.dependsOn relastToJastAdd
relastToJastAdd.dependsOn ragConnect
```
