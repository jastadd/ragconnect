# Use cases with `RagConnect`

## MODELS Paper (2022) - Codename 'Ros3Rag'

In the paper [*"Incremental Causal Connection for Self-Adaptive Systems Based on Relational Reference Attribute Grammars"*](https://doi.org/10.1145/3550355.3552460), a [previous use case](#mpm4cps-paper-2020-codename-ros2rag) was extended to Collaborative, Teaching-Based Robotic Cells.

![Screenshot](img/2022_models/photo.jpg)

This paper was presented on October, 26th 2022 in the technical track of the [MODELS 2022 conference](https://conf.researchr.org/home/models-2022) (Oct 23 - Oct 29).
There is an [artifact hosted at Zenodo](https://doi.org/10.5281/zenodo.7009758) containing all source code and the executable case study.
For more information, see the [presented slides](img/2022_models/slides.pdf) or the accompanied [poster](img/2022_models/poster.pdf).

The repository with the used source code can be found at: <https://git-st.inf.tu-dresden.de/ceti/ros/models2022>

## ACSOS Paper (2022) - Codename 'Motion Grammar Demo'

In the paper [*"Specifying Reactive Robotic Applications
With Reference Attribute Motion Grammars"*](http://mg.relational-rags.eu), motion grammars (an older approach by [Dantham and Stilman](https://doi.org/10.1109/TRO.2013.2239553)) were implemented using Relational RAGs.

![Used Architecture](img/2022_acsos/architecture.png)

This paper was presented on September, 21st 2022 during the [Posters and Demos session](https://2022.acsos.org/track/acsos-2022-posters-and-demos) of the [ACSOS 2022 conference](https://2022.acsos.org/).
For more information, see <http://mg.relational-rags.eu/>.

## MPM4CPS Paper (2020) - Codename 'Ros2Rag'

In the publication [*"Connecting conceptual models using Relational Reference Attribute Grammars"*](https://doi.org/10.1145/3417990.3421437), a use case involving a simulated robot arm and two different models connected to it was shown.
One model was used to ensure a low speed of the robot when within a safety zone (purple boxes in the picture below), and the other model executes a workflow to control the robot.

![Screenshot of Gazebo](img/2020_mpm4cps/robo3d.png)

This paper was presented on October, 16th 2020 during the [MPM4CPS workshop](https://msdl.uantwerpen.be/conferences/MPM4CPS/2020/) within the [MODELS 2020 conference](https://conf.researchr.org/home/models-2020). For more information, see the [presented slides](img/2020_mpm4cps/slides.pdf), [a recording of the session](https://youtu.be/Hgc1qFfmr44?t=1220) or the accompanied [poster](img/2020_mpm4cps/poster.pdf).

The repository with the used source code can be found at: <https://git-st.inf.tu-dresden.de/ceti/ros/mpm4cps2020>
The usage is dockerized, so starting the application only involves the commands listed below.
As ROS takes some time to start up, it is best to use separate terminals for the three applications (ROS, Safety-Model, Goal-Model):

```bash
# Preparation (only need once)
./prepare-docker-compose.sh
docker-compose up -d mosquitto  # Starts the MQTT broker

# Terminal 1: ROS
docker-compose up --build ros
# Wait until you see no more new logging output

# Terminal 2: Safety-Model
docker-compose up rag_app

# Terminal 3: Goal-Model
docker-compose up rag_goal
```
