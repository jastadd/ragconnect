# Extending `RagConnect`

To add a new communication protocol, the following locations have to be changed (replace `ABC` and `abc` with the name of the protocol).

### Within `ragconnect.base/src/main/resources`

{% raw %}
- Add a new handler `ABCHandler.jadd`, similar to the existing handlers. A handler must have a constructor accepting a single String parameter, and must have a `close()` method cleaning up any held resources. 
- In `handler.mustache`, add further methods if needed for handler usage in the application code (similar to `{{rootNodeName}}.{{SetupWaitUntilReadyMethodName}}` for `mqtt`)
- In `receiveDefinition.mustache` and `sendDefinition.mustache`: add a new case in the switch statements defining the logic to happen upon connect and disconnect for both definitions (that are four distinct locations). If the new protocol is close to a PUSH semantic, follow `mqtt`. If it is closer to PULL semantic, follow `restClient`.
{% endraw %}

### Within `ragconnect.base/src/main/jastadd`

In `Handlers.jrag`: Add a new attribute `RagConnect.abcHandler()` returning the resolved handler

### Within `ragconnect.base/src/main/java/org/jastadd/ragconnect/compiler`

In `Compiler.java`:
- Add a new choice for `--protocols` similar to the existing ones
- Add a newly constructed handler in `setConfiguration`  with the needed fields (class name of the handler; unique name for the protocol (must be a valid Java identifier); whether the handler is used, i.e., if it was given in `--protocols`)

Furthermore, new test cases are appreciated, see [below](#writing-tests).

## Writing Tests

To add new tests, have a look at the module `ragconnect.tests`.
It has three parts:
1) In `src/test/01-input/*` are the [specifications](#specifications) that are going to be compiled (in principle using the steps described in [the guide to add RagConnect](adding)).
2) In `src/test/java`, the jUnit 5 [test classes](#test-classes) are implemented. They mostly correspond 1-to-1 to a directory of the first part.
3) In `build.gradle` the [instructions how to compile](#buildgradle) the specifications using the gradle plugin [PreprocessorPlugin][preprocessor-plugin] (`org.jastadd.preprocessor:testing`).

### Specifications

Every specification must have at least a `README.md` to describe the purpose of the test, a grammar `Test.relast`, and a RagConnect specification `Test.connect`.
Usually an aspect file `Test.jadd` is included.

### Test Classes

Based on jUnit 5, the test classes testing some behaviour. If sending and/or receiving functionality is used, consider extending `AbstractMqttTest` in order to avoid duplicate code. In case of extending this class, please order the methods according to their lifecycle, i.e.:
- createModel
- setupReceiverAndConnect
- communicateSendInitialValue
- communicateOnlyUpdatedValue
- closeConnections

Within `AbstractMqttTest`, an `MqttHandler` named `publisher` is available to publish content.
Some convenience methods are provided in `TestUtils`, e.g., the `DefaultMappings`, and `mqttUri` to prepend `"mqtt://"` and the correct host for the mqtt broker (`localhost` or a CI-specific host).
All tests are required to run both locally, and within the CI.

### build.gradle

Using the [PreprocessorPlugin][preprocessor-plugin], the build process can be written concisely in three parts per task:

```groovy
task compileTreeAllowedTokens(type: RagConnectTest) {
    ragconnect {
        outputDir = file('src/test/02-after-ragconnect/treeAllowedTokens')
        inputFiles = [file('src/test/01-input/treeAllowedTokens/Test.relast'),
                      file('src/test/01-input/treeAllowedTokens/Test.connect'),
                      file('src/test/01-input/treeAllowedTokens/TestDependencies.connect')]
        rootNode = 'Root'
    }
    relast {
        useJastAddNames = true
        grammarName = 'src/test/03-after-relast/treeAllowedTokens/treeAllowedTokens'
        serializer = 'jackson'
    }
    jastadd {
        jastAddList = 'JastAddList'
        packageName = 'treeAllowedTokens.ast'
        inputFiles = [file('src/test/01-input/treeAllowedTokens/Test.jadd')]
    }
}
```

[preprocessor-plugin]: https://git-st.inf.tu-dresden.de/jastadd/testing
