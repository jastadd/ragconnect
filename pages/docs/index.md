# RagConnect Documentation

[RagConnect](https://git-st.inf.tu-dresden.de/jastadd/ragconnect) is a preprocessor to enable easy connection to/from models based on [Reference Attribute Grammars](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.108.8792&rep=rep1&type=pdf) and [Relational Reference Attribute Grammars](https://doi.org/10.1016/j.cola.2019.100940) built with [JastAdd](http://jastadd.org/).
