# Communication Protocol Characteristics (Handlers)

## Java (experimental)

Uses Java methods to supply values (receive) and for callbacks (send).

- Protocol identifier: `java`
- URI scheme: `java://<ignored-host>[:ignored-port]/<topic>`
    - the value for host and port are always ignored, but are necessary to form a legal URI
- No required runtime dependencies
- Receive behaviour: Use the generated method `ragconnectJavaPush` to pass a value to the receiving port.
- Send behaviour: When the value to be sent changes, previously registered callbacks are invoked.
- Additional remarks:
    - First leading slash not included in topic.
    - Currently, the default mappings are applied, which requires a consumer to expect `byte[]` (instead of a more intuitive token or node value). This might change in future versions.


## MQTT

Use an MQTT broker to receive and send messages.

- Protocol identifier: `mqtt`
- URI scheme: `mqtt://<broker-host>[:port]/<topic>`
- Default port: 1883
- Type for mapping definitions: `byte[]`
- Required runtime dependencies:
    - `group: 'org.fusesource.mqtt-client', name: 'mqtt-client', version: '1.15'`
- Receive behaviour: Upon connection, instruct the MQTT broker to listen for messages on some topic and pass the value of those messages to the receiving port.
- Send behaviour: When the value to be sent changes, publish a message to the topic specified upon connection.
- Additional remarks:
    - First leading slash not included in topic.
    - Mqtt is selected by default, so this dependency therefore is required "by default".
    - Might work with other versions of `org.fusesource.mqtt-client.mqtt.client` as well.


## REST Server

Create a new REST server with its own target routes.

- Protocol identifier: `rest`
- URI scheme: `rest://localhost[:port]/<path>`
- Default port: 4567
- Type for mapping definitions: `byte[]`
- Required runtime dependencies:
    - `group: 'com.sparkjava', name: 'spark-core', version: '2.9.3'`
- Receive behaviour: Upon connection, create a new PUT connection and pass the value of every call to this PUT route to the receiving port.
- Send behaviour: Upon connection, create a new GET connection and serve the latest value at this GET route.
- Additional remarks:
    - Host is always `localhost`.
    - Targets to be invoked need to replace `rest` with `http`
    - Might work with newer versions of `com.sparkjava.spark-core` as well.
    - For debugging, it is beneficial to include an implementation for [SLF4J][slf4j].


## REST Client

Invoke REST routes to fetch and send values.

- Protocol identifier: `restClient`
- URI scheme: `restClient://localhost[:port]/<path>`
- Default port: 80
- Type for mapping definitions: `byte[]`
- No required runtime dependencies
- Receive behaviour: Whenever the accessor is called, a GET request fetches the latest data and returns this data.
- Send behaviour: When the value to be sent changes, a PUT request sends this data.
- Additional remarks:
    - Invoked target replaces `restClient` with `http`
    - **Important constraint**: Receiving ports are only supported for tokens, since they interrupt the getter method!

[slf4j]: https://www.slf4j.org/manual.html
