# Changelog

## 1.0.1 (dev)

### Changes

- Added new REST client handler ([#61](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/61))

### Development Changes

- Bugfix: "error: variable handler is already defined" when using multiple protocols [#58](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/58)
- Bugfix: Inherited components of a type can not be chosen as port targets [#59](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/59)

## 1.0.0

### Changes

- Allow connection ports for
    - relations ([#37](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/37))
    - attributes ([#38](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/38)), especially collection and circular attributes ([#53](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/53))
    - (sending) non-NTA nonterminals ([#36](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/36))
    - context-free context ports ([#34](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/34))
- Experimental support for Java handler ([#52](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/52))
- Make specification language more concise ([#33](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/33))
- Make dependency definitions deprecated ([#42](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/42)) and warn when used

### Development Changes

- Make grammar(s) more concise ([#40](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/40))
- Enhance documentation, adding a DSL description
- Refactor debug messages from System.out to SLF4J ([#46](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/46))

## 0.3.2

- Allow connection ports for list nonterminals ([#21](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/21))
- Ensure correct connect and disconnect functionality ([#31](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/31))
- Enhance documentation ([#13](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/13), [#20](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/20), [#41](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/41))

## 0.3.1

### Changes

- Full support for incremental dependency tracking
- Full support for subtree port definitions ([#9](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/9))
- Bugfix [#22](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/22): Correct handling of malformed URIs passed when connecting an port
- Bugfix [#23](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/23): Correct handling of OptComponents as ports
- Bugfix [#27](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/27): Correctly handle whitespaces in grammars

### Development Changes

- Internal: Use updated gradle plugin for tests ([#18](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/18))

## 0.3.0

### Changes

- Added [API documentation](ragdoc/index.html) to documentation
- Add methods to `disconnect` a port
- Bugfix [#17](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/17): Added missing support for `boolean`

### Development Changes

- Internal: PoC for incremental dependency tracking and subtree port definitions ([#14](https://git-st.inf.tu-dresden.de/jastadd/ragconnect/-/issues/14))

## 0.2.2

- Allow normal tokens to be used in send definitions

## 0.2.1

### Changes

- New communication protocol: REST
- Selection of protocol when `connect` methods are called, by scheme of given URI

### Development Changes

- Supported printing out YAML data used for mustache templates
- Moved string constants to `MRagConnect` structure

## 0.2.0

- Version submitted in paper "A Connection from ROS to RAG-Based Models" (2020)
- Supported communication protocols: MQTT
