#!/bin/sh
echo "Old version: '$(./gradlew -q :ragconnect.base:run --args=--version)'"
if [ -z "$1" ]; then
	echo "Missing parameter for version"
	exit 1
fi
./gradlew newVersion -Pvalue=$1
